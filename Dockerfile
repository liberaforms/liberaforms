FROM python:3.10.14-slim-bookworm

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN apt-get update

RUN apt-get install libmemcached-dev libpq-dev gcc libz-dev mime-support libcairo2 supervisor -y

RUN mkdir /app
WORKDIR /app
ADD ./requirements.txt requirements.txt

RUN pip install --upgrade pip
RUN pip install -r requirements.txt --no-cache-dir
RUN pip install pylibmc --no-cache-dir

COPY . /app
COPY ./liberaforms/config/docker-build/gunicorn.py /app/gunicorn.py
COPY ./liberaforms/config/docker-build/supervisord.conf /etc/supervisor/supervisord.conf

EXPOSE 5000
CMD ["/usr/bin/supervisord", "-n"]
