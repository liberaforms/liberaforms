version: '3'

services:
  db:
    image: postgres:13.1-alpine
    container_name: ${DB_HOST}
    healthcheck:
      test: ["CMD", "pg_isready", "-q", "-d", "postgres", "-U", "${POSTGRES_ROOT_USER}"]
      timeout: 45s
      interval: 10s
      retries: 10
    expose:
      - 5432
    environment:
      POSTGRES_USER: ${POSTGRES_ROOT_USER}
      POSTGRES_PASSWORD: ${POSTGRES_ROOT_PASSWORD}
    volumes:
      - db_data:/var/lib/postgresql/data
    networks:
      - db_net

  liberaforms:
    image: liberaforms-app:${VERSION}
    container_name: liberaforms-app
    depends_on:
      - db
    ports:
      - 5000:5000
    environment:
      FLASK_DEBUG: ${FLASK_DEBUG}
      FLASK_CONFIG: ${FLASK_CONFIG}
      DB_HOST: ${DB_HOST}
      DB_USER: ${DB_USER}
      DB_PASSWORD: ${DB_PASSWORD}
      DB_NAME: ${DB_NAME}
      BASE_URL: ${BASE_URL}
      ROOT_USER: ${ROOT_USER}
      TMP_DIR: ${TMP_DIR}
      DEFAULT_LANGUAGE: ${DEFAULT_LANGUAGE}
      SECRET_KEY: ${SECRET_KEY}
      SESSION_TYPE: ${SESSION_TYPE}
      TOKEN_EXPIRATION: ${TOKEN_EXPIRATION}
      LOG_DIR: ${LOG_DIR}
      DEFAULT_TIMEZONE: ${DEFAULT_TIMEZONE}
      ENABLE_UPLOADS: ${ENABLE_UPLOADS}
      TOTAL_UPLOADS_LIMIT: ${TOTAL_UPLOADS_LIMIT}
      DEFAULT_USER_UPLOADS_LIMIT: ${DEFAULT_USER_UPLOADS_LIMIT}
      ENABLE_REMOTE_STORAGE: ${ENABLE_REMOTE_STORAGE}
      MAX_MEDIA_SIZE: ${MAX_MEDIA_SIZE}
      MAX_ATTACHMENT_SIZE: ${MAX_ATTACHMENT_SIZE}
      CRYPTO_KEY: ${CRYPTO_KEY}
      ENABLE_PROMETHEUS_METRICS: ${ENABLE_PROMETHEUS_METRICS}
      ENABLE_RSS_FEED: ${ENABLE_RSS_FEED}
      ENABLE_LDAP: ${ENABLE_LDAP}
      LDAP_SERVER: ${LDAP_SERVER}
      LDAP_ANONYMOUS_BIND: ${LDAP_ANONYMOUS_BIND}
      LDAP_BIND_ACCOUNT: ${LDAP_BIND_ACCOUNT}
      LDAP_BIND_PASSWORD: ${LDAP_BIND_PASSWORD}
      LDAP_USER_DN_LIST: ${LDAP_USER_DN_LIST}
      LDAP_SEARCH_BASE_DN: ${LDAP_SEARCH_BASE_DN}
      LDAP_FILTER: ${LDAP_FILTER}
      LDAP_MAIL_ATTRIB: ${LDAP_MAIL_ATTRIB}
      LDAP_RECOVER_PASSWD_URL: ${LDAP_RECOVER_PASSWD_URL}
      GUNICORN_WORKERS: ${GUNICORN_WORKERS}
      E2EE_MODE: ${E2EE_MODE}
    volumes:
      - /opt/liberaforms_uploads:/app/uploads
      - /opt/liberaforms_logs:/app/logs
    networks:
      - db_net

volumes:
  db_data:
    name: LF_db_data

networks:
  db_net:
    name: LF_db_net
    driver: bridge
