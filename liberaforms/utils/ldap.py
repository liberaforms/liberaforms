"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import ldap3
import ldap3.core.exceptions as ldap3_exceptions # LDAPBindError, LDAPException
import ldap3.utils as ldap3_utils
from flask import current_app, g
from liberaforms.models.site import Site

def bind(dn=None, passwd=None) -> tuple: # (connection, msg:dict)
    #ldap3_utils.dn.escape_rdn(dn)
    server = ldap3.Server(current_app.config['LDAP_SERVER'])
    if current_app.config['TESTING']:
        server = ldap3.Server.from_definition(
                current_app.config['LDAP_SERVER'],
                f"{current_app.config['ROOT_DIR']}/tests/assets/ldap/server_info.json",
                f"{current_app.config['ROOT_DIR']}/tests/assets/ldap/server_schema.json"
        )
        conn = ldap3.Connection(server, dn, passwd, client_strategy=ldap3.MOCK_SYNC)
        conn.strategy.entries_from_json(
                f"{current_app.config['ROOT_DIR']}/tests/assets/ldap/entries.json"
        )
        conn.bind()
    else:
        try:
            conn = ldap3.Connection(server, dn, passwd,
                                    lazy=False,
                                    read_only=True,
                                    client_strategy=ldap3.SYNC)
            conn.bind()
        except ldap3_exceptions.LDAPException as error:
            current_app.logger.error(error)
            return None, {"status": 0, "dn": dn, **unpack_error(error)}
    if not conn.bound:
        current_app.logger.error(conn.last_error)
        return None, {"status": 0, "dn": dn, "msg" : conn.last_error}
    if current_app.config['TESTING']:
        current_app.logger.debug(f'Just bound: dn={dn}')
    return conn, {"status": 1, "dn": dn, "msg": "bound"}

def unbind(conn) -> None:
    if conn.__class__.__name__ == "Connection":
        conn.unbind()

def build_filter(itentifier:str) -> str:
    site = g.site if 'site' in g else Site.find()
    return site.get_ldap_filter().replace('%uid', itentifier)

def search(conn, search_text:str) -> tuple: #(result:list, msg:dict)
    ldap_filter = build_filter(search_text)
    current_app.logger.debug(f"LDAP search filter: {ldap_filter}")
    try:
        conn.search(search_base=current_app.config['LDAP_SEARCH_BASE_DN'],
                    search_filter=ldap_filter,
                    search_scope=ldap3.SUBTREE,
                    attributes=ldap3.ALL_ATTRIBUTES,
                    get_operational_attributes=True)
        if conn.entries:
            return unpack_search_results(conn.entries), {'status': 1}
        return [], {'status': 0, 'msg': "Not found"}
    except ldap3_exceptions.LDAPException as error:
        current_app.logger.debug(error)
        return [], {**unpack_error(error), **{'status': 0}}

def unpack_error(error) -> dict:
    result = {'error': error.__class__.__name__, 'msg': str(error)}
    if isinstance(error.args[0], dict):
        for key, value in error.args[0]:
            result[key] = value
    return result

def unpack_search_results(entries:list) -> list:
    """Given a list of ldap3 results, return a list of dicts of attributes."""
    unpacked = []
    for entry in entries:
        data = {
            'dn': entry.entry_dn,
            'class': str(entry['structuralObjectClass']),
            'email': str(entry[current_app.config['LDAP_MAIL_ATTRIB']]),
            'entryUUID': str(entry['entryUUID'])
        }
        unpacked.append(data)
    return unpacked
