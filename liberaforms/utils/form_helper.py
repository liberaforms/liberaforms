"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""


from flask import url_for, current_app
from werkzeug.utils import secure_filename
from flask_babel import gettext as _
from liberaforms.utils import sanitizers
from liberaforms.utils import utils


def repair_form_structure(structure: list, form=None) -> list:
    """Sanitize form fields after saving formBuilder
       Ensure multi option fields have unique option.values"""

    repaired_structure = []
    for field in structure:
        if "type" in field:
            if field['type'] == 'paragraph':
                if "label" in field and field["label"]:
                    field["label"] = sanitizers.bleach_text(field["label"])
                if not field["label"]:
                    field["label"] = _('Paragraph')
                repaired_structure.append(field)
                continue

            if "label" in field and field["label"]:
                field['label'] = sanitizers.sanitize_label(field["label"])
            if not ("label" in field and field["label"]):
                # i18n: Refers to the name of a formfield
                field["label"] = _("Add an appropriate field name here")

            if "description" in field and field["description"]:
                field["description"] = sanitizers.sanitize_label(field["description"])

            if field['type'] == 'file':
                # just in case.
                # we use this name to save tmp file at public_form_bp.upload_file
                secure_name = secure_filename(field["name"])
                if field["name"] != secure_name:
                    current_app.logger.debug("File field name was not secure")
                field["name"] = secure_name

            # formBuilder does not save select dropdown correctly
            # seems to be resolved with latest version
            #if field["type"] == "select" and "multiple" in field:
            # if field["multiple"] is False:
            #     del field["multiple"]  # <- this is the fix

            # multi option fields
            if field["type"] == "checkbox-group" or \
               field["type"] == "radio-group" or \
               field["type"] == "select":
                updated_options = []

                for option in field["values"]:  # iterate this field's options
                    # unselect radio options
                    if "selected" in option and field["type"] == "radio-group":
                        option["selected"] = False

                    option["value"] = option["value"].strip()

                    if option["label"]:
                        option["label"] = sanitizers.sanitize_label(option["label"])
                    else:
                        # editor submitted an existing option with a blank label
                        # i18n: Refers to the name of a multichoice formfield option
                        option["label"] = _("Add an appropriate option name here")

                    # we need to ensure the option has a unique value because
                    # formBuilder does not enforce values for checkbox groups, radio groups and selects.
                    if not option["value"]:
                        option["value"] = utils.gen_random_uuid()

                    if option["label"] and option["value"]:
                        updated_options.append(option)
                field["values"] = updated_options
        repaired_structure.append(field)
    return repaired_structure


def status_badges(form) -> str:
    disabled = _("Disabled")
    expired = _("Expired")
    edit_mode =_("Edit mode")
    statement_required = _("Privacy statement required")
    # key_required = _("Please enable E2EE")

    badges = ""
    if form.expired:
        badges = badges + f' <span class="badge rounded-pill bg-info">{expired}</span>'
    if form.edit_mode:
        badges = badges + f' <span class="badge rounded-pill bg-info">{edit_mode}</span>'
    if form.requires_consent and not form.consents.count():
        badges = badges + f' <span class="badge rounded-pill bg-info">{statement_required}</span>'
    if not form.admin_preferences['public']:
        badges = badges + f' <span class="badge rounded-pill bg-info">{disabled}</span>'
    # if form.is_e2ee and not bool(form.e2ee_public_key):
    #     badges = badges + f' <span class="e2ee-required-badge badge rounded-pill bg-info">{key_required}</span>'

    return badges
