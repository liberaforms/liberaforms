"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import io
import base64
import re
import datetime
import time
import uuid
import json
import ssl
from urllib.request import urlopen
import magic
from PIL import Image
from email_validator import validate_email, EmailNotValidError
from flask import current_app, jsonify


def is_valid_email(email: str) -> bool:
    """Check email address string.

    check_deliverability does DNS lookup.
    """
    try:
        check = not (current_app.config['TESTING'] or
                     os.environ['FLASK_CONFIG'] == "development")
        validate_email(email, check_deliverability=check)
        return True
    except EmailNotValidError:
        return False


def is_hex_color(color: str) -> bool:
    """Ckeck for valid HTML color."""
    return bool(re.search("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", color))


def is_valid_UUID(value) -> bool:
    """Ckeck for valid UUID."""
    try:
        uuid.UUID(str(value))
        return True
    except Exception as error:
        current_app.logger.debug(error)
        return False


def is_valid_url(url) -> bool:
    if not url:
        return False
    # Regex to check valid URL
    regex = ("((http|https)://)(www.)?" +
             "[a-zA-Z0-9@:%._\\+~#?&//=]" +
             "{2,256}\\.[a-z]" +
             "{2,6}\\b([-a-zA-Z0-9@:%" +
             "._\\+~#?&//=]*)")
    if re.search(re.compile(regex), url):
        return True
    return False


def is_valid_image_url(url) -> bool:
    if current_app.config['TESTING']:
        return is_valid_url(url)
    try:
        ssl._create_default_https_context = ssl._create_unverified_context # localhost throws an error
        with urlopen(url, timeout=4) as file_like_object:
            meta = file_like_object.info()
        return meta["content-type"] in ("image/png", "image/jpeg")
    except Exception as error:
        current_app.logger.debug(error)
        return False


def is_valid_date(date) -> bool:
    """Ckeck for valid date."""
    try:
        datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
        return True
    except Exception as error:
        current_app.logger.debug(error)
        return False


def is_future_date(date) -> bool:
    """Return True when date is in the future."""
    try:
        now = time.time()
        future = int(datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S").strftime("%s"))
        return future > now
    except Exception as error:
        current_app.logger.debug(error)
        return False


def compare_field_index(index_1: list, index_2: list) -> bool:
    """Compare keys and values of two form field indexes."""
    if not (isinstance(index_1, list) and isinstance(index_2, list)):
        return False
    if len(index_1) != len(index_2):
        return False
    for field in index_1:
        if field not in index_2:
            return False
    return True


def is_valid_base64_image(image_string):
    try:
        image = base64.b64decode(image_string)
        Image.open(io.BytesIO(image))
        return True
    except Exception as error:
        current_app.logger.debug(error)
        return False


def is_valid_mimetype(file, allowed_mimetypes) -> bool:
    if file.content_type not in allowed_mimetypes:
        return False
    try:
        mimetype = magic.from_buffer(file.read(1024), mime=True)
        file.seek(0, 0)
        return mimetype and mimetype in allowed_mimetypes
    except Exception as error:
        current_app.logger.debug(error)
        return False


def is_valid_lang_code(lang_code: str) -> bool:
    lang_codes = os.path.join(current_app.config['ASSETS_DIR'], 'language_codes.json')
    with open(lang_codes, 'r', encoding="utf-8") as all_languages:
        languages = json.load(all_languages)
    return bool([d for d in languages if d['value'] == lang_code])


def is_valid_map_coords(center_point: str) -> bool:
    try:
        coords = center_point.split(", ")
        assert len(coords) == 2
        float(coords[0])
        float(coords[1])
        return True
    except:
        return False


def is_valid_e2ee_public_key(data):
    expected_keys = ["fingerprint", "public_key"]
    try:
        e2ee_public_key = data["e2ee_public_key"]
        for k in expected_keys:
            assert e2ee_public_key[k]
    except Exception as e:
        return jsonify(msg="Missing data"), 406
    if set(expected_keys) != set(e2ee_public_key.keys()):
        msg = _("Expected to receive only 'fingerprint' and 'public_key'")
        return jsonify(msg=msg), 406
    return jsonify(msg="Valid key"), 200

def is_valid_e2ee_ciphered_key(data):
    expected_keys = [
        'user_key_fingerprint',
        'form_key_fingerprint',
        'form_public_key',
        'ciphered_key_backup'
    ]
    try:
        for k in expected_keys:
            assert data[k]
    except Exception as e:
        return jsonify(msg="Unexpected data"), 406
    if set(expected_keys) != set(data.keys()):
        return jsonify(msg="Unexpected data"), 406
    return jsonify(msg="Valid ciphered key"), 200
