"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import unicodecsv as csv
import typing
from flask import current_app, g
from flask_babel import gettext as _
from liberaforms.models.answer import Answer


def write_users_csv(users) -> str:
    """Generate a CSV containing users data.

    Return temporary os path to the CSV.
    """
    fieldnames=["username", "created", "enabled", "email", "language", "forms", "admin"]
    fieldheaders={  "username": _("Username"),
                    "created": _("Created"),
                    # i18n: Used as column title
                    "enabled": _("Enabled"),
                    # i18n: Email, used as column title
                    "email": _("Email"),
                    # i18n: Used as column title
                    "language": _("Language"),
                    # i18n: Used as column title
                    "forms": _("Forms"),
                    # i18n: Whether user is admin
                    "admin": _("Admin")
                    }
    csv_name = os.path.join(os.environ['TMP_DIR'], f"{g.site.hostname}.users.csv")
    with open(csv_name, mode='wb') as csv_file:
        writer = csv.DictWriter(csv_file,
                                fieldnames=fieldnames,
                                extrasaction='ignore')
        writer.writerow(fieldheaders)
        for user in users:
            # i18n: Boolean option: True or False
            is_enabled = _("True") if user.enabled else _("False")
            is_admin = _("True") if user.is_admin() else _("False")
            row = { "username": user.username,
                    "created": user.created.strftime("%Y-%m-%d"),
                    "enabled": is_enabled,
                    "language": current_app.config['LANGUAGES'][user.get_language()][0],
                    "email": user.email,
                    "forms": user.get_forms().count(),
                    "admin": is_admin
                    }
            writer.writerow(row)
    return csv_name
