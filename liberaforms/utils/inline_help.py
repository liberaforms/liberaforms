"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import markdown
from jinja2 import Environment, FileSystemLoader
from flask import current_app, g, render_template, jsonify, url_for
from flask_babel import gettext, force_locale
import flask_login
from liberaforms.utils import utils
from liberaforms.utils import sanitizers
from liberaforms.utils import i18n
from liberaforms.utils import pygments


class InlineHelp():
    def __init__(self, language: str, user_role: str):
        self.language = language
        self.is_admin = user_role == "admin"
        self.is_editor = user_role in ("editor", "admin")
        self.is_guest = user_role == "guest"
        self.is_root_user = flask_login.current_user.email == current_app.config["ROOT_USER"]
        self.help_dir = os.path.join(current_app.config['ASSETS_DIR'], "inline_help")
        j2_env = Environment(loader=FileSystemLoader(self.help_dir))
        template = j2_env.get_template("menu.json")
        self.menu_data = json.loads(template.render({"_": self.translate}))
        self.pages_dir = os.path.join(self.help_dir, 'pages')

    def translate(self, string, **kwargs):
        with force_locale(self.language):
            return gettext(string, **kwargs)

    def _help_page_link(self, label, file_name):
        return f"<a href='#' onclick='inline_help_goto_page(\"{file_name}\")'>{label}</a>"

    def _build_link(self, text, file_name, external_page=False, **kwargs):
        if external_page:
            url = file_name
            return utils.build_link(text, url, **{**kwargs, **{"external_page": True}})
        return utils.build_link(text, "#", onclick=f'inline_help_goto_page("{file_name}")', **kwargs)

    def mimetype_risks(self, risk_level):
        risks_file = os.path.join(current_app.config['ASSETS_DIR'], 'mimetype_risks.json')
        with open(risks_file, 'r', encoding="utf-8") as all_risks:
            risks = json.load(all_risks)
        extensions = list(risks[risk_level].keys())
        extensions = (', ').join(extensions)
        mimetypes = []
        for mimetype in list(risks[risk_level].values()):
            if mimetype not in mimetypes:
                mimetypes.append(mimetype)
        mimetypes = ('\n+ ').join(mimetypes)
        return f"{extensions}\n\n+ {mimetypes}"

    def _external_link_icon(self):
        return '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-up-right"><line x1="7" y1="17" x2="17" y2="7"></line><polyline points="7 7 17 7 17 17"></polyline></svg>'

    def render_j2(self, j2_env, file_name) -> str:
        #print(file_name)
        template = j2_env.get_template(file_name)
        res = template.render({
            "help_page": self._help_page_link,
            "is_admin": self.is_admin,
            "is_editor": self.is_editor,
            "is_guest": self.is_guest,
            "is_root_user": self.is_root_user,
            "is_multilanguage_site": bool(len(g.site.custom_languages) > 1),
            "_": self.translate,
            "url_for": url_for,
            "BASE_URL": current_app.config["BASE_URL"],
            # "external_link_icon": self._external_link_icon,
            "default_enabled_language": self._get_default_enabled_language,
            "token_lifespan": self._token_lifespan(),
            "current_user": flask_login.current_user,
            "site": g.site,
            "with_link": self._build_link,
            "app_config": current_app.config,
            "human_readable_bytes": utils.human_readable_bytes,
            "mimetype_risks": self.mimetype_risks,
            "i18n_docs_site_url": i18n.get_docs_site_url})
        return res

    def search(self, search_string: str) -> dict:
        result: dict = {}
        j2_env = Environment(loader=FileSystemLoader(self.pages_dir))

        pages = []
        def load_pages(menu: dict):
            for key, value in menu.items():
                pages.append(key)
                if "sub_pages" in value and value["sub_pages"]:
                    load_pages(value["sub_pages"])

        load_pages(self.menu_data["user"])
        if self.is_admin:
            load_pages(self.menu_data["admin"])

        for file_name in pages:
            if file_name.startswith("_"):
                continue
            try:
                j2_rendered_md = self.render_j2(j2_env, file_name)
                for line in j2_rendered_md.splitlines():
                    if search_string in line:
                        page_name = self.get_page_label(file_name)
                        page_link = '<a href="#" onclick=\'inline_help_goto_page("'+file_name+'")\'>'+page_name+'</a>'
                        if page_link not in result:
                            result[page_link] = []
                        text = sanitizers.remove_html_tags(markdown.markdown(line))
                        result[page_link].append(text)
            except:
                current_app.logger.debug(f"APP - Inline help. Cannot find {file_name}")
        return result

    def get_page_label(self, file_name):
        for menu in self.menu_data.values():
            for item, item_value in menu.items():
                if file_name == item:
                    return item_value["label"]
                for sub_item, sub_value in item_value["sub_pages"].items():
                    if file_name == sub_item:
                        return sub_value["label"]
        current_app.logger.debug(f"INLINE-HELP - No page title for: {file_name}")
        return "No page title"

    def _get_default_enabled_language(self):
        default = [lang for lang in i18n.get_custom_languages() if lang['value'] == g.site.language][0]
        return default["name"]

    def _token_lifespan(self):
        """Return token life span in days."""
        return int(int(os.environ['TOKEN_EXPIRATION']) / 86400)

    def get_page(self, file_name: str):
        file_path = os.path.join(self.pages_dir, file_name)
        if not os.path.isfile(file_path):
            current_app.logger.debug(f"INLINE-HELP - Cannot file help page: {file_name}")
            return gettext("Cannot find the file %(file_name)s", file_name=file_name)
        j2_env = Environment(loader=FileSystemLoader(self.pages_dir))
        j2_rendered_md = self.render_j2(j2_env, file_name)
        html = markdown.markdown(
                j2_rendered_md,
                extensions=[pygments.CodeBlockExtension()]
        )
        return html

    def get_menu(self):
        return render_template('site/partials/inline_help_menu.html',
                               menu_data=self.menu_data,
                               is_admin=self.is_admin)
