"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
from urllib.parse import urlparse
from flask import request, g, current_app
from flask import render_template_string, has_request_context
import flask_login
from liberaforms import babel
from flask_babel import gettext as _
from flask_babel import force_locale


def all_enabled_site_languages() -> list:
    app_langs = list(current_app.config["LANGUAGES"].keys())
    return list(set(app_langs + g.site.custom_languages))


def parse_request_accept_language() -> list:
    try:
        accept_language = request.headers.get('Accept-Language', '')
        languages = accept_language.split(",")
        locales = []
        for language in languages:
            if language.split(";")[0] == language:
                locale = language.strip()[:2].lower()
                locales.append(locale)
            else:
                locale = language.split(";")[0].strip()[:2].lower()
                locales.append(locale)
        return locales if locales else [g.site.language]
    except Exception:
        current_app.logger.error("APP - unable to retrieve user-agent language")
        return [os.getenv('DEFAULT_LANGUAGE')]


def get_best_match_lang_code():
    try:
        agent_languages = parse_request_accept_language()
        for agent_lang_code in agent_languages:
            if agent_lang_code in g.site.custom_languages:
                return agent_lang_code
        app_languages = current_app.config["LANGUAGES"].keys()
        for agent_lang_code in agent_languages:
            if agent_lang_code in app_languages:
                return agent_lang_code
        return g.site.language
    except Exception:
        current_app.logger.error("APP - unable to get best match language")
        return os.getenv('DEFAULT_LANGUAGE')


@babel.localeselector
def get_interface_locale() -> str:
    """Return best locale."""
    if has_request_context():
        if flask_login.current_user and \
           flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_validated:
            return flask_login.current_user.get_language()
        return get_best_match_lang_code()
    current_app.logger.debug("APP - No request context")
    return os.getenv('DEFAULT_LANGUAGE')


def sort_languages(languages: list) -> list:
    return sorted(languages, key=lambda item: item['name'].lower())


def _get_languages(lang_codes: list) -> list:
    """Return a list of dicts ordered language name."""
    lang_file = os.path.join(current_app.config['ASSETS_DIR'], 'language_codes.json')
    with open(lang_file, 'r', encoding="utf-8") as all_languages:
        langs = json.load(all_languages)
    languages = [d for d in langs if d['value'] in lang_codes]
    return sort_languages(languages)


def iso_code_2_language(lang_code: str) -> str:
    lang_file = os.path.join(current_app.config['ASSETS_DIR'], 'language_codes.json')
    with open(lang_file, 'r', encoding="utf-8") as all_languages:
        langs = json.load(all_languages)
        language = [d for d in langs if d['value'] == lang_code]
        if len(language) == 1:
            return language[0]["name"]
        return lang_code


def get_custom_languages() -> list:
    """Return a list of dicts ordered language name."""
    return _get_languages(g.site.custom_languages)


def get_iso_lang_code(lang_code: str) -> str:
    """Given a code, return ISO code in config.LANGUAGES. Fallback en-US."""
    if lang_code in current_app.config["LANGUAGES"].keys():
        return current_app.config["LANGUAGES"][lang_code][1]
    return "en-US"


def get_best_language() -> str:
    return get_best_match_lang_code()
    #if flask_login.current_user and \
    #   flask_login.current_user.is_authenticated and \
    #   flask_login.current_user.is_validated:
    #    return flask_login.current_user.preferences["language"]
    #return get_best_match_lang_code()


def get_best_language_value(values: dict, lang_code: str = ""):
    """Return a value of a lang_code dict key."""
    # similar logic is repeated in models.consent.get_best_lang_code()

    def has_value(value) -> bool:
        if isinstance(value, dict):
            return ("html" in value and value["html"])
        return bool(value)

    if not lang_code:
        lang_code = get_best_language()
    if lang_code in values.keys() and has_value(values[lang_code]):
        return values[lang_code]
    enabled_lang_codes = all_enabled_site_languages()
    for code in values.keys():
        if code in enabled_lang_codes and has_value(values[code]):
            return values[code]
    lang_code = g.site.language
    if lang_code in values.keys() and has_value(values[lang_code]):
        return values[lang_code]
    return ""


def get_language_select_choices(include_user_language=False) -> list:
    """Return values required by a wtform select element."""
    choices = []
    for language in get_custom_languages():
        choices.append((language['value'], language['name']))
    if include_user_language:
        user_lang_code = flask_login.current_user.get_language()
        if user_lang_code not in dict(choices).keys():
            choices.append((user_lang_code, current_app.config["LANGUAGES"][user_lang_code][0]))
    return choices


def lazytext_to_serializable(obj):
    with force_locale(g.language):
        return json.loads(render_template_string('{{obj|tojson}}', obj=obj))


def get_docs_site_url(url: str) -> str:
    if g.language in ["es", "ca", "eu"]:  # available translations on docs.liberaforms.org
        path = urlparse(url).path
        if path:
            return url.replace(path, f"/{g.language}{path}")
        return f"{url}/{g.language}"
    return url
