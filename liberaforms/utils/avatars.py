"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import random
import base64
from io import BytesIO
from PIL import Image
from flask import current_app
try:
    from CairoSVG import svg2png
except ModuleNotFoundError:
    from cairosvg import svg2png


def save_avatar(username, img_base64: str):
    avatars_dir = os.path.join(current_app.config['UPLOADS_DIR'],
                               current_app.config['AVATAR_DIR'])
    avatar_path = os.path.join(avatars_dir, f"{username}.png")
    image = base64.b64decode(img_base64)
    avatar_image = Image.open(BytesIO(image))
    avatar_image.save(avatar_path, "png")


def get_avatar_source(username, as_base64=False):
    avatars_dir = os.path.join(current_app.config['UPLOADS_DIR'],
                               current_app.config['AVATAR_DIR'])
    avatar_file = f"{username}.png"
    if not os.path.exists(os.path.join(avatars_dir, avatar_file)):
        avatar_file = "default.png"
    avatar_path = os.path.join(avatars_dir, avatar_file)
    if as_base64:
        image = Image.open(avatar_path)
        with BytesIO() as buffer:
            image.save(buffer, 'png')
            base_64 = base64.b64encode(buffer.getvalue()).decode()
            return f"data:image/png;base64,{base_64}"
    return f"/file/media/avatars/{avatar_file}"


def create_default_avatar(username: str):
    """ Create an avatar with the username's initial. """

    valid_initials = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    initial = "X"
    for _initial in username.upper():
        if _initial in valid_initials:
            initial = _initial
            break

    template_path = os.path.join(current_app.config['ASSETS_DIR'], f"avatars/svg/{initial}.svg")
    with open(template_path, 'r', encoding="utf-8") as template:
        avatar_template = template.read()

    # red #8B0000
    # brown #CC6600
    # purple #990099
    # blue #00468B
    # green #009900
    # yellow #FFFF33

    svg_avatar = avatar_template.format(**{
        'bg_color': "#C0C0C0",
        'fg_color': random.choice(['#8B0000', '#CC6600', '#990099',
                                   '#00468B', '#009900', '#FFFF33'])
    })
    try:
        out_bytes = BytesIO()
        svg2png(svg_avatar, write_to=out_bytes)
        avatar_image = Image.open(out_bytes)
        avatar_path = os.path.join(current_app.config['UPLOADS_DIR'],
                                   current_app.config['AVATAR_DIR'],
                                   f"{username}.png")
        avatar_image.save(avatar_path, 'png')
    except Exception as error:
        current_app.logger.error(error)
