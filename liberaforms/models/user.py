"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import shutil
from datetime import datetime, timezone
from sqlalchemy import and_
from sqlalchemy.dialects.postgresql import ARRAY, JSONB, TIMESTAMP, ENUM, UUID
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.orm.attributes import flag_modified
from flask import current_app, g, url_for
from flask_babel import gettext as _
from liberaforms import db
from liberaforms.utils.database import CRUD
from liberaforms.models.site import Site
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.models.media import Media
from liberaforms.models.consent import Consent
from liberaforms.utils.storage.remote import RemoteStorage
from liberaforms.utils import password as password_utils
from liberaforms.utils import crypto
from liberaforms.utils import utils
from liberaforms.utils import tokens
from liberaforms.utils.avatars import get_avatar_source


#from pprint import pprint

class User(db.Model, CRUD):
    """User model definition."""

    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    password_hash = db.Column(db.String, nullable=False)
    role = db.Column(ENUM("guest", "editor", "admin", name='roles_enum'), nullable=False)
    preferences = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    blocked = db.Column(db.Boolean, default=False)
    admin = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    validated_email = db.Column(db.Boolean, default=False)
    uploads_enabled = db.Column(db.Boolean, default=False, nullable=False)
    uploads_limit = db.Column(db.Integer, nullable=False)
    token = db.Column(JSONB, nullable=True)
    authored_forms = db.relationship("Form", cascade="all, delete, delete-orphan")
    timezone = db.Column(db.String, nullable=True)
    alerts = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    fedi_auth = db.Column(MutableDict.as_mutable(JSONB), nullable=True)
    invited_by_id = db.Column(db.Integer, nullable=True)
    ldap_uuid = db.Column(UUID, nullable=True)  # user's entry id on the LDAP providor
    last_login = db.Column(TIMESTAMP, nullable=True)
    media = db.relationship("Media",
                            lazy='dynamic',
                            order_by="desc(Media.id)",
                            cascade="all, delete, delete-orphan")
    consents = db.relationship("Consent",
                               lazy='dynamic',
                               order_by="asc(Consent.id)",
                               cascade="all, delete, delete-orphan")
    data_protection = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    e2ee_public_key = db.Column(MutableDict.as_mutable(JSONB), nullable=False)

    def __init__(self, **kwargs):
        """Use to create a new User object."""
        self.created = datetime.now(timezone.utc)
        self.username = kwargs["username"]
        self.email = kwargs["email"]
        self.password_hash = password_utils.hash_password(kwargs["password"])
        self.role = kwargs["role"] if 'role' in kwargs else 'editor'
        self.preferences = self.default_user_preferences()
        self.blocked = False
        self.admin = self.default_admin_settings()
        self.validated_email = kwargs["validated_email"]
        self.uploads_enabled = Site.find().newuser_enableuploads
        self.uploads_limit = utils.string_to_bytes(current_app.config['DEFAULT_USER_UPLOADS_LIMIT'])
        self.invited_by_id = kwargs["invited_by_id"] if 'invited_by_id' in kwargs else None
        self.ldap_uuid = kwargs["ldap_uuid"] if 'ldap_uuid' in kwargs else None
        self.alerts = {}
        self.token = {}
        self.data_protection = {
            "is_public_administration": False,
            "email": "",
            "url": "",
            "name": ""
        }
        self.e2ee_public_key = {}
        self.last_login = self.created

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        """Return first User filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all Users filtered by kwargs."""
        filters = []
        if 'token' in kwargs:
            filters.append(cls.token.contains({'token': kwargs['token']}))
            kwargs.pop('token')
        if 'notifyNewForm' in kwargs:
            filters.append(cls.admin.contains({
                                "notifyNewForm": kwargs['notifyNewForm']
                            }))
            kwargs.pop('notifyNewForm')
        if 'notifyNewUser' in kwargs:
            filters.append(cls.admin.contains({
                                "notifyNewUser": kwargs['notifyNewUser']
                            }))
            kwargs.pop('notifyNewUser')
        for key, value in kwargs.items():
            filters.append(getattr(cls, key) == value)
        return cls.query.filter(*filters)

    def get_id(self):
        """Return id for LoginManager."""
        return self.id

    @property
    def is_anonymous(self):
        """Also AnonymousUserMixin property."""
        return False

    @property
    def is_authenticated(self):
        """Also AnonymousUserMixin property."""
        return True

    def is_active(self):
        """Also LoginManager function."""
        return not self.blocked and self.is_validated

    def is_guest(self) -> bool:
        """Check User role."""
        return bool(self.role == 'guest')

    def is_editor(self) -> bool:
        """Check User role."""
        return bool(self.role == 'editor' or self.is_admin())

    def is_admin(self) -> bool:
        """Check User role."""
        return bool(self.role == 'admin' or self.is_root_user())

    def is_root_user(self) -> bool:
        """Check User role."""
        return bool(self.email == current_app.config['ROOT_USER'])

    def set_role(self, role: str) -> bool:
        """Set User's role."""
        if self.is_root_user():
            return False
        self.role = role
        return True

    def is_ldap_user(self):
        return bool(self.ldap_uuid)

    @classmethod
    def count(cls) -> int:
        """Return total of all Users."""
        return cls.query.count()

    def get_created_date(self) -> str:
        """Return the User's creation datetime."""
        return utils.utc_to_g_timezone(self.created).strftime("%Y-%m-%d %H:%M:%S")

    def get_last_login(self) -> str:
        """Return the User's last login datetime."""
        if not self.last_login:
            return ""
        return utils.utc_to_g_timezone(self.last_login).strftime("%Y-%m-%d %H:%M:%S")

    @property
    def enabled(self) -> bool:
        """Is this User enabled."""
        return self.is_active()

    @property
    def is_validated(self) -> bool:
        """Has this user validated their email."""
        return self.validated_email

    def get_language(self) -> str:
        if self.preferences["language"] not in current_app.config['LANGUAGES'].keys():
            # lang_code translation may be missing after major version release
            self.preferences["language"] = "en"
            self.save()
        return self.preferences["language"]

    def get_avatar_src(self, as_base64=False):
        return get_avatar_source(self.username, as_base64)

    def get_timezone(self) -> str:
        """Return this User's timezone."""
        if self.timezone:
            return self.timezone
        return current_app.config['DEFAULT_TIMEZONE']

    def get_form(self, form_id, **kwargs):
        """Return a Form with form_id."""
        kwargs = {**{'form_id':form_id}, **kwargs}
        return self.get_forms(**kwargs).first()

    def get_forms(self, **kwargs):
        """Return filtered Forms."""
        kwargs = {**{'user_id': self.id}, **kwargs}
        return Form.query.join(FormUser, Form.id == FormUser.form_id) \
                         .filter_by(**kwargs)

    def get_edit_mode_forms(self):
        """Return Forms in edit_mode."""
        return Form.query.join(FormUser, Form.id == FormUser.form_id) \
                         .filter(FormUser.user_id == self.id,
                                 FormUser.is_editor == True,
                                 Form.edit_mode != '{}')

    def authored_forms_total(self, **kwargs) -> int:
        """Return this User's total authored Forms."""
        kwargs["author_id"] = self.id
        return Form.find_all(**kwargs).count()

    def is_e2ee_enabled(self) -> bool:
        return bool(self.e2ee_public_key)

    def encrypted_forms_data(self) -> int:
        e2ee_forms = db.session.query(Form) \
                .filter(Form.is_e2ee == True) \
                .join(FormUser) \
                .filter(FormUser.user_id == self.id) \
                .all()
        o: dict = {'forms': []}
        for e2ee_form in e2ee_forms:
            o['forms'].append({
                'slug': e2ee_form.slug,
                'id': e2ee_form.id,
                'answer_count': e2ee_form.get_total_answers(),
                'e2ee_public_key': e2ee_form.e2ee_public_key
            })
        return o

    # def can_inspect_form(self, form) -> bool:
    #     """Can User inspect Form."""
    #     if self.is_admin():
    #         return True
    #     if FormUser.find(user_id=self.id, form_id=form.id, is_editor=True):
    #         return True
    #     return False

    def invited_by(self):
        """Return User object that invited this User.

        Returns None when user created an account (site.invitation_only=False)
        """
        if not self.invited_by_id:
            return None
        user = User.find(id=self.invited_by_id)
        return user if user else None

    # @property
    # def language(self) -> str:
    #     """Return User's preferred language."""
    #     return self.preferences["language"]

    def new_form_notifications(self) -> dict:
        """Return new FormUser notification preferences."""
        return {'newAnswer': self.preferences["newAnswerNotification"],
                'expiredForm': True,
                'anon_answer_edition': False}

    def new_answer_notification_default(self) -> bool:
        """User preference for new Forms."""
        return self.preferences["newAnswerNotification"]

    def get_media_dir(self) -> str:
        """Return the os path where this User's media is stored."""
        return os.path.join(current_app.config['UPLOADS_DIR'],
                            current_app.config['MEDIA_DIR'],
                            str(self.id))

    def media_usage(self, human_readable=False):
        """Total Media disk usage."""
        total_bytes = Media.calc_total_size(user_id=self.id)
        if human_readable:
            return utils.human_readable_bytes(total_bytes)
        return total_bytes

    def attachments_usage(self, human_readable=False):
        """Total AnswerAttachment disk usage."""
        total_bytes = AnswerAttachment.calc_total_size(author_id=self.id)
        if human_readable:
            return utils.human_readable_bytes(total_bytes)
        return total_bytes

    def total_uploads_usage(self, human_readable=False):
        """Total disk usage."""
        total_bytes = self.media_usage() + self.attachments_usage()
        if human_readable:
            return utils.human_readable_bytes(total_bytes)
        return total_bytes

    def can_configure_fediverse(self) -> bool:
        """Can this User configure fediverse."""
        if self.enabled and self.role != 'guest' and current_app.config['CRYPTO_KEY']:
            return True
        return False

    def is_storage_limit_exceeded(self) -> bool:
        total_usage = self.total_uploads_usage()
        if total_usage > utils.string_to_bytes(current_app.config['TOTAL_UPLOADS_LIMIT']):
            return True  # site-wide limit exceeded
        return total_usage > self.uploads_limit

    def can_enable_uploads(self) -> bool:
        """Can uploads be enbled for this User."""
        if self.role == 'guest':
            return False
        if not (current_app.config['ENABLE_UPLOADS'] and
                current_app.config['CRYPTO_KEY']):
            return False
        return not self.is_storage_limit_exceeded()

    def can_upload(self) -> bool:
        """Can this User upload Media and solicit AnswerAttachment."""
        if not self.can_enable_uploads():
            return False
        return self.uploads_enabled

    def toggle_uploads_enabled(self) -> bool:
        """Enable/disable uploads."""
        if not self.can_enable_uploads():
            return False
        self.uploads_enabled = not self.uploads_enabled
        self.save()
        return self.uploads_enabled

    def get_uploads_limit(self, human_readable=False):
        if human_readable:
            return utils.human_readable_bytes(self.uploads_limit)
        return self.uploads_limit

    def set_uploads_limit(self, limit: int) -> None:
        max_limit = utils.string_to_bytes(current_app.config['TOTAL_UPLOADS_LIMIT'])
        self.uploads_limit = limit if limit < max_limit else max_limit

    def set_disk_usage_alert(self) -> bool:
        """Alert the User that uploads limits has been surpassed.

        Return True when alert value changes.
        """
        alert = self.total_uploads_usage() > self.uploads_limit
        if alert and 'disk_usage' not in self.alerts:
            self.alerts['disk_usage'] = True
            return True
        if not alert and 'disk_usage' in self.alerts:
            del self.alerts['disk_usage']
            return True
        return False

    def get_data_protection_org(self) -> dict:
        if g.site.data_protection["enforce_org"]:
            return g.site.data_protection["organization"]
        return self.data_protection

    def save_data_protection(self) -> None:
        flag_modified(self, "data_protection")
        self.save()

    def is_public_administration(self) -> bool:
        return self.get_data_protection_org()["is_public_administration"]

    def get_available_consents(self):
        _filter = db.or_(and_(Consent.site_id != None,
                              Consent.enforced == False,
                              Consent.shared == True),
                         Consent.user_id == self.id)
        return db.session.query(Consent).filter(_filter).all()

    def update_alerts(self) -> bool:
        """Update User's alerts."""
        return self.set_disk_usage_alert()

    def delete(self) -> None:
        """Delete User, Forms, Answers, Attachments, Media."""
        attachment_dirs = [form.get_attachment_dir() for form in self.authored_forms]
        super().delete()
        # delete uploaded media files dirs and buckets
        shutil.rmtree(self.get_media_dir(), ignore_errors=True)
        if current_app.config['ENABLE_REMOTE_STORAGE'] is True:
            prefix = f"media/{self.id}"
            RemoteStorage().remove_directory(prefix)
        for attachment_dir in attachment_dirs:
            shutil.rmtree(attachment_dir, ignore_errors=True)

    def save_token(self, **kwargs) -> None:
        """Use to recover password and to validate email address."""
        self.token=tokens.create_token(**kwargs)
        self.save()

    def delete_token(self) -> None:
        """Delete the token."""
        self.token={}
        self.save()

    def can_create_forms(self) -> bool:
        if not self.enabled or self.role == "guest":
            return False
        return self.preferences["can_create_forms"] if "can_create_forms" in self.preferences else True

    def set_can_create_forms(self, preference: bool) -> None:
        self.preferences["can_create_forms"] = preference

    def toggle_can_create_forms(self) -> bool:
        if "can_create_forms" in self.preferences:
            can_create = not self.preferences["can_create_forms"]
        else:
            can_create = False
        self.set_can_create_forms(can_create)
        self.save()
        return self.preferences["can_create_forms"]

    def toggle_blocked(self) -> bool:
        """Enable/disable this User."""
        if self.is_root_user():
            self.blocked=False
        else:
            self.blocked = not self.blocked
        self.save()
        return self.blocked

    def toggle_new_answer_notification_default(self):
        """Toggle notification preference."""
        preference = self.preferences["newAnswerNotification"]
        self.preferences["newAnswerNotification"] = not preference
        self.save()
        return self.preferences["newAnswerNotification"]

    @staticmethod
    def default_user_preferences() -> dict:
        """Set new User's preferences."""
        return {
            "language": Site.find().newuser_language,
            "newAnswerNotification": True,
            "show_edit_alert": True,
            "collapsed_nav_menu": False,
            "wizard_disclaimer": False,
            "can_create_forms": True  # admins can set this value
        }

    @staticmethod
    def default_admin_settings() -> dict:
        """Define default Admin preferences."""
        return {
            "notifyNewUser": False,
            "notifyNewForm": False,
            "forms": {},
            "users": {},
            "userforms": {}
        }

    def get_fedi_auth(self) -> dict:
        """Return User's Fediverse authorization."""
        if not self.fedi_auth:
            return {"node_url": "", "access_token": ""}
        return crypto.decrypt_dict(self.fedi_auth)

    def set_fedi_auth(self, data:dict) -> None:
        """Set User's Fediverse authorization."""
        self.fedi_auth = crypto.encrypt_dict(data)

    def fedi_connection_title(self) -> str:
        """Return the title of the authorization."""
        auth = self.get_fedi_auth()
        return auth['title'] if 'title' in auth else ""

    def toggle_new_user_notification(self) -> bool:
        """Send this admin an email when a new user registers at the site."""
        if not self.is_admin():
            return False
        self.admin['notifyNewUser'] = not self.admin['notifyNewUser']
        self.save()
        return self.admin['notifyNewUser']

    def toggle_new_form_notification(self) -> bool:
        """Send this admin an email when a new form is created."""
        if not self.is_admin():
            return False
        self.admin['notifyNewForm'] = not self.admin['notifyNewForm']
        self.save()
        return self.admin['notifyNewForm']

    def get_answers(self, **kwargs):
        """Return Answers to Forms authored by this User."""
        kwargs['author_id'] = str(self.id)
        return Answer.find_all(**kwargs)
