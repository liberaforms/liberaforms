"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os, markdown, shutil
import json
import glob
from datetime import datetime, timezone
from urllib.parse import urlparse
from PIL import Image
from flask import current_app, url_for

from liberaforms import db
from sqlalchemy.dialects.postgresql import JSONB, ARRAY, TIMESTAMP
from sqlalchemy.ext.mutable import MutableDict, MutableList
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy import Table as sqlalchemy_Table
from sqlalchemy import inspect as sqlalchemy_inspect
from liberaforms.models.consent import Consent
from liberaforms.domain.custom_text import CustomText
from liberaforms.utils.database import CRUD
from liberaforms.utils import sanitizers
from liberaforms.utils import html_parser
from liberaforms.utils import utils
from liberaforms.utils import i18n

#from pprint import pprint

class Site(db.Model, CRUD):
    """Site model definition."""

    __tablename__ = "site"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    name = db.Column(db.String, nullable=False)
    theme = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    invitation_only = db.Column(db.Boolean, default=True)
    smtp_config = db.Column(JSONB, nullable=False)
    newuser_enableuploads = db.Column(db.Boolean, nullable=False, default=False)
    newuser_language = db.Column(db.String, nullable=False)
    mimetypes = db.Column(JSONB, nullable=False)
    contact_info = db.Column(MutableDict.as_mutable(JSONB), nullable=False)  # other information
    blurb = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    ldap_filter = db.Column(db.String, nullable=True)
    new_form_msg = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    resources_menu = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    invitation_text = db.Column(JSONB, nullable=False)
    data_protection = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    registration_consent = db.Column(MutableList.as_mutable(ARRAY(db.Integer)), nullable=False)
    language = db.Column(db.String, nullable=False)  # default custom language
    custom_languages = db.Column(MutableList.as_mutable(ARRAY(db.String)), nullable=False)
    alerts = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    consents = db.relationship("Consent",
                               lazy='dynamic',
                               order_by="asc(Consent.id)",
                               cascade="all, delete, delete-orphan")
    form_preferences = db.Column(MutableDict.as_mutable(JSONB), default={}, nullable=False)

    def __init__(self):
        """Create a new Site object."""
        self.created = datetime.now(timezone.utc)
        self.name = "LiberaForms!"
        assert os.environ['DEFAULT_LANGUAGE'] in current_app.config["LANGUAGES"].keys()
        self.newuser_language = os.environ['DEFAULT_LANGUAGE']
        self.language = self.newuser_language
        self.custom_languages = [self.language]
        self.theme = {"primary_color": "#a13e3e", "navbar": {"font_color": "#ddd"}}
        self.mimetypes = {
                "extensions": ["pdf", "png", "odt"],
                "mimetypes": ["application/pdf",
                              "image/png",
                              "application/vnd.oasis.opendocument.text"]
        }
        self.new_form_msg = {"enabled": False, "languages": {}}
        self.resources_menu = {"enabled": True,
                               "languages": {self.language: [["Liberaforms",
                                                              "https://www.liberaforms.org"]]}}
        self.invitation_text = {}
        self.contact_info = {"enabled": True, "languages": {}}
        hostname = urlparse(current_app.config['BASE_URL']).netloc
        self.smtp_config = {
                "host": f"smtp.{hostname}",
                "port": 25,
                "encryption": "",
                "user": "",
                "password": "",
                "noreplyAddress": f"no-reply@{hostname}"
        }

        blurb_path = os.path.join(current_app.config['ASSETS_DIR'], "blurb")
        blurb_lang_codes = [_file[-5:-3] for _file in os.listdir(blurb_path)]
        assert self.language in blurb_lang_codes
        self.blurb = {'front_page': {}}
        self.save_blurb(self.language)
        self.save_short_description()

        self.data_protection = {
            "require": False,  # forms must include a consent
            "enforce_org": False,  # enforce this organization on the wizard
            "law": "General Data Protection Regulation, (EU) 2016/679",
            "organization": {
                "is_public_administration": False,
                "email": "",
                "url": "",  # privacy policy webpage
                "name": "",
            },
            "disclaimer": {"languages": {}}  # wizard disclaimer
        }
        disclaimer_path = os.path.join(current_app.config['ASSETS_DIR'], "gdpr-wizard")
        disclaimer_lang_codes = [_file[-5:-3] for _file in os.listdir(disclaimer_path)]
        assert self.language in disclaimer_lang_codes
        self.save_wizard_disclaimer(self.language)

        self.registration_consent = []
        self.alerts = {
            "smtp": {
                "label": "Email server config",
                "link": url_for('site_bp.smtp_config'),
                "level": "warning"
            },
            "data_protection": {
                "label": "Data protection settings",
                "link": url_for('site_bp.wizard_settings'),
                "level": "warning"
            },
            "default_map_field": {
                "label": "Map field coords and zoom",
                "link": url_for('site_bp.map_field_default'),
                "level": "warning"
            },
        }

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls):
        """Return this installation's Site object.

        Create the Site object when not present in database.
        """
        site = cls.query.first()
        if not site:
            site = Site()
            site.save()
        return site

    @property
    def hostname(self):
        return urlparse(current_app.config['BASE_URL']).netloc

    @property
    def host_url(self):
        """Return this site's base url."""
        return f"{current_app.config['BASE_URL']}/"

    @staticmethod
    def app_version() -> str:
        """Return LiberaForms version."""
        version_file = os.path.join(current_app.config['ROOT_DIR'], "VERSION.txt")
        try:
            with open(version_file, encoding="utf-8") as file:
                app_version = file.readline().strip()
                return app_version if app_version else ""
        except Exception as error:
            current_app.logger.error(error)
            return ""

    @staticmethod
    def alembic_version() -> str:
        """Return with database schema version."""
        try:  # new installations do not have a alembic_version table
            inspector = sqlalchemy_inspect(db.session.connection())
            alembic_table = sqlalchemy_Table("alembic_version",
                                             db.MetaData(),
                                             autoload=True,
                                             autoload_with=inspector)
            return db.session.query(alembic_table).one()[0]
        except:
            return "N/A"

    def change_favicon(self, file):
        """Change Site's icon.

        Save logo.png and favicon.ico
        """
        brand_dir = os.path.join(current_app.config['UPLOADS_DIR'],
                                 current_app.config['BRAND_DIR'])
        # Save the original image as logo.png. # used by opengraph
        new_logo = Image.open(file)
        new_logo.save(os.path.join(brand_dir, 'logo.png'))
        # Convert file to .ico and make the it square
        img = Image.open(file)
        x, y = img.size
        size = max(32, x, y)
        # icon_sizes = [(16,16), (32, 32), (48, 48), (64,64)]
        new_favicon = Image.new('RGBA', (size, size), (0, 0, 0, 0))
        new_favicon.paste(img, (int((size - x) / 2), int((size - y) / 2)))
        new_favicon.save(os.path.join(brand_dir, 'favicon.ico'))

    def reset_favicon(self) -> bool:
        """Reset Site's logo and favicon to default.

        Return True on success
        """
        brand_dir = os.path.join(current_app.config['UPLOADS_DIR'],
                                 current_app.config['BRAND_DIR'])
        logo_path = os.path.join(brand_dir, 'logo.png')
        default_logo = os.path.join(brand_dir, 'logo-default.png')
        favicon_path = os.path.join(brand_dir, 'favicon.ico')
        default_favicon = os.path.join(brand_dir, 'favicon-default.ico')
        try:
            shutil.copyfile(default_logo, logo_path)
            shutil.copyfile(default_favicon, favicon_path)
            return True
        except Exception as error:
            current_app.logger.error(error)
            return False

    def get_logo_uri(self) -> str:
        """Return public URL."""
        return f"{self.host_url}logo.png"

    def change_default_language(self, lang_code: str) -> None:
        self.language = lang_code
        assert self.language in self.custom_languages
        self.save_blurb(lang_code)
        self.save_short_description()
        self.save_wizard_disclaimer(lang_code)

    def get_all_blurbs(self) -> dict:
        result = {}
        file_name = f"front-page.{self.language}.md"
        blurb_path = os.path.join(current_app.config['ASSETS_DIR'], 'blurb')
        for lang_code in self.custom_languages:
            if lang_code in self.blurb['front_page'] and \
               self.blurb['front_page'][lang_code]["markdown"] != "":
                result[lang_code] = self.blurb['front_page'][lang_code]
            else:
                file_name = f"front-page.{lang_code}.md"
                blurb = os.path.join(blurb_path, file_name)
                if os.path.exists(blurb):
                    with open(blurb, 'r', encoding="utf-8") as default_blurb:
                        blurb_markdown = default_blurb.read()
                    result[lang_code] = {
                            "markdown": blurb_markdown,
                            "html": sanitizers.markdown_to_html(blurb_markdown)}
        return result

    def save_blurb(self, lang_code: str, md_text="") -> None:
        """Save markdown and HTML.
           Do not save when self.id == None"""
        md_text = sanitizers.remove_html_tags(md_text)
        if not md_text:
            blurb_path = os.path.join(current_app.config['ASSETS_DIR'], 'blurb')
            blurb = os.path.join(blurb_path, f"front-page.{lang_code}.md")
            if os.path.exists(blurb):
                with open(blurb, 'r', encoding="utf-8") as default_blurb:
                    md_text = default_blurb.read()
        if md_text:
            self.blurb['front_page'][lang_code] = {
                'markdown': md_text,
                'html': sanitizers.markdown_to_html(md_text)
            }
        else:
            self.blurb['front_page'][lang_code] = {"html": "", "markdown": ""}
        if self.id:
            flag_modified(self, "blurb")
            self.save()
        self.save_short_description()


    def get_all_wizard_disclaimers(self) -> dict:
        result = {}
        for lang_code in self.custom_languages:
            if lang_code in self.data_protection["disclaimer"]["languages"] and \
               self.data_protection["disclaimer"]["languages"][lang_code]["markdown"] != "":
                md_text = self.data_protection["disclaimer"]["languages"][lang_code]["markdown"]
                md_text = Consent.parse_disclaimer_md(md_text)
                result[lang_code] = {
                        "markdown": md_text,
                        "html": sanitizers.markdown_to_html(md_text)}
                #self.data_protection["disclaimer"]["languages"][lang_code]["markdown"] = md_text
                #result[lang_code] = self.data_protection["disclaimer"]["languages"][lang_code]
            else:
                default_disclaimer_path = os.path.join(current_app.config['ASSETS_DIR'], 'gdpr-wizard')
                file_name = f"disclaimer.{lang_code}.md"
                disclaimer_file = os.path.join(default_disclaimer_path, file_name)
                if os.path.exists(disclaimer_file):
                    with open(disclaimer_file, 'r', encoding="utf-8") as default_disclaimer:
                        md_text = default_disclaimer.read()
                    md_text = Consent.parse_disclaimer_md(md_text)
                    result[lang_code] = {
                            "markdown": md_text,
                            "html": sanitizers.markdown_to_html(md_text)}
        return result

    def save_wizard_disclaimer(self, lang_code: str, md_text="") -> None:
        """Save markdown and HTML.
           Do not save when self.id == None"""
        md_text = sanitizers.remove_html_tags(md_text)
        if not md_text:
            disclaimer_path = os.path.join(current_app.config['ASSETS_DIR'], 'gdpr-wizard')
            disclaimer_file = os.path.join(disclaimer_path, f"disclaimer.{lang_code}.md")
            if not os.path.exists(disclaimer_file):
                disclaimer_file = os.path.join(disclaimer_path, f"disclaimer.{self.language}.md")
            if not os.path.exists(disclaimer_file):
                disclaimer_file = os.path.join(disclaimer_path, "disclaimer.en.md")
            with open(disclaimer_file, 'r', encoding="utf-8") as default_disclaimer:
                md_text = default_disclaimer.read()
        self.data_protection["disclaimer"]["languages"][lang_code] = {
            'markdown': md_text,
            'html': sanitizers.markdown_to_html(md_text)
        }
        if self.id:
            self.save_data_protection()

    def save_short_description(self) -> None:
        """Create a short text from the blurb."""
        if self.language in self.blurb['front_page']:
            html = self.blurb['front_page'][self.language]['html']
            self.blurb['short_text'] = html_parser.get_opengraph_text(html)
        else:
            self.blurb['short_text'] = ""
        if self.id:
            flag_modified(self, "blurb")
            self.save()

    def get_short_description(self) -> str:
        """Return a short text.

        Create and save short text if missing.
        """
        if 'short_text' in self.blurb and self.blurb['short_text']:
            return self.blurb['short_text']
        self.save_short_description()
        return self.blurb['short_text']

    def get_other_information(self) -> str:
        """Return language sensitive contact info."""
        return utils.nl2br(CustomText("other-information").get_text())

    def get_resources_menu(self) -> list:
        """Return a language sensitive Resources menu."""
        menu = CustomText("resources-menu").get_text()
        return menu if menu else []

    def save_data_protection(self) -> None:
        flag_modified(self, "data_protection")
        self.save()

    def get_registration_consents(self) -> list:
        return [Consent.find(id=id) for id in self.registration_consent]

    def save_smtp_config(self, **kwargs):
        """Save SMTP configuration."""
        self.smtp_config = kwargs
        self.alerts["smtp"] = {"label": "Email server config",
                               "link": url_for('site_bp.smtp_config'),
                               "level": "warning"}
        self.save()

    def toggle_invitation_only(self) -> bool:
        """Enable/disable open new user registration."""
        self.invitation_only = not self.invitation_only
        self.save()
        return self.invitation_only

    def toggle_newuser_uploads_default(self) -> bool:
        """Enable/disable uploads for new Users."""
        self.newuser_enableuploads = not self.newuser_enableuploads
        self.save()
        return self.newuser_enableuploads

    @staticmethod
    def is_uploads_enabled() -> bool:
        return current_app.config['ENABLE_UPLOADS']

    def get_ldap_filter(self) -> str:
        return self.ldap_filter if self.ldap_filter else current_app.config['LDAP_FILTER']

    def get_default_map_field_attrs(self):
        if "default_map_field" in self.form_preferences:
            return self.form_preferences["default_map_field"]
        return {
            "centerPoint": "39.54099078086269, 3.3349138617888223", # La Sirena, Porto Cristo, Mallorca
            "zoom": 4
        }

    @staticmethod
    def get_system_log_file_count() -> int:
        try:
            file_names = f"{urlparse(current_app.config['BASE_URL']).netloc}.app.log*"
            return len(glob.glob(f"{current_app.config['LOG_DIR']}/{file_names}"))
        except Exception as error:
            current_app.logger.error(error)
            return 0

    @staticmethod
    def server_log_filters() -> list:
        return ["FORM", "USER", "DENY", "404", "SMTP"]

    @staticmethod
    def get_server_logs(log_number=None) -> dict:
        extension = f".{log_number}" if log_number else ""
        file_name = f"{urlparse(current_app.config['BASE_URL']).netloc}.app.log{extension}"
        log_path = f"{current_app.config['LOG_DIR']}/{file_name}"
        try:
            app_logs = []
            gunicorn_logs = []
            allowed_log_levels = ["DEBUG", "WARNING", "INFO"]
            log_filters = Site.server_log_filters()
            with open(log_path, 'r', encoding="utf-8") as log_file:
                for log in log_file:
                    log_parts = log.split(" ")
                    if not (len(log_parts) > 3 and log_parts[2] in allowed_log_levels):
                        continue
                    if log_parts[3].startswith("glogging:"):
                        if len(log_parts) > 5 \
                           and not (log_parts[4] == "GET" and log_parts[5] == "/\n"):
                            gunicorn_logs.append({"filter": "GUNICORN", "log": log})
                        continue
                    if log_parts[3] in log_filters:
                        app_logs.append({"filter": log_parts[3], "log": log})
                    else:
                        app_logs.append({"filter": "", "log": log})
            return {"app-logs": app_logs[::-1], "gunicorn-logs": gunicorn_logs[::-1]}
        except Exception as error:
            current_app.logger.error(error)
            return {"app-logs": [{"filter": "", "log": "ERROR: Cannot find log files"}]}
        return
