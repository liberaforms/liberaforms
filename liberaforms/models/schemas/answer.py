"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from liberaforms import ma
from liberaforms.models.answer import Answer


class AnswerSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Answer

    id = ma.Int(dump_only=True)
    created = ma.auto_field(dump_only=True)
    marked = ma.Bool()
    data = ma.auto_field()
    form = ma.auto_field()
    # submitted = ma.Method('get_submitted_time')

    # def get_submitted_time(self, obj) -> str:
    #     return obj.updated if obj.updated else obj.created
