"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from liberaforms import ma
from liberaforms.models.formuser import FormUser


class FormUserSchema(ma.SQLAlchemySchema):
    class Meta:
        model = FormUser

    id = ma.auto_field()
    user_id = ma.auto_field()
    is_editor = ma.auto_field()
    is_author = ma.Method('get_is_author')
    email = ma.Method('get_email')
    avatar_src = ma.Method('get_avatar_src')
    needs_personal_e2ee_keys = ma.Method('get_needs_personal_e2ee_keys')
    needs_e2ee_ciphered_key = ma.Method('get_needs_e2ee_ciphered_key')
    keys_are_valid = ma.Method('get_keys_are_valid')

    def get_is_author(self, obj) -> bool:
        return obj.user_id == obj.form.author_id

    def get_email(self, obj) -> str:
        return obj.user.email

    def get_avatar_src(self, obj) -> str:
        return obj.user.get_avatar_src()

    def get_needs_personal_e2ee_keys(self, obj) -> bool:
        return obj.form.is_e2ee and not obj.user.e2ee_public_key

    def get_needs_e2ee_ciphered_key(self, obj) -> bool:
        needs_ciphered = (
            obj.form.is_e2ee and not obj.e2ee_ciphered_key_backup
        )
        # print("needs_e2ee_ciphered_key",needs_ciphered)
        return needs_ciphered

    def get_keys_are_valid(self, obj) -> bool:
        # check if the user's key matches the form_user's key
        # will also be False / when the form_user has overwritten personal keys
        return obj.is_e2ee_enabled()
