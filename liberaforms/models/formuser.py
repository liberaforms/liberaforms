"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from datetime import datetime, timezone
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy.dialects.postgresql import JSONB, TIMESTAMP
from sqlalchemy.ext.mutable import MutableDict
from flask import url_for
from liberaforms import db
from liberaforms.utils.database import CRUD
from liberaforms.utils import utils

class FormUser(db.Model, CRUD):
    """FormUser model definition."""

    __tablename__ = "form_users"
    id = db.Column(db.Integer, primary_key=True, index=True, unique=True)
    created = db.Column(TIMESTAMP, nullable=False)
    form_id = db.Column(db.Integer,
                        db.ForeignKey('forms.id', ondelete="CASCADE"),
                        nullable=False)
    user_id = db.Column(db.Integer,
                        db.ForeignKey('users.id', ondelete="CASCADE"),
                        nullable=False)
    is_editor = db.Column(db.Boolean, default=False)
    notifications = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    ui_preferences = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    field_index = db.Column(JSONB, nullable=True)
    order_by = db.Column(db.String, nullable=True)  # data_display preference
    ascending = db.Column(db.Boolean, default=True)  # data_display preference
    e2ee_ciphered_key_backup = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    user = db.relationship("User", viewonly=True)
    form = db.relationship("Form", viewonly=True)

    def __init__(self, **kwargs):
        """Create a new FormUser object."""
        self.created = datetime.now(timezone.utc)
        self.form_id = kwargs['form'].id
        self.user_id = kwargs['user'].id
        self.notifications = kwargs['user'].new_form_notifications()
        self.is_editor = kwargs['is_editor']
        self.ui_preferences = FormUser.default_ui_prefs()
        self.e2ee_ciphered_key_backup = kwargs.get('e2ee_ciphered_key_backup', {})

    def __str__(self) -> str:
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        """Return first FormUser filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all FormUsers filtered by kwargs."""
        return cls.query.filter_by(**kwargs)

    @staticmethod
    def default_ui_prefs() -> dict:
        """Toggle blocks of info."""
        return {
            'introduction_text': True,  # show config block
            'form_preview': True,  # show config block
            'data_consents': True,  # show config block
            'after_submit_text': True,  # show config block
            'chrono_answers': True,  # show chronological answer submissions
            'landing_page': '',
        }

    @staticmethod
    def avaiable_landing_pages() -> list:
        return ["", "details", "expiration", "answers", "grid", "cards", "graphs", "map"]

    def get_ui_preference(self, pref):
        return self.ui_preferences[pref] if pref in self.ui_preferences else False

    def save_ui_preference(self, key, value):
        self.ui_preferences[key] = value
        flag_modified(self, "ui_preferences")
        self.save()

    def get_landing_page(self) -> str:
        if "landing_page" not in self.ui_preferences:
            return url_for('form_bp.inspect_form', form_id=self.form_id)
        pref = self.ui_preferences["landing_page"]
        if pref == "details":
            return url_for('form_bp.form_details', form_id=self.form_id)
        if pref.startswith("answers"):
            return url_for('answers_bp.list_answers', form_id=self.form_id)
        if pref == "expiration":
            return url_for('form_bp.expiration', form_id=self.form_id)
        return url_for('form_bp.inspect_form', form_id=self.form_id)

    def toggle_expiration_notification(self) -> bool:
        """Enable/disable expiration notification."""
        self.notifications['expiredForm'] = not self.notifications['expiredForm']
        self.save()
        return self.notifications['expiredForm']

    def toggle_new_answer_notification(self) -> bool:
        """Enable/disable new answer notification."""
        self.notifications['newAnswer'] = not self.notifications['newAnswer']
        self.save()
        return self.notifications['newAnswer']

    def get_field_index_preference(self) -> list:
        """Return a User's field_index order preference."""
        return self.field_index

    def save_field_index_preference(self, field_index:list) -> None:
        """Save a User's field_index order preference."""
        self.field_index = field_index
        self.save()

    def get_order_by_field(self) -> str:
        """User's "order answers by field" preference.

        Returns a field name.
        """
        return self.order_by

    def save_order_by(self, field_name:str) -> None:
        """Save a User's "order answers by field" preference."""
        self.order_by = field_name
        self.save()

    def toggle_ascending_order(self) -> bool:
        """Toggle User's ascending order preference."""
        self.ascending = not self.ascending
        self.save()
        return self.ascending

    def get_map_preferences(self) -> dict:
        if "map" in self.ui_preferences:
            return self.ui_preferences["map"]
        map_fields = self.form.get_map_fields()
        if map_fields:
            return {
                "center": map_fields[0]["centerPoint"].replace(' ', '')
                                                      .split(','),
                "zoom": map_fields[0]["zoom"]
            }
        return {}

    def get_form_role(self) -> str:
        if self.form.author_id == self.user.id:
            return 'author'
        return 'editor' if self.is_editor else 'reader'

    def is_e2ee_enabled(self) -> bool:
        # from pprint import pprint
        # pprint({
        #     'form': self.form.e2ee_public_key['fingerprint'],
        #     'ciphered_form': self.e2ee_ciphered_key_backup['form_key_fingerprint'],
        #     'user': self.user.e2ee_public_key['fingerprint'],
        #     'cyphered_user': self.e2ee_ciphered_key_backup['user_key_fingerprint'],
        # })
        return (
            self.form.is_e2ee_enabled() and
            self.user.is_e2ee_enabled() and
            self.e2ee_ciphered_key_backup and
            self.e2ee_ciphered_key_backup['form_key_fingerprint'] ==
            self.form.e2ee_public_key['fingerprint'] and
            self.e2ee_ciphered_key_backup['user_key_fingerprint'] ==
            self.user.e2ee_public_key['fingerprint']
        )
