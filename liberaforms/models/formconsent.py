"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from datetime import datetime, timezone
from sqlalchemy.dialects.postgresql import TIMESTAMP
from liberaforms import db
from liberaforms.utils.database import CRUD

class FormConsent(db.Model, CRUD):
    """FormConsent model definition."""

    __tablename__ = "form_consents"
    id = db.Column(db.Integer, primary_key=True, index=True, unique=True)
    created = db.Column(TIMESTAMP, nullable=False)
    weight = db.Column(db.Integer, nullable=False)
    form_id = db.Column(db.Integer,
                        db.ForeignKey('forms.id', ondelete="CASCADE"),
                        nullable=False)
    consent_id = db.Column(db.Integer,
                           db.ForeignKey('consents.id', ondelete="CASCADE"),
                           nullable=False)
    consent = db.relationship("Consent", viewonly=True)
    form = db.relationship("Form", viewonly=True)


    def __init__(self, **kwargs):
        """Create a new FormConsent object."""
        self.created = datetime.now(timezone.utc)
        self.form_id = kwargs['form_id']
        self.consent_id = kwargs['consent_id']
        self.weight = 0

    @classmethod
    def find(cls, **kwargs):
        """Return first FormConsent filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all FormConsent filtered by kwargs."""
        return cls.query.filter_by(**kwargs)
