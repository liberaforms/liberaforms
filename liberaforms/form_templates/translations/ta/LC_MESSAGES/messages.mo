��    �      4  �   L
      �  �   �  s   W  g   �    3  O   ?  �   �      �   �  .        ?     T     m     �  +   �     �  	   �  5   �  N   �  4   L  	   �     �  +   �  &   �  &   �                    !     '     +     -     /     1     B     N     `  -     /   �  .   �  8        E  "   Q     t     �     �     �     �     �               0     9     F     N     a     i     |  D   �  &   �  
   �            1   !  3   S  (   �     �     �     �  (   �     	     "  M   :     �  B   �     �  !   �  	     $   '  
   L     W     f  
   r  O   }     �     �  "   �     	               #     @     Q     ^  !   p     �     �     �     �     �     �  !   �  (        @     \     m     �     �  "   �  -   �       +     
   E  L   P     �     �     �     �  6   �                 	   #     -     :     G  
   M     X     `  '   o  >   �     �     �     
     &     B     ^     z  "   �  #   �     �  
   �  
   �  	            )   '   %   Q      w   X   �   ,   �   U   !  =   a!  :   �!  $   �!     �!  	   "     !"  
   ."  /   9"  1   i"     �"     �"     �"  �  �"  �  }$  �  \&    �'  7  �(    .+  �  <,  !  /  �  (1  �   &3  \   �3  h   14  b   �4  3   �4  �   15     �5     �5  �   6  �   �6  �   t7     	8  >   8  x   ^8  ^   �8  ^   69     �9     �9     �9     �9     �9     �9     �9     �9  N   �9  (   =:  7   f:  �   �:  �   1;  �   �;  �   �<  �   0=     >  �   2>  .   �>  I   �>  I   :?  7   �?  p   �?  \   -@  G   �@  x   �@     KA  (   jA     �A  C   �A     �A  :   B     >B    XB  �   dC  (   D  P   6D     �D  �   �D  �   ~E  �   9F  =   �F  )   �F  .   "G  �   QG  6   �G  h   H    �H  i   �I  �   J  T   �J  z   PK     �K  �   �K  0   �L     �L     �L     �L  ;  M     TN  I   jN  W   �N     O     O  /   /O  a   _O  2   �O     �O  "   
P  �   -P  ,   �P     �P  .   �P  (   &Q  "   OQ     rQ  k   �Q  �   �Q  h   �R  (   �R  (   S  (   =S  >   fS  o   �S  �   T  S   �T  ~   �T  %   rU  �   �U     fV  4   V  *   �V  "   �V  �   W     �W  '   �W     X  %   )X     OX     kX     X     �X     �X  !   �X  �   �X  �   Y  v   ]Z  ]   �Z  p   2[  p   �[  f   \  f   {\  p   �\  t   S]  �   �]  ;   I^      �^  1   �^  "   �^  (   �^  O   $_  }   t_  '   �_    `  }   1a    �a  �   �b  �   �c  i   �d  2   �d      e  *   9e  $   de  g   �e  �   �e     �f     �f     �f         �   �   (   
       *          J       �   �   �   �   e       �          F   p   5             _   O   9   <   w          .      /   �   K   |   ~       0       u   v   �       @                !          G   "   )   R   %       '   ^   W   6   V          Y   �   g       E   C   U          L              h   8   f      =       ;       T   1      �       \   $   j       �   �          :      t      A          �           �           Z   &   ?   H      ]   �       �   �       k           >   I   z   +   c      }   3   r   S   m   N   #       �   �   �   n   d       �   ,       b   �   `   l   �       �   	                     D   4   {       P       -                     �       i   s       �   �       �       a   X   �   7   q           �      x       [       o               �             y       B   �      �   2       Q       �   M   �        # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

Hotel name
Street name, 358
City, Post code.
Country

Phone number: 123 456 789 
[Website](https://example.com)
[Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
Street name, 15
City, Post code, County

Phone number: 123 456 789 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 1 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 2 20h30 21h 21h30 22h 3 4 5 A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 123 456 789 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com up to €5,000 Project-Id-Version: Tamil (LiberaForms)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Tamil <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/ta/>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.10-dev
 # செயல்பாட்டு கருத்து 
 இந்தச் செயலில் பங்கேற்றதற்கு நன்றி. தயவுசெய்து உங்கள் எண்ணத்தை எங்களுடன் பகிர்ந்து கொள்ளுங்கள், எனவே எதிர்கால நடவடிக்கைகளை நாங்கள் மேம்படுத்த முடியும். # காங்கிரச் II
 கடந்த ஆண்டு காங்கிரச் அத்தகைய வெற்றியாக இருந்தது, நாங்கள் அதை மீண்டும் செய்கிறோம்.

 இந்த ஆண்டு அமர்வுகளுக்கு இப்போது பதிவு செய்யுங்கள்! # தொடர்பு படிவம்
 கேள்வி இருக்கிறதா? இந்த தொடர்பு படிவத்தை நிரப்பவும், விரைவில் தொடர்புகொள்வோம். # ஓட்டல் முன்பதிவு
 எங்களுடன் தங்க வாருங்கள்! எங்கள் ஓட்டலில் ஒரு அறையை முன்பதிவு செய்ய இந்த படிவத்தை நிரப்பவும்.

 ஓட்டல் பெயர்
 தெரு பெயர், 358
 நகரம், இடுகை குறியீடு.
 நாடு

 தொலைபேசி எண்: 123 456 789 
 [வலைத்தளம்] (https://example.com)
 .
 # லாட்டரி
 சினிமாவுக்கு 5 டிக்கெட்டுகளை வெல்! செப்டம்பர் 20 க்கு முன் இந்த படிவத்திற்கு பதிலளிக்கவும். # திட்ட விண்ணப்ப படிவம்
 ஒரு திட்டம் உள்ளதா? உங்கள் திட்டத்தைப் பற்றி எங்களிடம் சொல்ல இந்த படிவத்தை நிரப்பவும். 

 இந்த படிவம் திட்ட பயன்பாடுகளுக்கு மட்டுமே. உங்களிடம் வேறு ஏதேனும் கேள்வி இருந்தால், தயவுசெய்து எங்கள் [தொடர்பு படிவம்] (https://example.com/contact-form) ஐப் பயன்படுத்தவும். # உணவக முன்பதிவு
 எங்கள் சுவையான உணவை ருசிக்க விரும்புகிறீர்களா? இந்த படிவத்தை நிறைவு செய்து உங்கள் நிகழ்ச்சி நிரலில் தேதியைச் சேமிக்கவும்!

 .
 தெரு பெயர், 15
 நகரம், போச்ட் கோட், கவுண்டி

 தொலைபேசி எண்: 123 456 789 # கோடைகால படிப்புகள்

 நாங்கள் மூன்று நாட்கள் படிப்புகள் மற்றும் பட்டறைகளை ஆயத்தம் செய்துள்ளோம்.

 தயவுசெய்து இப்போது உங்கள் இடத்தை முன்பதிவு செய்யுங்கள். முதலில், முதலில் பரிமாறப்பட்டது. # எங்களுக்கு உங்கள் உதவி தேவை
 எங்கள் மனுவில் கையெழுத்திடுங்கள். (மதியம் 12 மணி முதல் சரிபார்க்கவும்) (அதிகபட்சம் மதியம் 12 மணிக்கு பாருங்கள்) (செவ்வாய் முதல் ஞாயிற்றுக்கிழமை வரை) (ஏதேனும் இருந்தால்) (நாங்கள் 20H30 முதல் 23H30 வரை மட்டுமே திறந்திருக்கிறோம் என்பதை நினைவில் கொள்க) 1 10 எச் - 12 ம 10 எச் - 13 ம. இலவச மென்பொருளுடன் கணினி ஆய்வக மேலாண்மை 10 எச் - 13 ம. நரம்பியல் நெட்வொர்க்குகள். உங்கள் வைஃபை நிறுவலின் நடைமுறை விளக்கக்காட்சி 10 எச் - 13 ம. விளக்கக்காட்சி/டெமோ. டிபிவி ஓட்டலை நிர்வகிக்க 12 எச் - 14 ம 14 எச் - 15 மணிநேர மதிய உணவு 16 எச் - 19 எச். சமூக நாணயங்கள். உள்ளக பொருளாதாரம் 18 ம - 20 ம. தொடக்க அமர்வு 1 க்கான அறிவிலி 18 ம - 20 ம. தொடக்க அமர்வு 2 க்கான அறிவிலி 2 20 எச் 30 21 ஆ 21 எக்டேர் 0 நீதித்துறை 3 4 5 ஒரு நண்பர் என்னிடம் கூறினார் சரியானது பற்றி செயல்பாட்டு கருத்து நீங்கள் விரும்பினால் இரண்டாவது கோப்பைச் சேர்க்கவும். திட்டத்தைப் பற்றிய கூடுதல் தகவல்களைச் சேர்க்கவும். மானியத்திற்கு விண்ணப்பிக்கவும். உங்கள் திட்டத்தைப் பற்றி எங்களிடம் கூறுங்கள். உங்கள் உள்ளக முயற்சியை ஆதரிக்க குடிமக்களிடம் கேளுங்கள். உங்கள் செயல்பாட்டைப் பற்றிய அவர்களின் கருத்துக்களை பங்கேற்பாளர்களிடம் கேளுங்கள். இணைப்புகள் ஒரு அறையை முன்பதிவு செய்து எங்களுடன் தங்க வாருங்கள்! முன்பதிவு செய்தி காலை உணவு மற்றும் இரவு உணவு காலை உணவு மற்றும் மதிய உணவு இணையத்தில் உலாவுதல் நீங்கள் வர விரும்பும் நாளைத் தேர்வுசெய்க பயிற்றுவிப்பாளரைப் பற்றி கருத்து டெம்போக்கள் பற்றி கருத்து உள்ளடக்கத்தைப் பற்றி கருத்து தெரிவிக்கவும் கருத்துகள் தொடர்பு படிவம் உள்ளடக்கம் செலவு நியாயப்படுத்துதல் நாடு புறப்படுவதற்கான தேதி வந்த தேதி எங்கள் சுவையான உணவை ருசிக்க விரும்புகிறீர்களா? உங்கள் நிகழ்ச்சி நிரலில் ஒரு தேதியைச் சேமிக்கவும்! நாங்கள் உங்களுக்கு ஒரு நினைவூட்டலை அனுப்ப விரும்புகிறீர்களா? இரட்டை படுக்கை இரட்டை படுக்கை + ஒற்றை படுக்கை மின்னஞ்சல் உங்கள் திட்டத்தை இங்கே விளக்குங்கள். என்ன, எப்படி என்பதில் கவனம் செலுத்துங்கள். கோரப்பட்ட பட்செட் எதற்காகப் பயன்படுத்தப்படும் என்பதை விளக்குங்கள். முழு பலகை (காலை உணவு, மதிய உணவு மற்றும் இரவு உணவு) எதிர்கால நடவடிக்கைகள் கருத்து உள்ளதா? ஓட்டல் முன்பதிவு இந்த லாட்டரி பற்றி நீங்கள் எவ்வாறு கண்டுபிடித்தீர்கள்? நீங்கள் எத்தனை பேர்? நாங்கள் உங்களை எப்படி அழைக்க வேண்டும்? எதிர்கால பொருள் வாய்ப்புகளுக்காக நான் சமர்ப்பிக்கும் தகவல்களை வைத்திருக்க நான் உங்களை அனுமதிக்கிறேன் நான் உங்கள் நியூட்டர்களைப் பெறுகிறேன் திட்டம் தேர்ந்தெடுக்கப்படாவிட்டால் நீங்கள் எல்லா தகவல்களையும் அழிக்க விரும்புகிறேன் நான் வேறு எங்கே சாப்பிடுகிறேன் நான் வெளியே சாப்பிடுகிறேன், கேட்டதற்கு நன்றி அடையாள எண் நீங்கள் வென்றால், நாங்கள் உங்களுக்கு ஒரு மின்னஞ்சல் அனுப்புவோம்! பயிற்றுவிப்பாளர் காலை உணவு இரவு உணவு மதிய உணவு பங்கேற்பாளர்கள் இணையாக இயங்கும் இரண்டு பேச்சுக்களில் ஒன்றையும் அவர்களின் மதிய உணவு மெனுவையும் தேர்வு செய்யட்டும். லாட்டரி மதிய உணவு மற்றும் இரவு உணவு முன்பதிவுக்கு அதிகபட்சம் 10 பேர். செய்தி பெயர் பெயர் அல்லது நிக் இல்லை, நன்றி. நான் நினைவில் கொள்வேன். ஒரு நாள் காங்கிரச் அமைப்பு பிற தகவல்கள் நீங்கள் வெளிப்படுத்த விரும்பும் பிற விசயங்கள். ஏழை முதல் சிறந்த தனியுரிமை திட்ட விண்ணப்பம் திட்ட விளக்கம் திட்ட செய்தி திட்ட பெயர் பரிசை வெல்ல உங்கள் பெயரை கீழே வைக்கவும் பயிற்றுவிப்பாளரின் விளக்கங்களை மதிப்பிடுங்கள் உள்ளடக்கத்தின் தரத்தை மதிப்பிடுங்கள் கோரப்பட்ட தொகை உணவக முன்பதிவு அறை 1. AI பாகுபாடு அறை 1. நகராட்சி உத்திகள் அறை 2. அநாமதேய வலைத்தளத்தை உருவாக்குங்கள் அறை 2. பரவலாக்கப்பட்ட தொழில்நுட்பம். பேச்சு விடுதலை எங்கள் தங்குமிடம் சேமிக்கவும் பகிரப்பட்ட அறை (மற்ற 3 பேருடன் பங்க் படுக்கைகள்) ஒற்றை படுக்கை மூன்று நாட்களில் பரவியிருக்கும் பல்வேறு நடவடிக்கைகளில் மாணவர்கள் சேரலாம். பாடங்கள் கோடைகால படிப்புகள் குடும்பப்பெயர் தொலைபேசி எண் நீங்கள் ஒரு குறிப்பிட்ட விசயத்தில் ஆர்வமாக இருந்தால் எங்களிடம் கூறுங்கள். டெம்போச் வியாழக்கிழமை 27 மிக நீண்ட மிகக் குறுகிய செவ்வாய் 25 அறை வகை சைவ உணவு மரக்கறி வலைத்தளம் புதன்கிழமை 26 நீங்கள் என்ன விரும்பினீர்கள்? நாம் எதை மேம்படுத்தலாம்? உங்கள் திட்டம் உடனடியாக தேர்ந்தெடுக்கப்படாவிட்டால் நாங்கள் என்ன செய்ய வேண்டும்? நீங்கள் எப்போது சாப்பிட விரும்புகிறீர்கள்? நீங்கள் ஓட்டலில் சாப்பிடுவீர்களா? ஆம், 3 நாட்களுக்கு முன்பு மின்னஞ்சல் மூலம் ஆம், 5 நாட்களுக்கு முன்பு மின்னஞ்சல் மூலம் ஆம், 3 நாட்களுக்கு முன்பு தொலைபேசியில் ஆம், 5 நாட்களுக்கு முன்பு தொலைபேசியில் இடைவெளிகள் என்று நீங்கள் உணர்ந்தீர்கள் ... முதல் அமர்வு என்று நீங்கள் உணர்ந்தீர்கள் ... இரண்டாவது அமர்வு என்று நீங்கள் உணர்ந்தீர்கள் ... உங்கள் தொடர்பு செய்தி உங்கள் தரவு. உங்கள் மின்னஞ்சல் உங்கள் பெயர் எ.கா. +எச்டி 123 456 789 எ.கா. சங்கம், கூட்டுறவு, கூட்டு எ.கா. வன்பொருள் வாங்கவும், ஒருவரை நியமிக்கவும். எ.கா. ஃபெடிபூக் எ.கா. முதல் அமர்வு மிகவும் சுவாரச்யமானது என்றாலும் அதைப் பற்றிய கூடுதல் தகவல்களை நான் எதிர்பார்த்தேன். எ.கா. சிடிபிஆர் இணக்கம் மற்றும் மொழியியல் அறம். எ.கா. நான் இரண்டு அமர்வுகளையும் அனுபவித்தேன். பயிற்றுவிப்பாளர் இந்த விசயத்தில் மிகவும் ஆர்வமாக இருந்தார். எ.கா. நான் ஏற்கனவே உங்கள் உணவகத்தில் இருந்தேன், அது மிகவும் நன்றாக இருக்கிறது! எ.கா. நான் ஏற்கனவே உங்கள் ஓட்டலில் தங்கியிருக்கிறேன், அது மிகவும் நன்றாக இருக்கிறது! எ.கா. இது பொதுவாக நன்றாக இருந்தது. நன்றி. எ.கா. வணக்கம் சொல்ல! எ.கா. மேரி எ.கா. பாப்பின்ச் எ.கா. ச்பெயின் I.S. காபி இடைவேளையில் பச்சை தேநீர் இல்லை. எ.கா. ஃபெடிவர்ச் பற்றி ஒரு புத்தகத்தை எழுத விரும்புகிறோம். எ.கா. Https: // fedi.y எ.கா. Marie@example.cum € 5,000 வரை 