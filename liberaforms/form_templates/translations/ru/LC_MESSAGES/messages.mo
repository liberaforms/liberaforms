��    �        �   �	      H  �   I  s   �  g   K    �  O   �  �       �  �     .   �     �     �     �       +     	   ;  5   E  N   {  4   �  	   �     	  +     &   E  &   l     �     �     �     �     �     �     �     �  -   �  /   #  .   S  8   �     �  "   �     �     �          '     >     ^     w     �     �     �     �     �     �     �     �  D     &   G  
   n     y     �  1   �  3   �  (   �     &     8     H  (   V          �  M   �     �  B        [  !   q  	   �  $   �  
   �     �     �  
   �  O   �     C     K  "   \          �     �     �     �     �     �  !   �               "     6     J     ^  !   k  (   �     �     �     �     �       "   -  -   P     ~  +   �  
   �  L   �               +     3  6   D     {     �     �  	   �     �     �     �  
   �     �     �  '   �  >        L     e     �     �     �     �     �  "     #   +     O  
   h  
   s  	   ~     �  )   �  %   �     �  X   �  ,   T   U   �   =   �   :   !  $   P!     u!  	   �!     �!  
   �!  /   �!  1   �!     "     '"     ="  �  L"  >  C$  �   �%  �   Y&  �  '  �   �(  �  k)  �  O+    -  k   %.  $   �.  5   �.  1   �.  (   /  M   G/     �/  �   �/  �   G0  i   �0     61     B1  T   Y1  ;   �1  >   �1     )2     02     52     <2  %   A2     g2     u2  =   �2  U   �2  n   %3  d   �3  v   �3     p4  F   �4  0   �4     �4     5  )   /5  D   Y5  2   �5  $   �5  .   �5     %6     <6     \6  '   k6     �6     �6     �6  �   �6  O   b7     �7  O   �7  !    8  r   B8  j   �8  B    9  '   c9      �9  %   �9  6   �9  (   	:  1   2:  �   d:  D   :;  p   ;  /   �;  @    <  -   a<  J   �<     �<     �<     =     !=  �   7=     �=     �=  I   �=     @>     S>     Z>  3   o>  $   �>     �>  !   �>  I   ?  +   K?  $   w?  &   �?     �?  (   �?     @  F   ,@  >   s@  0   �@  !   �@  -   A  f   3A  7   �A  E   �A  ^   B      wB  `   �B  '   �B  �   !C     �C     �C     �C     �C  t   D  
   yD     �D     �D     �D     �D     �D  +   �D     (E     AE     QE  N   bE  V   �E  *   F  6   3F  L   jF  A   �F  ;   �F  0   5G  @   fG  E   �G  I   �G  2   7H     jH  *   �H     �H  !   �H  P   �H  ]   /I     �I  �   �I  o   UJ  �   �J  c   \K  u   �K  H   6L  :   L     �L      �L      �L  J   M  T   bM  "   �M  "   �M     �M     v       i   �      d       �   A   �   %              �   S   o   Q       z      h   R   8   E   9   -   ^   U      P   :   Y       �   2   g   *       .       5   N   D   n   X   ?       K   \       �   3              L       
       �       <       +      �   l       y      "      `   b      q       �   �   �                                �       x   �   u   7   }   e   �   =   {   _   W              �           �   $   F   ;   G       f       I       �   �           p   '   T   �   k           &          	          �          [   >   r   �   C   s   �       m   /          J   c   !   ~         |   �   �       (   )       V       @       t   �      1   �   Z   �       �   #   a   4           ,   j          ]          B         �   �         w   H   0   �                     M      O           6    # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

Hotel name
Street name, 358
City, Post code.
Country

Phone number: 123 456 789 
[Website](https://example.com)
[Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
Street name, 15
City, Post code, County

Phone number: 123 456 789 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 20h30 21h 21h30 22h A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 123 456 789 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com up to €5,000 Project-Id-Version: Russian (LiberaForms)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Russian <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/ru/>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Weblate 5.10-dev
 # Деятельность Обратная связь 
Благодарим вас за участие в этом мероприятии. Пожалуйста, поделитесь с нами своими впечатлениями, чтобы мы могли улучшить будущие мероприятия. # Конгресс II
Прошлогодний конгресс имел такой успех, что мы проводим его снова.

Регистрируйтесь на сессии этого года! # Контактная форма
У вас есть вопрос? Заполните эту контактную форму, и мы свяжемся с вами как можно скорее. # Бронирование отелей
Остановитесь у нас! Заполните эту форму, чтобы забронировать номер в нашем отеле.

Название отеля
Название улицы, 358
Город, почтовый индекс
Страна

Номер телефона: 123 456 789 
[Сайт](https://example.com)
[Карта](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
 # Лотерея
Выиграйте 5 билетов в кино! Ответьте на вопросы анкеты до 20 сентября. # Форма заявки на проект
У вас есть проект? Заполните эту форму, чтобы рассказать нам о своем предложении. 

Эта форма предназначена только для заявок на проекты. Если у вас есть другие вопросы, пожалуйста, воспользуйтесь нашей [контактной формой] (https://example.com/contact-form). # Бронирование ресторанов
Хотите попробовать наши вкусные блюда? Заполните эту форму и сохраните дату в своем ежедневнике!

[Карта вкусных ресторанов](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
Название улицы, 15
Город, почтовый индекс, округ

Номер телефона: 123 456 789 # Летние курсы

Мы подготовили три дня курсов и семинаров.

Пожалуйста, забронируйте свое место прямо сейчас. Кто первый пришел, того и обслуживаем. # Нам нужна ваша помощь
Пожалуйста, подпишите нашу петицию. (заезд с 12 часов дня) (выезд не позднее 12 часов дня) (с вторника по воскресенье) (если таковые имеются) (помните, что мы открыты только с 20:30 до 23:30) 10ч - 12ч 10 ч. - 13 ч. Управление компьютерным классом с помощью бесплатного программного обеспечения 10ч - 13ч. Нейтральные сети. Практическая презентация нашей WIFI-инсталляции 10 ч. - 13 ч. Презентация/демонстрация. TPV для управления кафе 12ч - 14ч 14 ч - 15 ч Обед 16 ч. - 19 ч. Социальные валюты. Местная экономика 18ч - 20ч. GIT для начинающих Сессия 1 18 ч. - 20 ч. GIT для начинающих Сессия 2 20ч30 21ч 21ч30 22ч Один друг сказал мне О праве Обратная связь При желании добавьте второй файл. Добавьте дополнительную информацию о проекте. Подайте заявку на грант. Расскажите нам о своем предложении. Попросите граждан поддержать вашу местную инициативу. Попросите участников высказать свое мнение о вашем мероприятии. Вложения Забронируйте номер и приезжайте к нам! Информация о бронировании Завтрак и ужин Завтрак и обед Просматривая Интернет Выберите день, когда вы хотите прийти Комментарий об инструкторе Комментарий о темпе Комментарий о содержании Комментарии Контактная форма Контент Обоснование расходов Страна Дата отправления Дата прибытия Хотите попробовать наши вкусные блюда? Запишите дату в своем ежедневнике! Хотите, чтобы мы отправили вам напоминание? Двойная кровать Двуспальная кровать + односпальная кровать Электронный адрес Объясните здесь свой проект. Сосредоточьтесь на том, что и как. Объясните, на что будет использован запрашиваемый бюджет. Полный пансион (завтрак, обед и ужин) Будущая деятельность Есть комментарий? Бронирование отелей Как вы узнали об этой лотерее? Сколько у вас человек? Как мы должны вас называть? Я разрешаю вам сохранить предоставленную мной информацию для использования в будущем для получения финансирования Я получаю ваши информационные письма Я хочу, чтобы вы удалили всю информацию, если проект не выбран Я буду есть в другом месте Я ем вне дома, спасибо, что спросили Идентификационный номер Если вы выиграете, мы пришлем вам письмо! Инструктор Просто завтрак Просто ужин Просто обед Позвольте слушателям выбрать один из двух параллельно идущих докладов и меню обеда. Лотерея Обед и ужин Не более 10 человек за одно бронирование. Сообщение Имя Имя или ник Нет, спасибо. Я буду помнить. Один день конгресса Организация Другая информация Другие вещи, которые вы хотите выразить. От плохого до отличного Конфиденциальность Приложение к проекту Описание проекта Информация о проектах Название проекта Назовите свое имя, чтобы выиграть приз Оцените объяснения преподавателя Оцените качество контента Запрошенная сумма Бронирование ресторанов Комната 1. Дескриминирование искусственного интеллекта Зал 1. Муниципальные стратегии Комната 2. Создайте анонимный веб-сайт Зал 2. Децентрализованные технологии. Свобода слова Спасите наш приют Общий номер (двухъярусные кровати с 3 другими людьми) Односпальная кровать Студенты могут принять участие в различных мероприятиях, рассчитанных на три дня. Темы Летние курсы Фамилия Номер телефона Расскажите нам, если вам интересен какой-то конкретный предмет. Темпы Четверг 27 Слишком долго Слишком короткий Вторник, 25-е Тип комнаты Вегетарианское питание Вегатерианец Веб сайт Среда 26-я Что вам понравилось? Что мы можем улучшить? Что делать, если ваш проект не был сразу выбран? Когда вы хотите поесть? Будете ли вы питаться в отеле? Да, по электронной почте за 3 дня до начала Да, по электронной почте за 5 дней до Да, по телефону за 3 дня до начала Да, по телефону за 5 дней до Вы чувствовали, что перерывы были... Вы чувствовали, что первый сеанс был... Вам показалось, что вторая сессия была... Ваша контактная информация Ваши данные. Ваша электронная почта Ваше имя например, +34 123 456 789 например, ассоциация, кооператив, коллектив Например, купить оборудование, нанять кого-то и т.д. например, FediBook Например, первая сессия была очень интересной, хотя я ожидал получить больше информации о ней. Например, соответствие GDPR и лингвистическая справедливость. Например, мне понравились оба занятия. Преподаватель был очень увлечен предметом. Например, я уже был в вашем ресторане, и он очень хорош! Например, я уже останавливался в вашем отеле, и он очень хороший! Например: В целом было приятно. Спасибо. Например, просто поздороваться! например. Мэри например, Поппинс Например, Испания например. В кофейне не было зеленого чая. Например, мы хотим написать книгу о Вселенной. например, https://fedi.cat например, mary@example.com до €5,000 