��    �      4  �   L
      �  �   �  s   W  g   �    3  O   ?  �   �      �   �  .        ?     T     m     �  +   �     �  	   �  5   �  N   �  4   L  	   �     �  +   �  &   �  &   �                    !     '     +     -     /     1     B     N     `  -     /   �  .   �  8        E  "   Q     t     �     �     �     �     �               0     9     F     N     a     i     |  D   �  &   �  
   �            1   !  3   S  (   �     �     �     �  (   �     	     "  M   :     �  B   �     �  !   �  	     $   '  
   L     W     f  
   r  O   }     �     �  "   �     	               #     @     Q     ^  !   p     �     �     �     �     �     �  !   �  (        @     \     m     �     �  "   �  -   �       +     
   E  L   P     �     �     �     �  6   �                 	   #     -     :     G  
   M     X     `  '   o  >   �     �     �     
     &     B     ^     z  "   �  #   �     �  
   �  
   �  	            )   '   %   Q      w   X   �   ,   �   U   !  =   a!  :   �!  $   �!     �!  	   "     !"  
   ."  /   9"  1   i"     �"     �"     �"  �  �"  �   �$  o   4%  q   �%  H  &  V   _'    �'  E  �(  �   *  ;   �*     �*      +     +     7+  =   H+     �+  	   �+  E   �+  G   �+  4    ,  	   U,     _,  /   o,  +   �,  +   �,     �,     �,      -     -     -     -     -     -     -     '-     3-  &   N-  '   u-  4   �-  2   �-  =   .  	   C.  (   M.     v.     �.     �.     �.  #   �.     �.     
/     /     1/     =/     S/     Y/     q/     w/     �/  P   �/  '   �/     0  (   0     D0  ;   K0  7   �0  )   �0     �0     �0     1  '   )1     Q1     `1  p   x1     �1  E   2  
   K2  &   V2  	   }2  &   �2  
   �2     �2     �2  	   �2  e   �2     D3     L3  '   ]3     �3     �3     �3      �3     �3  
   �3     �3  (   �3     %4  	   <4     F4     \4     k4     �4  $   �4     �4     �4     �4     5  &   5     @5  7   ]5  8   �5     �5  0   �5     6  L   %6     r6     {6     �6     �6  4   �6     �6     �6     �6     7     7  
    7  	   +7     57     E7     V7  )   b7  >   �7     �7     �7     �7     8     48     S8  %   s8  ,   �8  ,   �8     �8     9     9     $9     19  &   I9  ,   p9     �9  ]   �9  .   :  K   ::  :   �:  <   �:  %   �:     $;     9;     F;     X;  :   k;  '   �;     �;     �;     �;         �   �   (   
       *          J       �   �   �   �   e       �          F   p   5             _   O   9   <   w          .      /   �   K   |   ~       0       u   v   �       @                !          G   "   )   R   %       '   ^   W   6   V          Y   �   g       E   C   U          L              h   8   f      =       ;       T   1      �       \   $   j       �   �          :      t      A          �           �           Z   &   ?   H      ]   �       �   �       k           >   I   z   +   c      }   3   r   S   m   N   #       �   �   �   n   d       �   ,       b   �   `   l   �       �   	                     D   4   {       P       -                     �       i   s       �   �       �       a   X   �   7   q           �      x       [       o               �             y       B   �      �   2       Q       �   M   �        # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

Hotel name
Street name, 358
City, Post code.
Country

Phone number: 123 456 789 
[Website](https://example.com)
[Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
Street name, 15
City, Post code, County

Phone number: 123 456 789 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 1 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 2 20h30 21h 21h30 22h 3 4 5 A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 123 456 789 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com up to €5,000 Project-Id-Version: Czech (LiberaForms)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Czech <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/cs/>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=((n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2);
X-Generator: Weblate 5.10-dev
 # Zpětná vazba k činnosti 
Děkujeme vám za účast na této aktivitě. Podělte se s námi o své dojmy, abychom mohli zlepšit budoucí aktivity. # Kongres II
Loňský kongres měl takový úspěch, že ho opakujeme.

Registrujte se na letošní zasedání! # Kontaktní formulář
Máte dotaz? Vyplňte tento kontaktní formulář a my se s vámi co nejdříve spojíme. # Rezervace hotelu
Přijďte se ubytovat k nám! Vyplňte tento formulář a rezervujte si pokoj v našem hotelu.

Název hotelu
Název ulice, 358
Město, poštovní směrovací číslo.
Země

Telefonní číslo: 123 456 789 
[Webové stránky](https://example.com)
[Mapa](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
 # Loterie
Vyhrajte 5 vstupenek do kina! Odpovězte na tento formulář do 20. září. # Formulář žádosti o projekt
Máte projekt?Vyplňte tento formulář a informujte nás o svém návrhu. 

Tento formulář je určen pouze pro projektové žádosti. Pokud máte jakýkoli jiný dotaz, použijte náš [kontaktní formulář](https://example.com/contact-form). # Rezervace restaurace
Chcete ochutnat naše vynikající jídlo?Vyplňte tento formulář a uložte si termín do svého programu!

Mapa restaurace [Delicious Restaurant Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
Název ulice, 15
Město, poštovní směrovací číslo, okres

Telefonní číslo: 123 456 789 # Letní kurzy

Připravili jsme pro vás tři dny kurzů a workshopů.

Rezervujte si prosím své místo již nyní. Kdo dřív přijde, ten dřív mele. # Potřebujeme vaši pomoc
Podepište prosím naši petici. (check-in od 12:00) (check out 12.00h maximálně) (od úterý do neděle) (pokud existuje) (nezapomeňte, že máme otevřeno pouze od 20:30h do 23:30h) 1 10h - 12h 10h - 13h. Správa počítačové laboratoře s bezplatným softwarem 10h - 13h. Neutrální sítě. Praktická ukázka naší instalace WIFI 10h - 13h. Prezentace/demo. TPV pro správu kavárny 12h - 14h 14h - 15h Oběd 16h - 19h. Sociální měny. Místní ekonomika 18h - 20h. GIT pro začátečníky 1. lekce 18h - 20h. GIT pro začátečníky 2. lekce 2 20:30h 21h 21:30h 22h 3 4 5 Kamarád mi řekl Tak akorát Zpětná vazba k činnosti Pokud chcete, přidejte druhý soubor. Přidejte další informace o projektu. Požádejte o grant. Řekněte nám o svém návrhu. Požádejte občany o podporu místní iniciativy. Požádejte účastníky o zpětnou vazbu k vaší aktivitě. Přílohy Rezervujte si pokoj a přijeďte k nám! Informace o rezervaci Snídaně a večeře Snídaně a oběd Procházením internetu Vyberte si den, kdy chcete přijít Komentář k instruktorovi Komentář k tempu Komentář k obsahu Komentáře Kontaktní formulář Obsah Odůvodnění nákladů Země Datum odjezdu Datum příjezdu Chcete ochutnat naše vynikající jídlo?Zapište si termín do svého diáře! Chcete, abychom vám poslali upomínku? Dvoulůžko Manželská postel + samostatné lůžko E-mail Vysvětlete zde svůj projekt.Zaměřte se na to, co a jak. Vysvětlete, na co bude požadovaný rozpočet použit. Plná penze (snídaně, oběd a večeře) Budoucí aktivity Máte nějakou připomínku? Rezervace hotelu Jak jste se o této loterii dozvěděl? Kolik vás je? Jak vám máme říkat? Dovolím vám, abyste si informace, které vám poskytnu, ponechali pro budoucí příležitosti k financování Dostávám vaše zpravodaje Chci, abyste vymazali všechny informace, pokud projekt není vybrán Jím jinde Stravuji se venku, děkuji za optání ID number Pokud vyhrajete, pošleme vám e-mail! Instruktor Jen snídaně Jen večeře Jen oběd Nechte účastníky vybrat si jednu ze dvou paralelně probíhajících přednášek a polední menu. Loterie Oběd a večeře Maximálně 10 osob na jednu rezervaci. Vzkaz Jméno Jméno nebo přezdívka Ne, díky. Budu si to pamatovat. Jednodenní kongres Organizace Další informace Další věci, které chcete vyjádřit. Špatný až výborný Soukromí Přihláška projektu Popis projektu Informace o projektu Název projektu Zapište své jméno a vyhrajte cenu Ohodnoťte výklad instruktora Hodnocení kvality obsahu Požadovaná částka Rezervace restaurace Místnost 1. Popis umělé inteligence Místnost 1. Strategie obcí Místnost 2. Vytvoření anonymních webových stránek Místnost 2. Decentralizovaná technika. Svoboda projevu Zachraňte náš útulek Společný pokoj (palanda s dalšími 3 osobami) Jednolůžko Studenti se mohou přihlásit na různé aktivity rozložené do tří dnů. Subjekty Letní kurzy Příjmení Telefonní číslo Řekněte nám, zda vás zajímá konkrétní téma. Tempo Čtvrtek 27. Příliš dlouhé Příliš krátké Úterý 25. Typ pokoje Veganský Vegeteriánský Webová stránka Středa 26. Co se vám líbilo? Co můžeme zlepšit? Co máme dělat, pokud váš projekt nebyl vybrán okamžitě? Kdy chcete jíst? Budete se v hotelu stravovat? Ano, e-mailem 3 dny předem Ano, e-mailem 5 dní předem Ano, telefonicky 3 dny předem Ano, telefonicky 5 dní předem Cítili jste, že přestávky jsou... Měli jste pocit, že první sezení bylo... Měli jste pocit, že druhé sezení bylo... Vaše kontaktní údaje Vaše data. Váš e-mail Vaše jméno např. +420 123 456 789 např. sdružení, družstvo, kolektiv Např. koupit hardware, někoho najmout atd. např. FediBook Např. první sezení bylo velmi zajímavé, i když jsem o něm očekával více informací. např. soulad s GDPR a jazyková spravedlnost. Např. obě sezení se mi líbila.Instruktor byl pro věc velmi zapálený. Např. ve vaší restauraci jsem už byl a je moc pěkná! Např. ve vašem hotelu jsem již bydlel a je velmi pěkný! Např. Bylo to celkově hezké.Díky. Např. Jen zdravím! např. Marie např. Nováková např. Španělsko Např. v kávové přestávce nebyl žádný zelený čaj. Např. chceme napsat knihu o Fediverse. např. https://fedi.cat např. marie@example.com až 5 000 EUR 