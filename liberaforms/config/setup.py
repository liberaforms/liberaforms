"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import shutil
from pathlib import Path

def ensure_directories(app):
    uploads_dir = app.config['UPLOADS_DIR']

    media_dir = os.path.join(uploads_dir, app.config['MEDIA_DIR'])
    if not os.path.isdir(media_dir):
        os.makedirs(media_dir)

    brand_dir = os.path.join(uploads_dir, app.config['BRAND_DIR'])
    if not os.path.isdir(brand_dir):
        shutil.copytree(os.path.join(app.config['ASSETS_DIR'], 'brand'),
                        os.path.join(uploads_dir, app.config['BRAND_DIR']))

    avatar_dir = os.path.join(uploads_dir, app.config['AVATAR_DIR'])
    if not os.path.isdir(avatar_dir):
        shutil.copytree(os.path.join(app.config['ASSETS_DIR'], 'avatars'),
                        os.path.join(uploads_dir, app.config['AVATAR_DIR']),
                        ignore=shutil.ignore_patterns('svg'))

    attachment_dir = os.path.join(uploads_dir, app.config['ATTACHMENT_DIR'])
    if not os.path.isdir(attachment_dir):
        os.makedirs(attachment_dir)

    logs_dir = app.config['LOG_DIR']
    os.makedirs(logs_dir, exist_ok=True)
    log_file = Path(os.path.join(logs_dir, f"{app.config['SERVER_NAME']}.app.log"))
    log_file.touch(exist_ok=True)
