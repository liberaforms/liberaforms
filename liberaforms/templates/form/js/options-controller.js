
var is_e2ee = {{form.is_e2ee|tojson}}
var is_form_public = {{form.is_public()|tojson}}
const form_has_map = {{ form.get_map_fields()|length|tojson }}
var is_map_enabled = {{is_map_enabled|tojson}}
const form_has_email_field = {{form_has_email_field|tojson}}
var is_send_confirmation = {{send_confirmation|tojson}}
var is_anon_edition = {{is_anon_edition|tojson}}
var notify_answer_edition = {{notify_answer_edition|tojson}}

function update_form_options_state() {
  activate_public_link()
  activate_public_map_links(is_map_enabled)
  update_e2ee_dependant_features() // call this function last
}

function activate_public_link() {
  if (is_form_public == true) {
    $('#formURL').hide();
    $('#formLink').show();
  }
  else {
    $('#formURL').show();
    $('#formLink').hide();
  }
}

function activate_public_map_links(state) {
  if (form_has_map) {
    if (state == true && !is_e2ee) {
      $(".public_map_link").show()
      $(".public_map_url").hide()
    }
    else {
      $(".public_map_link").hide()
      $(".public_map_url").show()
    }
  }
}

function update_e2ee_dependant_features() {
  if (is_e2ee) { // disable features
    $('.disabled-by-e2ee-badge').show()

    toggle_option("anon_edition", false)
    $("#anon_edition_toggle").find("button").prop("disabled", true)
    toggle_option("notify_answer_edition", false)
    $("#notify_answer_edition_toggle").find("button").prop("disabled", true)

    if (form_has_map) {
      toggle_option("enable_public_map", false)
      $("#enable_public_map_toggle").find("button").prop("disabled", true)
    }
    if (form_has_email_field) {
      toggle_option("send_confirmation", false)
      $("#send_confirmation_toggle").find("button").prop("disabled", true)
    }
  }
  else { // enable features
    $(".disabled-by-e2ee-badge").hide()

    toggle_option("anon_edition", is_anon_edition)
    $("#anon_edition_toggle").find("button").prop("disabled", false)
    toggle_option("notify_answer_edition", notify_answer_edition)
    $("#notify_answer_edition_toggle").find("button").prop("disabled", false)

    if (form_has_map) {
      toggle_option("enable_public_map", is_map_enabled)
      $("#enable_public_map_toggle").find("button").prop("disabled", false)
    }
    if (form_has_email_field) {
      $("#send_confirmation_toggle").find("button").prop("disabled", false)
      toggle_option("send_confirmation", is_send_confirmation)
    }
  }
}

function lf_setup_toggle(toggle_group, url, action_id, on_true_cb, on_false_cb, before_post)
{
  $(`#${toggle_group}`).find("button").on('click', function(evt) {

    (async () => {
      if (before_post!==undefined) {
        // optional before_post() must return bool. false = do not post
        return await before_post()
      }
      return true
    })
    ().then(can_post => {
      if (!can_post) return
      $.ajax(
      {
        url : url,
        type: "POST",
        dataType: "json",
        beforeSend: function(xhr, settings) {
          if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken)
          }
        },
        success: function(data, textStatus, jqXHR)
        {
          //console.log(data)
          if (data[action_id] === true) {
            toggle_option(action_id, true)
            if (on_true_cb)
              on_true_cb(data)
          }
          else if (data[action_id] === false) {
            toggle_option(action_id, false)
            if (on_false_cb)
              on_false_cb(data)
          }
        }
      })
    })
  })
}

function toggle_option(action_id, state) {
  if (state==true) {
    $(`#${action_id}_true`).addClass('btn-primary')
                           .removeClass('btn-outline-secondary');
    $(`#${action_id}_false`).removeClass('btn-primary')
                            .addClass('btn-outline-secondary');
  }
  else {
    $(`#${action_id}_true`).removeClass('btn-primary')
                           .addClass('btn-outline-secondary');
    $(`#${action_id}_false`).addClass('btn-primary')
                            .removeClass('btn-outline-secondary');
  }
}
