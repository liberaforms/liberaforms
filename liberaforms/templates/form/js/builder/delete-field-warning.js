/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/


const delete_field_modal = new bootstrap.Modal(document.getElementById("delete-warning-modal"))

document.addEventListener("LF_deleted_field", event => {
  let field_id = delete_field_modal.LF_field_id
  if (field_id)
    fb.actions.removeField(field_id);
  delete_field_modal.hide()
});
document.getElementById('delete-warning-modal')
        .addEventListener('hide.bs.modal', event => {
  $('.stage-wrap').find('.prev-holder').removeClass('alert alert-danger ds-alert')

});

let $modal_answer_info = $('#delete-warning-modal').find('.modal-answer-info')

$('.stage-wrap').find('.delete-confirm').each(function() {
  let title = $(this).prop('title'); // i18n string
  let $btn = $(`<a class="btn del-button formbuilder-icon-cancel custom-delete-btn" title="${title}" />`)
  $(this).replaceWith($btn)
  let $form_field = $btn.closest('.form-field')
  $form_field.on('click', '.custom-delete-btn', function() {

    delete_field_modal.show()
    delete_field_modal.LF_field_id = $form_field.prop('id')
    $form_field.find('.prev-holder').addClass('alert alert-danger ds-alert')

    if ($form_field.hasClass('header-field') || $form_field.hasClass('paragraph-field'))
      $modal_answer_info.hide()
    else
      $modal_answer_info.show()
  });
});
