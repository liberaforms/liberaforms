/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/


// https://stackoverflow.com/a/3323835
var savedRange;
function saveCursorPos() {
  if(window.getSelection) //non IE Browsers
    savedRange = window.getSelection().getRangeAt(0);
  else if(document.selection) //IE
    savedRange = document.selection.createRange();
}
function restoreCursorPos() {
  if (savedRange != null) {
    if (window.getSelection) { //non IE and there is already a selection
      var s = window.getSelection();
      if (s.rangeCount > 0)
          s.removeAllRanges();
      s.addRange(savedRange);
    }
    else if (document.createRange) //non IE and no selection
      window.getSelection().addRange(savedRange);
    else if (document.selection) //IE
      savedRange.select();
  }
}

const warning_modal = new bootstrap.Modal(document.getElementById("edition-warning-modal"))
var $focused_builder_field = null
let warning_key = `LF_form_${$('#fb-editor').attr('form-id')}_edit_field_warning`

document.getElementById('edition-warning-modal')
        .addEventListener('hidden.bs.modal', event => {
          $focused_builder_field.focus()
          restoreCursorPos()
        })

jQuery(function($) {
  $('.form-field').on('click', '.fld-label, .option-label', function() {
    let $field = $(this)
    let block = $field.closest('li')
    if (block.hasClass('header-field') ||
        block.hasClass('paragraph-field') ||
        sessionStorage.getItem(warning_key))
      return
    $focused_builder_field = $field
    saveCursorPos()
    warning_modal.show()
    sessionStorage.setItem(warning_key, 'warned');
  });
});
