

$('form[name={{form_name}}]').find("#password").on("input change", function() {
  updateStrengthStatusDisplay()
});
$('form[name={{form_name}}]').find("#password").on("blur", function() {
  $(this).closest(".form-group").find(".copy-error").remove()
});
$('form[name={{form_name}}]').find("input[type=password]").on("copy cut", function(e) {
  $(this).closest(".form-group").find(".wtf-error").remove()
  $(this).closest(".form-group")
         .find(".error-messages")
         .append('<span class="wtf-error copy-error ds-error-message">{{_("Cannot copy your password")}}</span>')
  e.preventDefault();
});

$(function () {
  if ($('form[name={{form_name}}]').find("#password").val()) {
    updateStrengthStatusDisplay()
  }
  const parsley_options = {
    errorsContainer: function (field) {
      return $(field.element).closest(".form-group").find(".error-messages")
    }
  }
  $('form[name={{form_name}}]').parsley(parsley_options)
                               .on('field:validate', function(field) {
    $(field.element).closest('.form-group').find('.wtf-error').remove()
  }).on("form:submit", function() {
    let min_password_strength = 3;
    let password_strength = $('form[name={{form_name}}]').find("#password").attr("strength")
    if ( password_strength < min_password_strength) {
      return false
    }
  })
})

function updateStrengthStatusDisplay() {
  $(".error-messages").find(".copy-error").remove()
  let password = $('form[name={{form_name}}]').find("#password").val()
  let meter = document.getElementById('password-quality-meter');
  if (!password) {
    meter.value = 0
    $("#password-strength").html("")
    return
  }
  $.ajax({
      url : "{{url_for('user_bp.check_password_quality')}}",
      type: "POST",
      contentType: "application/json",
      dataType: "json",
      data : JSON.stringify({password: password}),
      beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", "{{csrf_token()}}")
        }
      },
      success: function(data, textStatus, jqXHR)
      {
        meter.value = data.strength;
        $('form[name={{form_name}}]').find("#password").attr("strength", data.strength)
        if (data.strength == 1) {
          $("#password-strength").html("{{ _('More characters please')}}")
          $("#password-strength").css("color", "var(--lf-danger)")
        }
        if (data.strength == 2) {
          $("#password-strength").html("{{ _('Almost acceptable')}}")
          $("#password-strength").css("color", "var(--lf-danger)")
        }
        if (data.strength == 3) {
          $("#password-strength").html("{{ _('Acceptable')}}")
          $("#password-strength").css("color", "var(--lf-success)")
        }
        if (data.strength == 4) {
          $("#password-strength").html("{{ _('Good password')}}")
          $("#password-strength").css("color", "var(--lf-success)")
        }
      }
  });
}
