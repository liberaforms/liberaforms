/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

var uploadsInProgress = []

function resetFailedUploadField(el) {
  $(el).closest(".form-group").find(".progress").remove()
  let $field = $(el).closest(".form-group").find("input[type=file]")
  $field.val(null)
  $field.attr('data-parsley-inhibit-submission', false)
  $field.parsley().validate()
}

async function getEncryptedUploadFormData(fields)
{
  const form = new FormData();
  // Before uploading, we encrypt the file
  // We may lose some metadata here (orig filename?)
  const pubKey = await LF_E2EE.keys.getPublic(e2ee_form_public_key);
  for (var field of fields)
  {
    for (var orig_f of field.files)
    {
      const rawBlob = await LF_E2EE.stream.toBlob(orig_f.stream());
      const cipheredDataStream = await pubKey.encryptBinary(orig_f.stream());
      // Since we pass a stream, OpenPGP.js returns a ReadableStream
      const cipheredBlob = await LF_E2EE.stream.toBlob(cipheredDataStream);
      // And we finally add the ciphered file with the correct metadata
      // (filename and mimetype)
      form.append(field.name,
        new File([cipheredBlob],
          orig_f.name, { type: orig_f.type }));
    }
  }
  return form;
}

function getPlainUploadFormData(fields)
{
  const form = new FormData();
  for (var field of fields)
  {
    for (var file of field.files)
      form.append(field.name, file)
  }
  return form
}

async function getUploadFormData(field)
{
  let form;
  if (is_e2ee_form)
    form = await getEncryptedUploadFormData(field)
  else
    form = getPlainUploadFormData(field)
  form.append("identifier", value="{{ identifier }}")
  return form;
}

$(function () {
  const $form = $("form[id=form]")
  const $submitButton = $form.find("button[name=submit]")

  $("input[type='file']").on('change', async function(evt) {
    let $field = $(this)
    let field_name = $field.prop("name")

    uploadedFields = uploadedFields.filter(item => item !== field_name)

    $('#progress-'+field_name).closest(".progress").remove()

    if (evt.target.files[0] === undefined) { return }
    //console.log(evt.target.files[0].size)

    let $progress = $(progress_template.replace(/%fieldName%/, field_name))
    $field.after($progress)

    $field.attr('data-parsley-inhibit-submission', false)
    $field.parsley().validate()
    if (!$field.parsley().isValid()) {
      return
    }
    let uploadFormData = await getUploadFormData($field)

    {% if not g.embedded %}
    var url = "{{ url_for('public_form_bp.upload_file', slug=form.slug) }}"
    {% else -%}
    url = "{{ url_for('public_form_bp.upload_file_embedded_form', slug=form.slug) }}"
    {% endif -%}

    let $progressBar = $('#progress-'+field_name)
    $.ajax({
      url: url,
      data: uploadFormData,
      processData: false,
      contentType: false,
      cache: false,
      type: 'POST',
      beforeSend: function(xhr, settings) {
        {% if not g.embedded %}
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken)
        }
        {% endif -%}
        $progressBar.addClass("bg-success")
        $field.prop("disabled", true)
        $submitButton.prop("disabled", true)
        uploadsInProgress.push(field_name)
      },
      xhr: function() {
        var xhr = new window.XMLHttpRequest();
        xhr.upload.addEventListener('progress', function(evt) {
          if (evt.lengthComputable) {
            var percentComplete = (evt.loaded / evt.total) * 100;
            $progressBar.css("width", percentComplete+"%")
            $progressBar.attr("aria-valuenow", percentComplete)
          }
        }, false);
        return xhr;
      },
      success: function(response) {
        if (!uploadedFields.includes(field_name)) {
          uploadedFields.push(field_name)
        }
        $field.attr('data-parsley-inhibit-submission', false)
      },
      error: function(xhr) {
        //console.log("xhr.status", xhr.status)
        uploadedFields = uploadedFields.filter(item => item !== field_name)
        $progressBar.removeClass("bg-success").addClass("bg-danger")
        $field.attr('data-parsley-inhibit-submission', true)
        $field.parsley().validate()
      },
      complete: function() {
        $field.prop("disabled", false)
        uploadsInProgress = uploadsInProgress.filter(item => item !== field_name)
        if (uploadsInProgress.length == 0) {
          $submitButton.prop("disabled", false)
        }
        // console.log(uploadedFields);
      }
    })
  })
});
const progress_template = `
<div class="progress ds-file-upload-progress">
<div id="progress-%fieldName%" class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" />
</div>
`
