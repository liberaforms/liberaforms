

{% if is_preview == True %}
  {# 72 = navbar, 48 = margin-y, 32 = padding-top, 84 = svg, 120 = footer #}
  let other_elements = 72 + 48 + 36 + 84 + 120;
  let min_height = window.innerHeight - other_elements + "px"
  $(".ds-marked-up").css('min-height', min_height)
  window.addEventListener('resize', () => {
    min_height = window.innerHeight - other_elements + "px"
    $(".ds-marked-up").css('min-height', min_height)
  });
{% else %}
  {# 48 = margin-y, 32 = padding-top, 84 = svg, 120 = footer #}
  let other_elements = 48 + 36 + 84 + 120;
  var min_height = window.innerHeight - other_elements + "px"
  $(".ds-marked-up").css('min-height', min_height)
  window.addEventListener('resize', () => {
    min_height = window.innerHeight - other_elements + "px"
    $(".ds-marked-up").css('min-height', min_height)
  });
{% endif %}
