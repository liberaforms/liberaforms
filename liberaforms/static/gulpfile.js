'use strict';

const gulp        = require('gulp');
const sass        = require('gulp-sass')(require('sass'));

// Compile sass. Have to use synchronous for Dart Sass.
gulp.task('sass-backend', function(){
    return gulp.src('./sass/main-backend.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass-frontend', function(){
    return gulp.src('./sass/main-frontend.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass-landing', function(){
    return gulp.src('./sass/main-landing.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass-login', function(){
    return gulp.src('./sass/main-login.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('watch', function() {
    gulp.watch('./sass/**/*.scss',gulp.series('sass-backend'));
    gulp.watch('./sass/**/*.scss',gulp.series('sass-frontend'));
    gulp.watch('./sass/**/*.scss',gulp.series('sass-landing'));
    gulp.watch('./sass/**/*.scss',gulp.series('sass-login'));
});

// Default.
gulp.task('default', gulp.series('watch'));

// Build.
gulp.task('build', function(){
    return gulp.src('./sass/main-backend.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
