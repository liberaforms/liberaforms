# FormBuilder Map Position Control Plugin

This plugin provides a map position control for the FormBuilder library, allowing users to specify a map's center point and zoom level. The plugin is designed to integrate seamlessly with FormBuilder version 3.19.12.

## Features

- **Map Control Icon**: Embedded SVG. A modified [feathericons map icon](https://github.com/feathericons/feather/blob/main/icons/map.svg) (MIT)
- **Map Initialization**: Sets up the map with default or specified settings (Example: See Liberaforms' defaultMapAttrs in edit-form.html)
- **Tile Layers**: The map control supports multiple tile sources, including OpenStreetMap, CartoDB, OpenTopoMap, and Esri World Imagery. You can easily add or modify these tile sources by updating the `tiles.options` in the initial `definition()` function of the JavaScript file. This flexibility allows you to tailor the map’s appearance to fit different use cases and preferences.
- **Dynamic Map Updates**: Automatically updates the map based on user input and tile layer changes.
- **Address Search**: Includes a search UI for locating addresses on the map using the Nominatim API (instance's url can be changed in the plugin's javascript)

## Installation

Simply import this map_position_plugin.js in your HTML before importing form-render.min.js or form-builder.min.js

## builder UI

When adding or editing the map control in FormBuilder, users can easily configure the map’s zoom level and center point through an interactive map component. This approach removes the need for manual coordinate input. The Leaflet map dynamically updates based on user selections, automatically applying the chosen zoom level and center point to the internal component settings. This ensures that adjustments are immediately reflected in the map, streamlining the configuration process.

## Rendering Modes

The map control behaves differently depending on the rendering mode. If the component is not rendered in the final mode or preview mode (e.g., during the form's formBuilder mode), the maps' address search functionality and zoom controls are disabled. This is to prevent interference with the standard drag-and-drop functionality of FormBuilder. By disabling these features in non-final modes, the user experience remains smooth and focused on form design without the distraction of map interactions.

## Customizing Styles

To modify the styles of the map component, you can find and adjust the relevant CSS in the `loadCSS` function within the JavaScript file. This function contains the CSS rules applied to the map and its associated UI elements. Customize these styles as needed to match your application's design and ensure a consistent look and feel with the rest of your user interface.

## Customizing Leaflet source URL

By default the plugin pulls leaflet from `unpkg.com`

You can change that by defining a different URL like this

```
<script>const leaflet_URL="https://example.com/some/path/to/leaflet"</script>
<script src="js/formBuilder/map-position/map_position_plugin.js"></script>
```

## builder Options

```
formBuilder({
  replaceFields: [
    ....
    {
      type: "map",
      label: "Map position",
    }
  ],
  defaultMapAttrs: {
    centerPoint: "39.54099078086269, 3.3349138617888223",
    zoom: 4
  },
  ...
})
```

## Events

The plugin fires a javascript `input` event when the map marker coordinates change.
