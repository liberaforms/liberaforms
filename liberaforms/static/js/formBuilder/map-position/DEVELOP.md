# FormBuilder Map Plugin - Developer Guide

This document provides a comprehensive guide to the `Map Position` form control in the LiberaForms FormBuilder plugin. The guide explains each function and class method used in the plugin, detailing how it works and how it can be modified.

## Overview

The `Map Position` control is a custom form element that integrates an interactive map using the Leaflet.js library. This control allows users to set a map's center position, zoom level, and tile source directly within the form editor.

## Table of Contents

1. [Plugin Structure](#plugin-structure)
2. [Class Definition](#class-definition)
3. [Key Methods](#key-methods)
   - [configure](#configure)
   - [createInitialStateMapEditor](#createinitialstatemapeditor)
   - [build](#build)
   - [onRender](#onrender)
   - [initializeMap](#initializemap)
   - [updateTileLayer](#updatetilelayer)
   - [loadCSS](#loadcss)
   - [createSearchUI](#createsearchui)
   - [searchAddress](#searchaddress)
   - [setMarker](#setmarker)
   - [updateHiddenInput](#updatehiddeninput)
4. [Modifying the Plugin](#modifying-the-plugin)

## Plugin Structure

This plugin is defined as an additional control for the FormBuilder library. It registers itself using the `window.fbControls.push` method, allowing it to be included and recognized as a control within the form editor.

- **Main Class**: `controlMap`
- **Library Dependencies**:
   - Leaflet.js for map functionality
   - Leaflet CSS for map styling

## Class Definition

### `controlMap`

This class extends the `controlClass` provided by FormBuilder and adds custom functionality for the map control.

#### Key Properties

- `definition`: Contains metadata and default attributes for the control, such as the icon, internationalization (i18n) text, and disabled attributes.

**Modifying:**

*   **Change Default Zoom:** Modify the `zoom` value in `defaultAttrs`.
*   **Change the default Map Center:** Modify the `centerPoint` in `defaultAttrs`.
*   **Change Default Tile Layer:** Modify the `tiles` option dictionary to add new tile sources.

- `js` and `css`: URLs for the Leaflet.js and Leaflet CSS files, respectively.

## Key Methods

### `configure()`

```javascript
configure() {
  if (leaflet_URL !== undefined) {
    this.js = `${leaflet_URL}/leaflet.js`;
    this.css = `${leaflet_URL}/leaflet.css`;
  }
  else {
    this.js = 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.js';
    this.css = 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.css';
  }
}
```

This method sets the JavaScript and CSS dependencies required for the map control. These are loaded dynamically when the control is initialized.
You also have the option to download these files locally inside your project, allowing you to have your project 100% offline and more secure.

See `README.md` for instructions to do that.

### `createInitialStateMapEditor(name)`

This function creates and initializes the map editor used during the form-building process. It dynamically creates a map element and inserts it into the DOM.

**Key Components:**

*   **Map Container:** A div element that serves as the map's parent container.
*   **Map Editor:** Initializes a Leaflet map with a specific center and zoom level. You can send a default value from your project by setting `formBuilderInstance.controls.opts.defaultMapAttrs` with a dictionary containing 'centerPoint' and 'zoom' pre-defined, otherwise it will default to a world map.
*   **Event Listeners:** Listeners for zoom and map movement to update the corresponding input fields.


### `build()`

This method constructs the map control's HTML structure, including the map container and the hidden input field where the map's coordinates are stored.

**Key Components:**

*   **Map Container:** A div element that holds the map.
*   **Hidden Input:** It is filled when the user clicks on the map or selectes a searched address. Stores the map's current marker coordinates, used for form submission.

**Modifying:**

*   **Change Map Size:** Adjust the height and width of the div element in this method.

### `onRender()`

The `onRender` method initializes the map when the control is rendered in the DOM. It ensures the map is fully functional and ready for user interaction.

**Modifying:**

*   **Enable/Disable Interactions:** Change the `disable_interaction` flag in `initializeMap`.

### `initializeMap(disable_interaction = false)`

This method sets up the map's initial state, including its center point, zoom level, and tile layer. It can also disable user interactions (e.g., scrolling and zooming) if needed. Currently the disable_interaction is active if the component is rendered during the builder (to avoid interferences with the formbuilder drag and drop component builder's feature.)

**Modifying:**

*   **Disable Map Interaction:** Set `disable_interaction` to `true` to disable user interactions with the map.

### `updateTileLayer(url)`

This function updates the map's tile layer based on the selected URL, allowing users to switch between different map styles.

**Modifying:**

*   **Add New Tile Sources:** Modify the `tiles` object in the `definition` to include additional tile sources.

### `loadCSS()`

The `loadCSS` method injects custom CSS into the page to style the map control and its UI elements.

**Modifying:**

*   **Customize Map Appearance:** Modify the CSS strings in this method to change the appearance of the map container, search UI, and suggestions dropdown.

### `createSearchUI()`

This method creates a search UI element on the map, allowing users to search for locations by address using the free Nominatim API Service.

**Key Components:**

*   **Search Input:** An input field for entering search queries.
*   **Search Button:** A button that triggers the search.
*   **Suggestions Dropdown:** Displays location suggestions based on the search query.

### `searchAddress(query, suggestionsList)`

This function fetches location data from the OpenStreetMap Nominatim API based on a user's search query and displays suggestions.

**Modifying:**

*   **Change API Endpoint:** Modify the URL in the `fetch` call to use a different geocoding API, it should be an API compatible Nominatim-type.

### `setMarker(lat, lon)`

This method sets a marker on the map at the specified latitude and longitude and updates the map's view to center on the marker.

**Modifying:**

*   **Customize Marker Behavior:** Modify this function to change how markers are displayed or interact with the map.

### `updateHiddenInput(lat, lng)`

This function updates the hidden input field with the map's current coordinates, ensuring that the data is saved with the form.
It launches a `dispatchEvent(new Event('input'))` for the component's `hiddenInput` that can be useful to trigger additional code.

**Modifying:**

*   **Change Input Format:** Modify how the coordinates are formatted before being set in the hidden input field.

Leaflet Component Scroll Behavior
--------------------
### Scroll Zoom Property

The Leaflet map component has scroll zoom disabled until either:

1. **First Click**: Scroll zoom is enabled after the first click on the map.
2. **First Drag**: Scroll zoom is enabled after the first drag on the map.

### Use Case

This feature prevents the map from capturing scroll events, ensuring smooth scrolling through HTML forms on the same page without interference from the map.


Modifying the Plugin
--------------------

To customize this plugin for your specific needs, you can modify the methods and properties outlined above. For example, you can change the default zoom level, add new tile layers, or customize the search functionality.

Always ensure that any changes you make are thoroughly tested to maintain the functionality and stability of the FormBuilder.
