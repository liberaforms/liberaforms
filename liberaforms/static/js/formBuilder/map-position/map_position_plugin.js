"use strict";
/**
* This file is part of LiberaForms.

* SPDX-FileCopyrightText: 2024 LiberaForms.org
* SPDX-License-Identifier: AGPL-3.0-or-later
 */

if (!window.fbControls) { window.fbControls = []; }
window.fbControls.push(function (controlClass) {
    "use strict";

    class controlMap extends controlClass {
        static get definition() {
            return {
                icon: `<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg width="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="0" stroke-linecap="round" stroke-linejoin="round" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"><path style="opacity:1;fill:currentColor;fill-opacity:1;stroke:currentColor;stroke-width:0.35;stroke-linecap:butt;stroke-linejoin:bevel;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers" d="M 0.72084806,21.942974 C 0.55537719,21.890911 0.48579791,21.84815 0.33108548,21.703434 0.15697848,21.540577 0.06442959,21.360101 0.02885363,21.114064 -0.00720099,20.864715 -0.00914855,5.1347288 0.02684438,4.8858327 0.04098356,4.7880582 0.08004872,4.6512605 0.11365586,4.5818375 0.25742202,4.284856 0.180242,4.3332418 3.9293286,2.1897176 5.8650177,1.0829957 7.5123675,0.1447149 7.590106,0.1046491 7.7129484,0.0413371 7.7665993,0.0318268 8,0.0319907 l 0.2685512,1.885e-4 3.8496138,1.9283634 3.849614,1.9283634 3.312382,-1.8936188 c 1.821811,-1.0414902 3.375259,-1.9141841 3.452106,-1.9393196 0.196603,-0.0643055 0.484148,-0.040445 0.682584,0.056641 0.304886,0.1491669 0.506588,0.4295614 0.556295,0.7733283 0.03605,0.2493481 0.038,15.9793341 0.002,16.2282301 -0.01414,0.09778 -0.0532,0.234573 -0.08681,0.303996 -0.143766,0.296981 -0.06659,0.248595 -3.815673,2.392119 -1.935689,1.106722 -3.583039,2.045003 -3.660777,2.085069 -0.122843,0.06331 -0.176494,0.07282 -0.409894,0.07266 L 15.73144,21.967813 11.883948,20.040521 8.0364569,18.113219 4.7288786,20.002915 c -1.819168,1.039332 -3.36704,1.909883 -3.4397155,1.934557 -0.1559376,0.05294 -0.409736,0.0554 -0.56832404,0.0055 z M 15.010601,12.505238 V 5.6005829 L 12,4.095362 8.9893993,2.5901412 v 6.9046378 6.904638 l 3.0035337,1.50366 c 1.651943,0.827013 3.006714,1.504371 3.010601,1.505239 0.0039,8.68e-4 0.0071,-3.105517 0.0071,-6.903078 z M 4.5189572,17.858346 7.0106007,16.434007 V 9.566957 2.6999062 L 6.8456305,2.793416 C 6.754897,2.8448464 5.6228808,3.4911661 4.330039,4.229682 L 1.9794176,5.5724382 1.979108,12.436985 c -1.724e-4,3.808301 0.010491,6.860351 0.023948,6.855123 0.013342,-0.0052 1.1454973,-0.650375 2.5159012,-1.433762 z m 15.0815818,-0.04486 2.420662,-1.384392 V 9.56378 c 0,-3.8085806 -0.0108,-6.8611177 -0.02426,-6.8558904 -0.01334,0.00518 -1.145497,0.6503755 -2.515901,1.4337621 l -2.491644,1.4243392 v 6.8660891 6.866088 l 0.09524,-0.05015 c 0.05238,-0.02758 1.184536,-0.673122 2.515901,-1.434537 z" id="path310" /></svg>`
                ,
                i18n: {
                    default: 'Map Position',
                },
                disabledAttrs: [
                    'placeholder',
                    'example',
                    'default',
                ],
                defaultAttrs: {
                    zoom: {
                        label: 'Zoom',
                        type: 'number',
                        value: 2,
                        min: 1,
                        max: 18,
                        step: 1,
                    },
                    initialState: {
                        label: 'Initial State',
                        type: 'text',
                        subtype: 'text',
                        className: 'form-control',
                        value: 'Initial' //this is the init value, it will be 'changed' after getting the default zoom/centerPoint default values
                    },
                    centerPoint: {
                        label: 'Map Center Point',
                        type: 'text',
                        subtype: 'text',
                        className: 'form-control',
                        value: '28.0, 34.1', // The world
                    },
                    tiles: {
                        label: 'Tiles URL',
                        options: {
                            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png': 'OpenStreetMap',
                            'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png': 'CartoDB Positron',
                            'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png': 'OpenTopoMap',
                            'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}': 'Esri World Imagery'
                        },
                        value: 'OpenStreetMap'
                    },
                }
            };
        }

        configure() {
            if (leaflet_URL !== undefined) {
              this.js = `${leaflet_URL}/leaflet.js`;
              this.css = `${leaflet_URL}/leaflet.css`;
            }
            else {
              this.js = 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.js';
              this.css = 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.css';
            }
        }

        updateTileLayer(url) {
            // Remove the existing tile layer if there is one
            var map = this.edit_map
            var currentTileLayer = this.currentTileLayer
            if (currentTileLayer) {
                map.removeLayer(currentTileLayer);
            }
            // Add the new tile layer
            this.currentTileLayer = L.tileLayer(url, {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
        }

        // Define the function to create and initialize the map editor
        createInitialStateMapEditor(name) {
            const component = this;
            const config = this.config || {};
            let $component_map_field_id = $($(this)[0].container).closest('li.map-field.form-field').attr('id');
            if ($component_map_field_id===undefined) {
              return
            }
            let InitialStateId = 'initialState-'+$component_map_field_id
            // initialState-frmb-1724336361020-fld-2
            const inputElementInitialState = $('#' + InitialStateId)
            const mapEditDivId = 'map-' + InitialStateId;
            const divMap = $('#'+mapEditDivId);
            console.log("$component_map_field_id", $component_map_field_id)

            if (!divMap.length  && !component.edit_map){
                    const mapContainerDiv = $('<div></div>').attr('id', InitialStateId).addClass('map-container');
                    mapContainerDiv.id = 'mapcontainer-' + InitialStateId;
                    const mapEditDiv = document.createElement('div');
                    mapEditDiv.id = mapEditDivId;
                    mapEditDiv.style.height = '250px';
                    mapEditDiv.style.zIndex = '10';
                    mapEditDiv.style.width = '500px';
                    $(mapEditDiv).addClass('form-control');
                    // Append the map editor to the map container
                    mapContainerDiv.append(mapEditDiv);
                    inputElementInitialState.after(mapContainerDiv);

                    // Step 5: Set the input element's type to hidden to be ignored by the map editor
                    inputElementInitialState.attr('type', 'hidden');

                    var centerPoint = config.centerPoint
                    var zoom = config.zoom;

                    // When moving the map scroll, changing the zoom/centerPoint fields so that the rendered map will be autoupdated
                    const zoomInput = inputElementInitialState.closest('.form-elements').find('.form-control.fld-zoom');
                    const centerPointInput = inputElementInitialState.closest('.form-elements').find('.form-control.fld-centerPoint');
                    const zoomGroup = inputElementInitialState.closest('.form-elements').find('.form-group.zoom-wrap');
                    const centerPointGroup = inputElementInitialState.closest('.form-elements').find('.form-group.centerPoint-wrap');
                    const tilesInput = inputElementInitialState.closest('.form-elements').find('.form-control.fld-tiles');

                    if (inputElementInitialState.val() == 'Initial') {
                        // Access the form builder instance using jQuery
                        var formBuilderInstance = $('#fb-editor').data('formBuilder');
                        var defaultMapAttrs = formBuilderInstance.controls &&
                                formBuilderInstance.controls.opts &&
                                formBuilderInstance.controls.opts.defaultMapAttrs;

                        if (defaultMapAttrs) {
                            // Access defaultMapAttrs from the form builder controls options
                            const defaultMapAttrs = formBuilderInstance.controls.opts.defaultMapAttrs;

                            // Set centerPoint and zoom from defaultMapAttrs
                            centerPoint = defaultMapAttrs.centerPoint;
                            zoom = defaultMapAttrs.zoom;
                        }

                        // populate inputs with default values
                        $(centerPointInput).val(centerPoint);
                        $(zoomInput).val(zoom);

                        config.initialState = '';
                        inputElementInitialState.val('Changed');
                    }
                    var [lat, lng] = centerPoint.split(',').map(Number);

                    //if tiles selectbox are changed, change the leaflet tiles
                    $(tilesInput).on('change', function() {
                        var tiles_url = $(this).val();
                        component.updateTileLayer(tiles_url)
                    });

                    var tiles_url = $(tilesInput).val();
                    component.edit_map = L.map(mapEditDivId).setView([lat, lng], zoom); // Initializes the map with default view
                    component.updateTileLayer(tiles_url)

                    // Add event listeners for Leaflet map
                    component.edit_map.on('zoomend', function() {
                        var new_zoom = this.getZoom();
                        $(zoomInput).val(new_zoom);
                    });

                    component.edit_map.on('moveend', function() {
                        const new_center = this.getCenter();
                        $(centerPointInput).val(`${new_center.lat},${new_center.lng}`);
                    });

                    $(zoomGroup).hide();
                    $(centerPointGroup).hide();
            }//end if (!divMap.length)


            setTimeout(function () {
                window.dispatchEvent(new Event('resize'));
            }, 200);

        }


        /**
         * Build the Map element
         * @return {HTMLElement|HTMLElement[]|Object} DOM Element to be injected into the form.
         */
        build() {
            const { name, description, required, ...config } = this.config;

            // Create the container for the map and the label
            this.container = this.markup('div', null, { className: 'map-container' });

            // Create the map element
            this.dom = this.markup('div', null, { id: name + '-map', style: 'height: 250px;' });
            this.container.appendChild(this.dom);

            // Create the hidden input field for coordinates
            this.hiddenInput = this.markup('input', null, {
                type: 'hidden',
                name: name,
                id: name,
                value: '',
                'field-type': 'map', // add this to help client side validation
            });
            if (required) {
              this.hiddenInput.required = "required"
            }
            this.container.appendChild(this.hiddenInput);
            if(this.preview){
                setTimeout(() => {
                    this.createInitialStateMapEditor(name);
                }, 300); //3 seconds min to avoid error when adding in the DOM the map initial state editor.
            }
            return this.container;
        }

        /**
         * Initialize the Map and handle interactions
         */
        onRender() {
            const controlSelf = this;
            const { name, description, required, ...config } = this.config;
            /** Initialize map once element is attached to the DOM
             * when editing the element(controlSelf.preview), the map disables the scroll and marker click
            */
            setTimeout(() => {
                controlSelf.initializeMap(controlSelf.preview);
            }, 100);

        }

        /**
         * Initialize the Map
         */
        initializeMap(disable_interaction = false) {

            const config = this.config || {};
            const centerPoint = config.centerPoint;
            const [lat, lng] = centerPoint.split(',').map(Number);
            const zoom = config.zoom;
            const tiles = config.tiles || "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

            if (this.map) {
                this.map.remove(); // Remove existing map instance if it exists
            }


            if(disable_interaction){
                // Do not add the marker initially or let scroll the map
                var zoomControl = false;
                this.map = L.map(this.dom.id).setView([lat, lng], zoom, zoomControl); // Disables the zoom control);
                this.map.scrollWheelZoom.disable();
                this.map.dragging.disable();
                this.map.doubleClickZoom.disable();
                this.map.off('click');
                this.map.touchZoom.disable();
                this.map.zoomControl.remove();

            }else{
                this.loadCSS();
                 // Cargar el CSS desde JavaScript
                this.map = L.map(this.dom.id).setView([lat, lng], zoom);
                this.marker = null;
                this.isZoomEnabled = false; // Variable de control
                this.map.scrollWheelZoom.disable();

                this.map.on('drag', (e) => {
                    const searchContainer = document.getElementById('search-container');
                    if (!searchContainer.contains(e.originalEvent.target)) {
                        if (!this.isZoomEnabled) {
                            // Habilita el zoom con scroll solo en el primer clic
                            this.map.scrollWheelZoom.enable();
                            this.isZoomEnabled = true; // Marca que el zoom ya está habilitado
                        }
                    }
                });
                // Add marker on map click, but not on search input click
                this.map.on('click', (e) => {
                    const searchContainer = document.getElementById('search-container');
                    if (!searchContainer.contains(e.originalEvent.target)) {
                        if (!this.isZoomEnabled) {
                            // Habilita el zoom con scroll solo en el primer clic
                            this.map.scrollWheelZoom.enable();
                            this.isZoomEnabled = true; // Marca que el zoom ya está habilitado
                        } else {
                            // Maneja la adición o actualització del marcador en el segundo clic
                            if (!this.marker) {
                                this.marker = L.marker(e.latlng).addTo(this.map);
                            } else {
                                this.marker.setLatLng(e.latlng);
                            }
                            this.updateHiddenInput(e.latlng.lat, e.latlng.lng);
                        }
                    }
                });
                this.createSearchUI();
            }
            document.getElementById(this.dom.id).style.zIndex = '10';

            L.tileLayer(tiles, {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(this.map);



        }

        loadCSS() {
            const css = `
                #map {
                    height: 100vh;
                    width: 100%;
                    position: relative;
                }


                #search-container {
                    position: absolute;
                    top: 10px;
                    left: 60px;
                    z-index: 1000;
                    display: flex;
                    align-items: center;
                    background: white;
                    padding: 5px;
                    border-radius: 4px;
                    box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.3);
                }

                #search-input {
                    width: 200px;
                    border: none;
                    outline: none;
                    padding: 5px;
                    font-size: 14px;
                }

                #search-button {
                    background: transparent;
                    border: none;
                    cursor: pointer;
                    margin-left: 5px;
                }

                #search-button img {
                    width: 20px;
                    height: 20px;
                }

                #suggestions {
                    position: absolute;
                    top: 35px;
                    left: 0;
                    z-index: 1001;
                    background: white;
                    width: 100%;
                    max-height: 150px;
                    overflow-y: auto;
                    border-radius: 4px;
                    box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.3);
                    display: none;
                }

                #suggestions ul {
                    list-style: none;
                    padding: 0;
                    margin: 0;
                }

                #suggestions li {
                    padding: 5px;
                    cursor: pointer;
                }

                #suggestions li:hover {
                    background-color: #f0f0f0;
                }
            `;

            const style = document.createElement('style');
            style.appendChild(document.createTextNode(css));
            document.head.appendChild(style);
        }

        createSearchUI() {
            const searchContainer = document.createElement('div');
            searchContainer.id = 'search-container';

            const searchInput = document.createElement('input');
            searchInput.type = 'text';
            searchInput.id = 'search-input';
            searchInput.placeholder = 'Search for an address...';

            const searchButton = document.createElement('button');
            searchButton.id = 'search-button';
            const searchIcon = document.createElement('img');
            searchIcon.src = 'https://cdn-icons-png.flaticon.com/512/622/622669.png';
            searchIcon.alt = 'Search';
            searchButton.appendChild(searchIcon);

            const suggestions = document.createElement('div');
            suggestions.id = 'suggestions';
            const suggestionsList = document.createElement('ul');
            suggestionsList.id = 'suggestions-list';
            suggestions.appendChild(suggestionsList);

            searchContainer.appendChild(searchInput);
            searchContainer.appendChild(searchButton);
            searchContainer.appendChild(suggestions);

            this.dom.appendChild(searchContainer);

            // Prevent form submission on Enter key press
            searchInput.onkeydown = (e) => {
                if (e.key === 'Enter') {
                    e.preventDefault(); // Prevent the form from submitting
                    this.searchAddress(searchInput.value, suggestionsList);
                }
            };

            // Prevent form submission on search button click
            searchButton.onclick = (e) => {
                e.preventDefault(); // Prevent the form from submitting
                this.searchAddress(searchInput.value, suggestionsList);
            };
        }

        searchAddress(query, suggestionsList) {
            suggestionsList.innerHTML = '<li>Cargando...</li>';
            const suggestionsBox = suggestionsList.parentElement;
            suggestionsBox.style.display = 'block';

            fetch(`https://nominatim.openstreetmap.org/search?q=${query}&format=json&addressdetails=1`)
                .then(response => response.json())
                .then(data => {
                    suggestionsList.innerHTML = '';
                    data.forEach((location) => {
                        const listItem = document.createElement('li');
                        listItem.textContent = location.display_name;
                        listItem.dataset.lat = location.lat;
                        listItem.dataset.lon = location.lon;
                        listItem.onclick = () => {
                            this.setMarker(location.lat, location.lon);
                            this.updateHiddenInput(location.lat, location.lon);
                            document.getElementById('search-input').value = location.display_name;
                            suggestionsBox.style.display = 'none';
                        };
                        suggestionsList.appendChild(listItem);
                    });
                })
                .catch(error => {
                    console.error('Error al buscar la dirección:', error);
                    suggestionsList.innerHTML = '<li>Error en la búsqueda</li>';
                });
        }

        setMarker(lat, lon) {
            if (this.marker) {
                this.map.removeLayer(this.marker);
            }

            this.marker = L.marker([lat, lon]).addTo(this.map);
            this.map.setView([lat, lon], 15);
        }


        /**
         * Update the hidden input field with the new coordinates
         * @param {number} lat Latitude
         * @param {number} lng Longitude
         */
        updateHiddenInput(lat, lng) {
            this.hiddenInput.value = `${lat}, ${lng}`;
            // trigger an event to emulate a standard input[type=text] element
            this.hiddenInput.dispatchEvent(new Event('input'));
        }
    }

    // Register this control for the following types & text subtypes
    controlClass.register('map', controlMap);
});
