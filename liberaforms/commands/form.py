"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import click
from flask.cli import AppGroup
from flask.cli import with_appcontext
from liberaforms.models.form import Form

form_cli = AppGroup('form')


@form_cli.command()
@click.argument("form_id")
@with_appcontext
def disable(form_id):
    form = Form.find(id=form_id)
    if not form:
        click.echo("Form not found")
        return
    form.admin_preferences['public'] = False
    form.save()
    click.echo(f"Form disabled: id={form.id}, '{form.slug}'")


@form_cli.command()
@click.argument("form_id")
@with_appcontext
def enable(form_id):
    form = Form.find(id=form_id)
    if not form:
        click.echo("Form not found")
        return
    form.admin_preferences['public'] = True
    form.save()
    click.echo(f"Form enabled: id={form.id}, '{form.slug}'")
