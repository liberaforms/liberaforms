"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import flask_login
import flask_babel
from flask import current_app, g
from liberaforms.models.invite import Invite
from liberaforms.models.consent import Consent
from liberaforms.utils import i18n
from liberaforms.utils import sanitizers


class CustomText():
    text_name: str = ""
    user_lang_code: str = ""
    browser_lang_codes: list = []
    custom_texts: dict = {}
    default_is_asset = ["frontpage-blurb", "wizard-disclaimer"]  # see ASSETS_DIR
    markdown_texts = ["frontpage-blurb", "wizard-disclaimer"]  # {markdown/html} texts

    def __init__(self, text_name: str):

        self.text_name = text_name
        if flask_login.current_user and \
           flask_login.current_user.is_authenticated and \
           flask_login.current_user.preferences["language"] in g.site.custom_languages:
            self.user_lang_code = flask_login.current_user.preferences["language"]
        self.browser_lang_codes = i18n.parse_request_accept_language()

        if self.text_name == "frontpage-blurb":
            self.custom_texts = g.site.blurb['front_page']
        elif self.text_name == "other-information":
            self.custom_texts = g.site.contact_info["languages"]
        elif self.text_name == "resources-menu":
            self.custom_texts = g.site.resources_menu["languages"]
        elif self.text_name == "invitation-text":
            self.custom_texts = g.site.invitation_text
        elif self.text_name == "wizard-disclaimer":
            self.custom_texts = g.site.data_protection["disclaimer"]["languages"]
        elif self.text_name == "new-form-msg":
            self.custom_texts = g.site.new_form_msg["languages"]

    #def __str__(self):
    #    from liberaforms.utils import utils
    #    """Use for debugging."""
    #    return utils.print_obj_values(self)

    def _get_asset_path(self, lang_code: str) -> str:
        if self.text_name == "frontpage-blurb":
            return os.path.join(current_app.config['ASSETS_DIR'],
                                "blurb",
                                f"front-page.{lang_code}.md")
        if self.text_name == "wizard-disclaimer":
            return os.path.join(current_app.config['ASSETS_DIR'],
                                "gdpr-wizard",
                                f"disclaimer.{lang_code}.md")
        return ""

    def _get_custom_text(self, lang_code):
        if lang_code not in self.custom_texts:
            return ""
        if self.text_name in self.markdown_texts and \
           self.custom_texts[lang_code]["markdown"] or \
           self.custom_texts[lang_code]:
            return self.custom_texts[lang_code]
        return ""

    def get_text(self, with_lang_code: str = "", as_html=False):

        def _get_text(lang_code, as_html=False):
            custom_text = self._get_custom_text(lang_code)
            if custom_text:
                if self.text_name in self.markdown_texts and custom_text["markdown"]:
                    if self.text_name == "wizard-disclaimer":
                        md = Consent.parse_disclaimer_md(custom_text["markdown"])
                        custom_text["markdown"] = md
                        custom_text["html"] = sanitizers.markdown_to_html(md)
                    return custom_text["html"] if as_html else md
                return custom_text
            # return default
            if self.text_name in self.default_is_asset:
                asset_file = self._get_asset_path(lang_code)
                if os.path.exists(asset_file):
                    with open(asset_file, 'r', encoding="utf-8") as default_text:
                        default_markdown = default_text.read()
                    if self.text_name == "wizard-disclaimer":
                        default_markdown = Consent.parse_disclaimer_md(default_markdown)
                    if as_html:
                        return sanitizers.markdown_to_html(default_markdown)
                    return default_markdown
            if self.text_name == "invitation-text":
                # default translations are babel gettext strings
                return Invite.default_message(lang_code)
            return ""

        if with_lang_code:
            return _get_text(with_lang_code, as_html=as_html)
        if self.user_lang_code:
            text = _get_text(self.user_lang_code, as_html=as_html)
            if text:
                return text
        if g.site.language in self.browser_lang_codes:
            text = _get_text(g.site.language, as_html=as_html)
            if text:
                return text
        for lang_code in self.browser_lang_codes:
            if lang_code in g.site.custom_languages:
                text = _get_text(lang_code, as_html=as_html)
                if text:
                    return text
        text = _get_text(g.site.language, as_html=as_html)
        if text:
            return text
        for lang_code in g.site.custom_languages:
            text = _get_text(lang_code, as_html=as_html)
            if text:
                return text
        return ""

    def get_completion(self) -> dict:
        total_translations = 0
        for lang_code in g.site.custom_languages:
            defined = False
            if lang_code in self.custom_texts and self._get_custom_text(lang_code):
                defined = True
            if not defined and self.text_name in self.default_is_asset and \
               os.path.exists(self._get_asset_path(lang_code)):
                defined = True
            if not defined and self.text_name == "invitation-text":
                # default translations are babel gettext strings
                app_default_lang = g.site.newuser_language
                if lang_code == app_default_lang or \
                   Invite.default_message(app_default_lang) != Invite.default_message(lang_code):
                    defined = True
            if defined:
                total_translations += 1
        total_languages = len(g.site.custom_languages)
        return {
            "completed": total_translations == total_languages,
            "stats": f"{total_translations} / {total_languages}"
        }
