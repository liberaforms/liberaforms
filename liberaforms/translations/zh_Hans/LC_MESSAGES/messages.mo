��          |               �   1   �   U     -   e     �     �  5   �  _   �  k   Z  T   �  t     x   �  �  	     �  &   �     �  
   �     �     �  )     /   6  "   f  7   �  2   �   Attached to one form Attached to %(number)s forms Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Deleted one answer Deleted %(number)s answers Map marker Map markers One byte %(number)s bytes One pending invitation %(number)s pending invitations This form has already been answered once. This form has already been answered %(number)s times. This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2025-02-13 11:08+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Automatically generated
Language: zh_Hans
Language-Team: none
Plural-Forms: nplurals=1; plural=0;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
 Attached to one form Deleted '%(form_name)s' and one answer Deleted one answer Map marker One byte One pending invitation This form has already been answered once. This form has been posted on the Fediverse once You are going to delete one answer You are going to delete this form and its unique answer Your form %(form_name)s is shared with one person. 