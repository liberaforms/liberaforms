��    3      �              L     M     S  1   [     �     �     �     �  	   �     �     �  U   �  -        G     O     T     [     c     i     q     y          �     �     �     �     �     �     �     �     �     �     �     �     �     �  5        G     N     S  
   X  _   c  k   �     /     5     :     C     I  T   Q  t   �  x       �     �	  
   �	  N   �	     
     
     &
     -
  
   4
  	   ?
     I
  �   O
  H   �
          )     0  
   9     D  
   Q     \     e  	   l  
   v     �     �  
   �     �     �     �     �  "   �     �     �     �     �  *     T   /  	   �     �     �  
   �  �   �  �   >  	   �     �     �     	       �     �   �  �   V   Admin Answers Attached to one form Attached to %(number)s forms Author Average Cancel Change Configure Created Delete Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Deleted one answer Deleted %(number)s answers Details Edit Editor Editors Email Enabled Expired False Form Forms Graphs Guest Installation Invitations Label Language Login Map marker Map markers Maximum Media Minimum Name One byte %(number)s bytes One pending invitation %(number)s pending invitations Public Role Save Statistics This form has already been answered once. This form has already been answered %(number)s times. This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times Total True Username Users Version You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2025-02-13 11:08+0100
PO-Revision-Date: 2024-05-15 09:01+0000
Last-Translator: LiberaForms <info@liberaforms.org>
Language: pl
Language-Team: Polish <https://hosted.weblate.org/projects/liberaforms/server-liberaforms/pl/>
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
 Administrator Odpowiedzi Attached to one form Attached to %(number)s forms Attached to %(number)s forms Autor Średnia Anuluj Zmień Konfiguruj Utworzono Usuń Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Deleted '%(form_name)s' and %(number)s answers Deleted one answer Deleted %(number)s answers Deleted %(number)s answers Szczegóły Edytuj Redakcja Redaktorzy Adres e-mail Włączone Wygasło Fałsz Formularz Formularze Wykresy Gość Instalacja Zaproszenia Etykieta Język Zaloguj się Map marker Map markers Map markers Maksimum Pliki Minimum Nazwa One byte %(number)s bytes %(number)s bytes One pending invitation %(number)s pending invitations %(number)s pending invitations Publiczne Funkcja Zapisz Statystyki This form has already been answered once. This form has already been answered %(number)s times. This form has already been answered %(number)s times. This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times This form has been posted on the Fediverse %(number)s times Całość Prawda Nazwa uzytkownika Użytkownicy Wersja You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers You are going to delete this form and its %(number)s answers Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Your form %(form_name)s is shared with %(people_count)s other people. 