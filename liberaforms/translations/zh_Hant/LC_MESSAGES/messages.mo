��                        �  1   �          0  U   <  -   �     �     �     �     �     �               '     6     C     G     U     l     u     ~  5   �     �     �  _   �  k   J  T   �  t     x   �    �     �          $  &   1     X  	   k     u     �     �     �     �     �     �     �     �     �  
   �     �  	   	     	     	     6	     C	  )   P	  /   z	  "   �	  7   �	  2   
   Attached to one form Attached to %(number)s forms Change form name Delete form Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Deleted one answer Deleted %(number)s answers Editors Expiration conditions Form configuration Form fields Form invitation Form options Form preview Form templates Introduction Log Look and feel Map marker Map markers My forms New form One byte %(number)s bytes One pending invitation %(number)s pending invitations Share answers The form's QR This form has already been answered once. This form has already been answered %(number)s times. This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2025-02-13 11:08+0100
PO-Revision-Date: 2024-06-30 09:09+0000
Last-Translator: hugoalh <hugoalh@users.noreply.hosted.weblate.org>
Language: zh_Hant
Language-Team: Chinese (Traditional) <https://hosted.weblate.org/projects/liberaforms/server-liberaforms/zh_Hant/>
Plural-Forms: nplurals=1; plural=0;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
 Attached to one form 變更表單名稱 刪除表單 Deleted '%(form_name)s' and one answer Deleted one answer 編輯器 有效期限條件 表單配置 表單欄位 表單邀請 表單選項 表單預覽 表單範本 介紹 日誌 外觀和感覺 Map marker 我的表單 新表單 One byte One pending invitation 分享答案 表單的 QR This form has already been answered once. This form has been posted on the Fediverse once You are going to delete one answer You are going to delete this form and its unique answer Your form %(form_name)s is shared with one person. 