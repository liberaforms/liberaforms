"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import shutil
import datetime
import json
from flask import current_app, Blueprint, jsonify
from flask import g, request, render_template, redirect, url_for
import flask_login

from liberaforms import csrf
from liberaforms.domain import form as form_domain
from liberaforms.models.form import Form
from liberaforms.models.schemas.consent import ConsentSchema
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.utils import utils
from liberaforms.utils import sanitizers
from liberaforms.utils import validators
from liberaforms.utils import i18n

public_form_bp = Blueprint('public_form_bp', __name__,
                           template_folder='../templates/public_form')


def get_temp_attachment_dir(identifier):
    return os.path.join(current_app.config['TMP_DIR'], identifier)


@public_form_bp.route('/embed/<string:slug>', methods=['GET', 'POST'])
@csrf.exempt
@sanitizers.sanitized_slug_required
def view_embedded_form(slug):
    flask_login.logout_user()
    g.embedded = True
    return view(slug=slug)


@public_form_bp.route('/<string:slug>', methods=['GET', 'POST'])
@public_form_bp.route('/<string:slug>/edit/<string:edition_id>', methods=['GET', 'POST'])
@sanitizers.sanitized_slug_required
def view(slug, edition_id=None):
    """The public form."""

    form = Form.find(slug=slug)

    unavaiable_url = form_domain.get_unavaiable_url(form, request, edition_id)
    if unavaiable_url:
        return unavaiable_url

    previous_answer = None
    if edition_id and validators.is_valid_UUID(edition_id):
        previous_answer = Answer.find(public_id=edition_id)
        if request.method == 'GET' and not previous_answer:
            return render_template('main/page-not-found.html'), 404

    if request.method == 'POST':
        submitted_data = request.form.to_dict(flat=False)
        if 'plain-answer' in submitted_data.keys():
            plain_answer = json.loads(submitted_data['plain-answer'][0])
            submitted_data = {**submitted_data, **plain_answer}
            submitted_data.pop('plain-answer')
        if 'e2ee-answer' in submitted_data.keys():
            submitted_data['e2ee-answer'] = submitted_data['e2ee-answer'][0]

        expected_field_names = form.get_expected_field_names_on_submit(is_edition=bool(previous_answer))

        if len(submitted_data.keys()) > len(expected_field_names):
            current_app.logger.info(f"FORM {request.remote_addr} - '{slug}', submitted fields are greater than expected fields. Answers not saved.")
            return redirect(url_for('public_form_bp.view', slug=slug))

        # Populate the answer with the submitted data
        answer_data = {}
        for key, value in submitted_data.items():
            if key == 'csrf_token' or \
               key == 'identifier' or \
               key == 'uploaded-file-fields' or \
               key == 'send-confirmation':
                continue
            if key.startswith("dummy-file") and previous_answer:
                # find the file attachment in previous answer and populate answer_data
                field_name = submitted_data[key][0]
                attachment = AnswerAttachment.find(answer_id=previous_answer.id,
                                                   field_name=field_name)
                if attachment:
                    answer_data[field_name] = attachment.get_link()
                continue
            answer_data[key] = value

        if previous_answer and form.is_anon_edition_enabled():
            answer = previous_answer
            answer.save_previous_answer()
            answer.previous_id = answer.editions[0].id
            answer.data = answer_data
            answer.updated = datetime.datetime.now(datetime.timezone.utc)
            previous_answer = answer.editions[0]
        else:
            answer = Answer(form.id, form.author_id, answer_data)
        answer.save()

        if form.has_file_field() and \
           "identifier" in submitted_data and \
           "uploaded-file-fields" in submitted_data and \
           validators.is_valid_UUID(submitted_data["identifier"][0]) and \
           isinstance(json.loads(submitted_data["uploaded-file-fields"][0]), list):
            # Save the attachments already uploaded before this final form submission

            tmp_dir = get_temp_attachment_dir(submitted_data["identifier"][0])
            uploaded_fields = json.loads(submitted_data["uploaded-file-fields"][0])
            if os.path.exists(tmp_dir) or bool(previous_answer):  # a previous_answer is possibly only deleting files
                form_domain.save_submitted_files(
                        form,
                        answer,
                        uploaded_fields,
                        tmp_dir,
                        bool(previous_answer),
                )
                shutil.rmtree(tmp_dir, ignore_errors=True)
            elif not previous_answer and uploaded_fields:
                # file uploads must be present when not editing a previous answer
                current_app.logger.warning("FORM - Missing answer temp file dir")

        if form.is_anon_edition_enabled() and not answer.public_id:
            answer.public_id = utils.gen_random_uuid()
            answer.save()

        # Send emails and log
        form_domain.notify_public_form_submission(
            form,
            answer,
            bool(previous_answer),
            # confirmation not sent / when submitted_data == {"e2ee-answers": .... }
            send_confirmation=form.confirmation_field_name in submitted_data
        )

        if not edition_id:
            if not form.expired and form.has_expired():
                form_domain.expire_form(form)

        after_submit_text = form.get_after_submit_text_html()
        edition_link = form.get_edit_answer_link(answer) if form.is_anon_edition_enabled() else None
        return jsonify(thankyou=render_template('public_form/partials/thankyou.html',
                                                form=form,
                                                after_submit_text=after_submit_text,
                                                edition_link=edition_link)), 200

    previous_answer_data = previous_answer.data if previous_answer else None
    if g.embedded:
        submit_url = url_for('public_form_bp.view_embedded_form', slug=slug)
    else:
        submit_url = url_for('public_form_bp.view', slug=slug, edition_id=edition_id)
    return render_template('public_form/view.html',
                           form=form,
                           submit_url=submit_url,
                           identifier=form.gen_identifier(),
                           opengraph=form.get_opengraph(),
                           no_bot=True,
                           iso_lang_code=i18n.get_iso_lang_code(g.language),
                           consents=ConsentSchema(many=True).dump(form.get_consents()),
                           human_readable_bytes=utils.human_readable_bytes,
                           previous_answer=previous_answer_data)


@public_form_bp.route('/embed/form/<string:slug>/upload-file', methods=['POST'])
@csrf.exempt
@sanitizers.sanitized_slug_required
def upload_file_embedded_form(slug):
    flask_login.logout_user()
    g.embedded = True
    return upload_file(slug=slug)


@public_form_bp.route('/form/<string:slug>/upload-file', methods=['POST'])
@sanitizers.sanitized_slug_required
def upload_file(slug):
    """Upload a form attachment."""

    form = Form.find(slug=slug)
    if form_domain.get_unavaiable_url(form, request, None):
        return jsonify(error="form is unavaiable"), 404

    if not request.files:
        return jsonify(error="expected a file"), 400

    data = request.form.to_dict(flat=False)
    if not ("identifier" in data.keys() and validators.is_valid_UUID(data["identifier"][0])):
        return jsonify(error="No identifier")
    identifier = data["identifier"][0]

    field_name = list(request.files.keys())[0]
    if not (field_name.startswith("file-") and form.has_field(field_name)):
        return jsonify(error="not a valid field name"), 400

    file = request.files[field_name]
    if not (file.filename and
            file.content_type and
            (form.is_e2ee or validators.is_valid_mimetype(file, g.site.mimetypes['mimetypes']))):
        return jsonify(error="not a valid file"), 400

    file_dir = get_temp_attachment_dir(identifier)
    os.makedirs(file_dir, exist_ok=True)
    try:
        with open(f"{file_dir}/{field_name}.txt", "w", encoding="utf-8") as fo:
            fo.write(file.filename)
        file.save(f"{file_dir}/{field_name}.file")
    except Exception as error:
        current_app.logger.error(f"Cannot save tmp form attachment : {error}")
        return jsonify(error="Cannot save tmp form attachment"), 500

    return jsonify(msg="saved file"), 200
