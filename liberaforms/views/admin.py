"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from datetime import datetime, timezone
from flask import current_app, g, request, render_template, redirect
from flask import flash, Blueprint, url_for, jsonify
from flask import send_file, after_this_request
from flask_babel import gettext as _
import flask_login
from liberaforms.domain.storage import StorageDomain
from liberaforms.domain import form as form_domain
from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.invite import Invite
from liberaforms.models.schemas.invite import InviteSchema
from liberaforms.models.schemas.consent import ConsentSchema
from liberaforms.utils import auth
from liberaforms.utils import utils
from liberaforms.utils import chart_data
from liberaforms.utils.dispatcher import Dispatcher
from liberaforms.utils.exports import write_users_csv
from liberaforms.utils import tokens
from liberaforms.utils import wtf
from liberaforms.utils import i18n
from liberaforms.utils import form_helper
from liberaforms.utils import password as password_utils

# from pprint import pprint

admin_bp = Blueprint('admin_bp', __name__,
                     template_folder='../templates/admin')


@admin_bp.route('/admin', methods=['GET'])
@auth.enabled_admin_required
def admin_panel():
    return render_template('admin-panel.html',
                           user=flask_login.current_user,
                           custom_languages=i18n.get_custom_languages(),
                           StorageDomain=StorageDomain,
                           goto=request.args.get("goto", None))

# User management

@admin_bp.route('/admin/users', methods=['GET'])
@auth.enabled_admin_required
def list_users():
    return render_template('list-users.html',
                           users=User.find_all(),
                           invites=Invite.find_all().count())

@admin_bp.route('/admin/user/<int:user_id>', methods=['GET'])
@flask_login.login_required
@auth.enabled_admin_required
def inspect_user(user_id):
    user = User.find(id=user_id)
    if not user:
        flash(_("User not found"), 'warning')
        return redirect(url_for('admin_bp.list_users'))
    return render_template('inspect-user.html',
                           user=user,
                           has_invited_users=User.find_all(invited_by_id=user.id),
                           chart_data=chart_data.get_user_statistics(user),
                           human_readable_bytes=utils.human_readable_bytes)


@admin_bp.route('/admin/user/<int:user_id>/set-role/<string:role>', methods=['POST'])
@auth.enabled_admin_required__json
def set_role(user_id, role):
    user = User.find(id=user_id)
    if not user:
        return jsonify("Not found"), 404
    if role not in ('guest', 'editor', 'admin'):
        return jsonify("Invalid role"), 406
    if user.username != flask_login.current_user.username:
        # current_user cannot remove their own admin permission
        if user.set_role(role):
            user.save()
            admin_name = flask_login.current_user.username
            current_app.logger.info(f'USER - Admin "{admin_name}" changed user "{user.username}" role: {role}')
    return jsonify(role=user.role,
                   can_create_forms=user.can_create_forms(),
                   can_toggle_new_forms=user.enabled and user.role != "guest"), 200



@admin_bp.route('/admin/user/<int:user_id>/toggle-user-form-creation', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_user_form_creation(user_id):
    user = User.find(id=user_id)
    if not user:
        return jsonify("Not found"), 404
    can_toggle = user.enabled and user.role != "guest"
    if not can_toggle:
        return jsonify(can_create_forms=False,
                       can_toggle_new_forms=can_toggle), 200
    can_create_forms = user.toggle_can_create_forms()
    admin_name = flask_login.current_user.username
    if can_create_forms:
        current_app.logger.info(f'USER - Admin "{admin_name}" enabled form creation for user "{user.username}"')
    else:
        current_app.logger.info(f'USER - Admin "{admin_name}" disabled form creation for user "{user.username}"')
    return jsonify(can_create_forms=can_create_forms,
                   can_toggle_new_forms=can_toggle), 200


@admin_bp.route('/admin/user/<int:user_id>/toggle-blocked', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_user_blocked(user_id):
    user = User.find(id=user_id)
    if not user:
        return jsonify("Not found"), 404
    if user.id == flask_login.current_user.id:
        # current_user cannot disable themself
        return jsonify(blocked=False,
                       can_create_forms=user.can_create_forms(),
                       can_toggle_new_forms=user.enabled and user.role != "guest"), 200
    blocked = user.toggle_blocked()
    admin_name = flask_login.current_user.username
    if blocked:
        current_app.logger.info(f'USER - Admin "{admin_name}" blocked user "{user.username}"')
    else:
        current_app.logger.info(f'USER - Admin "{admin_name}" unblocked user "{user.username}"')
    return jsonify(blocked=blocked,
                   can_create_forms=user.can_create_forms(),
                   can_toggle_new_forms=user.enabled and user.role != "guest"), 200


@admin_bp.route('/admin/user/<int:user_id>/toggle-uploads-enabled', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_uploads_enabled(user_id):
    user = User.find(id=user_id)
    if not user:
        return jsonify("Not found"), 404
    return jsonify(uploads_enabled=user.toggle_uploads_enabled()), 200


@admin_bp.route('/admin/user/<int:user_id>/set-upload-limit', methods=['GET', 'POST'])
@auth.enabled_admin_required
def set_user_upload_limit(user_id):
    user = User.find(id=user_id)
    if not user:
        flash(_("User not found"), 'warning')
        return redirect(url_for('admin_bp.list_users'))
    wtform = wtf.UserUploadLimit()
    if wtform.validate_on_submit():
        result = f"{wtform.size.data.strip()} {wtform.unit.data.strip()}"
        bytes_limit: int = utils.string_to_bytes(result)
        user.set_uploads_limit(bytes_limit)
        user.set_disk_usage_alert()
        user.save()
        flash(_("Uploads limit updated OK"), 'success')
        return redirect(url_for('admin_bp.inspect_user', user_id=user.id))
    if request.method == 'GET':
        bytes_str = utils.human_readable_bytes(user.uploads_limit)
        wtform.size.data = float(bytes_str[:-3])
        wtform.unit.data = bytes_str[-2:]
    default = [current_app.config['DEFAULT_USER_UPLOADS_LIMIT'][:-3],
               current_app.config['DEFAULT_USER_UPLOADS_LIMIT'][-2:]]
    return render_template('user-upload-limit.html',
                           user=user, default=default, wtform=wtform)


@admin_bp.route('/admin/user/<int:user_id>/change-password', methods=['GET', 'POST'])
@auth.enabled_admin_required
def change_user_password(user_id):
    user = User.find(id=user_id)
    if not user:
        flash(_("User not found"), 'warning')
        return redirect(url_for('admin_bp.list_users'))
    if request.method == 'POST' and 'username' in request.form:
        if user.username == request.form['username']:
            password = utils.gen_random_uuid()
            user.password_hash = password_utils.hash_password(password)
            user.save()
            admin_name = flask_login.current_user.username
            current_app.logger.info(f'USER - Admin "{admin_name}" changed user "{user.username}" password')
            return render_template('change-user-password.html',
                                   user=user,
                                   new_password=password)
        flash(_("Username does not match"), 'warning')
    return render_template('change-user-password.html', user=user)


@admin_bp.route('/admin/user/<int:user_id>/send-unique-email', methods=['GET', 'POST'])
@auth.enabled_admin_required
def send_unique_email(user_id):
    wtform = wtf.SendUniqueEmail()
    user = User.find(id=user_id)
    if not user:
        flash(_("User not found"), 'warning')
        return redirect(url_for('admin_bp.list_users'))
    subject = _("Confirmation email")
    sent_body = ""
    if wtform.validate_on_submit():
        message = wtform.text.data.replace("[id]", utils.gen_random_uuid())
        wtform.text.data = message
        status = Dispatcher().send_message([user.email],
                                           subject,
                                           message,
                                           bcc=[flask_login.current_user.email])
        if status['email_sent']:
            sent_body = utils.nl2br(message)
            flash(_("We have sent an email to %(email)s", email=user.email), 'success')
        else:
            flash(_("Sorry, something went wrong."), 'error')
    if request.method == 'GET' and not wtform.text.data:
        message = _("Hello %(username)s,\n\nPlease forward this email to %(email)s\n\nRegards.\n[id]",
                    username=user.username,
                    email=flask_login.current_user.email)
        wtform.text.data = message
    return render_template('send-unique-email.html',
                           user=user,
                           wtform=wtform,
                           subject=subject,
                           sent_body=sent_body)


@admin_bp.route('/admin/user/<int:user_id>/delete', methods=['GET', 'POST'])
@auth.enabled_admin_required
def delete_user(user_id):
    user = User.find(id=user_id)
    if not user:
        flash(_("User not found"), 'warning')
        return redirect(url_for('admin_bp.list_users'))
    if request.method == 'POST' and 'username' in request.form:
        if user.is_root_user():
            flash(_("Cannot delete root user"), 'warning')
            return redirect(url_for('admin_bp.inspect_user', user_id=user.id))
        if user.id == flask_login.current_user.id:
            flash(_("Cannot delete yourself"), 'warning')
            return redirect(url_for('admin_bp.inspect_user', user_id=user.id))
        if user.username == request.form['username']:
            user.delete()
            admin_name = flask_login.current_user.username
            current_app.logger.info(f'USER - Admin "{admin_name}" deleted user "{user.username}"')
            flash(_("Deleted user '%(name)s'", name=user.username), 'success')
            return redirect(url_for('admin_bp.list_users'))
        flash(_("Username does not match"), 'warning')
    return render_template('delete-user.html', user=user)


@admin_bp.route('/admin/user/<int:user_id>/stats', methods=['GET'])
@auth.enabled_admin_required__json
def get_user_usage_graph_data(user_id):
    user = User.find(id=user_id)
    if not user:
        return jsonify("Not found"), 404
    try:
        offset = int(request.args.get("year-offset", 0))
        return jsonify(chart_data=chart_data.get_user_statistics(user, offset_year=offset))
    except:
        current_app.logger.debug("APP - Failed to get user usage graph data")
        return jsonify("Not acceptable"), 406


@admin_bp.route('/admin/users/csv', methods=['GET'])
@auth.enabled_admin_required
def csv_users():
    csv_file = write_users_csv(User.find_all())

    @after_this_request
    def remove_file(response):
        os.remove(csv_file)
        return response
    return send_file(csv_file, mimetype="text/csv", as_attachment=True)


#  Form management

@admin_bp.route('/admin/forms', methods=['GET'])
@auth.enabled_admin_required
def list_forms():
    return render_template('list-forms.html')


@admin_bp.route('/admin/form/<int:form_id>', methods=['GET'])
@flask_login.login_required
@auth.enabled_admin_required
@auth.instantiate_form()
@form_domain.expire_on_date_condition
def inspect_form(form_id, **kwargs):
    form = kwargs["form"]
    return render_template('admin/form-details.html',
                           form=form,
                           FormUser=FormUser,
                           viewer_role="admin",
                           Invite=Invite,
                           human_readable_bytes=utils.human_readable_bytes,
                           status_badges=form_helper.status_badges)


# ## Previews

@admin_bp.route('/admin/form/<int:form_id>/preview', methods=['GET'])
@auth.enabled_admin_required
@auth.instantiate_form()
def preview_form(form_id, **kwargs):
    form = kwargs['form']
    max_attach_size = utils.human_readable_bytes(current_app.config['MAX_ATTACHMENT_SIZE'])
    consents = form.get_consents()
    return render_template('form/preview-form.html',
                           form=form,
                           consents=ConsentSchema(many=True).dump(consents),
                           upload_media_form=wtf.UploadMedia(),
                           human_readable_bytes=utils.human_readable_bytes,
                           viewer_role="admin",
                           go_back_url=url_for('admin_bp.inspect_form', form_id=form.id),
                           max_attach_size=max_attach_size)


@admin_bp.route('/admin/form/<int:form_id>/preview/thank-you-page', methods=['GET'])
@auth.enabled_admin_required
@auth.instantiate_form()
def preview_thankyou(form_id, **kwargs):
    form = kwargs['form']
    after_submit_text = form.get_after_submit_text_html()
    edition_link = None
    edition_link = form.get_edit_answer_link() if form.is_anon_edition_enabled() else None
    return render_template('form/preview-thankyou.html',
                           form=form,
                           after_submit_text=after_submit_text,
                           edition_link=edition_link,
                           viewer_role="admin",
                           go_back_url=url_for('admin_bp.inspect_form', form_id=form.id))


@admin_bp.route('/admin/form/<int:form_id>/preview/expired-page', methods=['GET'])
@auth.enabled_admin_required
@auth.instantiate_form()
def preview_expired(form_id, **kwargs):
    form = kwargs['form']
    return render_template('form/preview-expired.html',
                           form=form,
                           viewer_role="admin",
                           go_back_url=url_for('admin_bp.inspect_form', form_id=form.id))


@admin_bp.route('/admin/forms/<int:form_id>/disable', methods=['POST'])
@auth.enabled_admin_required
@auth.instantiate_form()
def disable_form(form_id, **kwargs):
    form = kwargs["form"]
    is_disabled = not form.toggle_admin_form_public()
    if is_disabled:
        flash_text = _("The form has been disabled")
    else:
        flash_text = _("The form has been enabled")
    flash(flash_text, 'success')
    return redirect(url_for('admin_bp.inspect_form', form_id=form.id))


#  Invitations

@admin_bp.route('/admin/invites', methods=['GET'])
@auth.enabled_admin_required
def list_invites():
    invites = InviteSchema(many=True).dump(Invite.find_all())
    return render_template('site/list-invites.html', invites=invites)


@admin_bp.route('/admin/invites/new', methods=['GET', 'POST'])
@auth.enabled_admin_required
def new_invite():
    wtform = wtf.UserInvite()
    wtform.language.choices = i18n.get_language_select_choices()
    if request.method == 'POST' and not wtform.message.data:
        wtform.message.data = Invite.default_message(wtform.language.data)
    if wtform.validate_on_submit():
        invite = Invite.find(email=wtform.email.data)
        if invite:
            flash_text = _("An invitation has already been sent to %(email)s", email=invite.email)
            flash(flash_text, 'warning')
            return redirect(url_for('admin_bp.new_invite'))
        token = tokens.create_token()
        url = Invite.get_invitation_url(token["token"])
        message = Invite.parse_message(wtform.message.data, invite_url=url)
        invite = Invite(email=wtform.email.data,
                        message=message,
                        token=token,
                        role=wtform.role.data,
                        invited_by_id=flask_login.current_user.id)
        invite.save()
        status = Dispatcher().send_invitation(invite)
        if status['email_sent'] is True:
            invite.last_sent = datetime.now(timezone.utc)
            invite.save()
            flash_text = _("We have sent an invitation to %(email)s",
                           email=invite.email)
            flash(flash_text, 'success')
            return redirect(url_for('admin_bp.list_invites'))
        invite.delete()
        flash(status['msg'], 'warning')
    if request.method == 'GET':
        wtform.language.data = g.language
        wtform.role.data = "editor"
    if Invite.find_all().count():
        go_back_url = url_for('admin_bp.list_invites')
    else:
        go_back_url = url_for('admin_bp.list_users')
    return render_template('admin/new-invite.html',
                           wtform=wtform,
                           templates=g.site.invitation_text,
                           previews=Invite.previews(),
                           placeholders=Invite.placeholders(),
                           go_back_url=go_back_url)


@admin_bp.route('/invite/send-again', methods=['POST'])
@auth.enabled_admin_required
def send_invite_again():
    try:
        invite_id = int(request.form['invite_id'])
    except:
        flash(_("Opps! We cannot find that invitation"), 'error')
        return redirect(url_for('admin_bp.list_invites'))
    invite = Invite.find(id=invite_id)
    if invite and not invite.granted_form:
        status = Dispatcher().send_invitation(invite)
        if status['email_sent'] is True:
            invite.last_sent = datetime.now(timezone.utc)
            invite.save()
            invite.renew_token_life()
            flash_text = _("We have sent the invitation to %(email)s again", email=invite.email)
            flash(flash_text, 'success')
        else:
            flash(status['msg'], 'warning')
    else:
        flash(_("Opps! We cannot find that invitation"), 'error')
    return redirect(url_for('admin_bp.list_invites'))


@admin_bp.route('/admin/invite/<int:invite_id>/delete', methods=['GET'])
@auth.enabled_admin_required
@auth.instantiate_invite(as_admin=True)
def delete_invite(invite_id, **kwargs):
    invite = kwargs["invite"]
    invite.delete()
    return redirect(url_for('admin_bp.list_invites'))


# Personal Admin preferences

@admin_bp.route('/admin/toggle-newuser-notification', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_newuser_notification():
    return jsonify(notify=flask_login.current_user.toggle_new_user_notification()), 200


@admin_bp.route('/admin/toggle-newform-notification', methods=['POST'])
@auth.enabled_admin_required__json
def toggle_newform_notification():
    return jsonify(notify=flask_login.current_user.toggle_new_form_notification()), 200


# ROOT_USER functions

@admin_bp.route('/admin/form/<int:form_id>/change-author', methods=['GET', 'POST'])
@auth.rootuser_required
@auth.instantiate_form()
def change_author(form_id, **kwargs):
    form = kwargs["form"]
    if request.method == 'POST':
        author = form.author
        if not ('old_author_username' in request.form and
                request.form['old_author_username'] == author.username):
            flash(_("Current author incorrect"), 'warning')
            return render_template('change-author.html', form=form)
        if 'new_author_username' in request.form:
            new_author = User.find(username=request.form['new_author_username'])
            if new_author:
                if new_author.enabled:
                    old_author = author
                    if form.change_author(new_author):
                        form_user = FormUser.find(form_id=form_id, user_id=new_author.id)
                        if form_user:
                            form_user.is_editor = True
                        else:
                            form_user = FormUser(form=form, user=new_author, is_editor=True)
                        form_user.save()
                        log_text = _("Changed author from %(old_user)s to %(new_user)s",
                                     old_user=old_author.username,
                                     new_user=new_author.username)
                        form.add_log(log_text)
                        flash(_("Changed author OK"), 'success')
                        return redirect(url_for('admin_bp.inspect_form', form_id=form.id))
                else:
                    flash(_("Cannot use %(username)s. The user is not enabled",
                          username=request.form['new_author_username']), 'warning')
            else:
                flash(_("Cannot find username %(name)s",
                      name=request.form['new_author_username']), 'warning')
    return render_template('change-author.html', form=form)
