"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import traceback
from flask_wtf.csrf import CSRFError
from flask import Blueprint, render_template, redirect, request, jsonify
from flask import current_app, url_for
import flask_login
from liberaforms.utils.dispatcher import Dispatcher

errors_bp = Blueprint('errors_bp',
                      __name__,
                      template_folder='../templates/main')


@errors_bp.app_errorhandler(404)
def page_not_found(error):
    if request.path.endswith("/") and request.method == 'GET':
        # sometimes users accidentally append '/' to a form slug
        # redirect just in case this happens
        return redirect(url_for('public_form_bp.view', slug=request.path.strip("/")))
    if flask_login.current_user.is_authenticated:
        username = flask_login.current_user.username
        current_app.logger.info(f'404 {request.remote_addr} - User "{username}". Page not found: {request.path}')
    else:
        current_app.logger.info(f'404 {request.remote_addr} - Anonymous user. Page not found: {request.path}')
    return render_template('page-not-found.html'), 404


@errors_bp.app_errorhandler(405)
def method_not_allowed(error):
    if flask_login.current_user.is_authenticated:
        username = flask_login.current_user.username
        current_app.logger.info(f'APP {request.remote_addr} - Method {request.method} not allowed. "{username}", {request.path}')
    else:
        current_app.logger.info(f'APP {request.remote_addr} - Method {request.method} not allowed. Anonymous user, {request.path}')
    if request.is_json:
        return jsonify(error="Method not allowed"), 405
    return render_template('method-not-allowed.html'), 405


@errors_bp.app_errorhandler(500)
def server_error(error):
    Dispatcher().send_error(traceback.format_exc())
    current_app.logger.error(f'500 {request.remote_addr} - Server Error: {error}')
    return render_template('server-error.html'), 500


@errors_bp.app_errorhandler(CSRFError)
def csrf_error(error):
    path = request.full_path if len(request.args) else request.path
    if flask_login.current_user.is_authenticated:
        current_app.logger.warning(f'CSRF-ERROR - "{flask_login.current_user.username}" posted {path}')
    else:
        current_app.logger.warning(f'CSRF-ERROR - Anonymous user posted {path}')
    return render_template('csrf-error.html', requested_page=path), 500
