"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from functools import wraps
from flask import Blueprint, jsonify
from flask import g, request, render_template, redirect, url_for
from flask_babel import gettext as _

from liberaforms.models.form import Form
from liberaforms.models.schemas.answer import AnswerSchema
from liberaforms.utils import utils
from liberaforms.utils import sanitizers
from liberaforms.utils import validators
from liberaforms.utils import auth


public_answers_bp = Blueprint('public_answers_bp', __name__,
                              template_folder='../templates/public_answers')


def return_404(return_404_as):
    if return_404_as == "json":
        return jsonify("Not found"), 404
    return render_template('main/page-not-found.html'), 404


def return_error_as(content_type):
    # I do this because Flask's request.get_json() should return None
    # when Content-Type is "application/json", but doesn't always work. ?
    # Maybe when we update Flask.
    def decorator(f):
        @wraps(f)
        def wrap(*args, **kwargs):
            kwargs['content_type'] = content_type
            return f(*args, **kwargs)
        return wrap
    return decorator


def instantiate_form(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        form = None
        if "slug" in kwargs:
            form = Form.find(slug=kwargs['slug'])
        if "form_id" in kwargs:
            form = Form.find(id=kwargs['form_id'])
        if not form:
            return return_404(kwargs["content_type"])
        kwargs['form'] = form
        return f(*args, **kwargs)
    return wrap


def can_be_published(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        form = kwargs["form"]
        if not(
               form.author.is_active()
               and form.admin_preferences['public']
               and form.is_map_enabled()
               and not form.is_e2ee
           ):
            return return_404(kwargs["content_type"])
        return f(*args, **kwargs)
    return wrap


@public_answers_bp.route('/<string:slug>/map', methods=['GET'])
@sanitizers.sanitized_slug_required
@return_error_as("html")
@instantiate_form
@can_be_published
def as_map(slug, **kwargs):
    form = kwargs["form"]
    return render_template('public_answers/map.html',
                           endpoint=url_for("public_answers_bp.map_json_data",
                                            form_id=form.id,
                                            _external=True)), 200


@public_answers_bp.route('/form/<string:form_id>/map-json', methods=['GET'])
@return_error_as("json")
@instantiate_form
@can_be_published
def map_json_data(form_id, **kwargs):
    form = kwargs["form"]

    def populate_marker_popup(map_field_name, answer_data) -> list:
        try:
            popup_fields = form.preferences["map"]["markers"][map_field_name]["popup_fields"]
        except:
            return []
        popup: list = []
        for popup_field in popup_fields:
            field_answer = ""
            if popup_field in answer_data:
                field_answer = form.get_field_answer(answer_data, popup_field).strip()
            if not field_answer:
                field_answer = f"<i>{_('No data')}</i>"
            popup.append({
                "label": form.get_field_label(popup_field),
                "value": field_answer
            })
        return popup

    first_map = form.get_map_fields()[0]
    map_data = {
        "meta" : {
            "tiles": first_map["tiles"],
            "centerPoint": first_map["centerPoint"],
            "zoom": first_map["zoom"],
        },
        "markers": []
    }

    answers = AnswerSchema(many=True).dump(form.answers)
    for map_field in form.get_map_fields():
        map_field_name = map_field["name"]
        for answer in answers:
            if not map_field_name in answer["data"]:
                continue
            if validators.is_valid_map_coords(answer["data"][map_field_name]):
                map_data["markers"].append({
                    "coords": answer["data"][map_field_name].replace(' ','').split(','),
                    "popup": populate_marker_popup(map_field_name, answer["data"]),
                })
    return jsonify(map_data), 200
