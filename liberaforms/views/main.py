"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import Blueprint
from flask import render_template
from liberaforms.domain.custom_text import CustomText

main_bp = Blueprint('main_bp',
                    __name__,
                    template_folder='../templates/main')


@main_bp.route('/', methods=['GET'])
def index():
    page_text = CustomText("frontpage-blurb").get_text(as_html=True)
    if not isinstance(page_text, str):
        # blurb has not been translated
        page_text = CustomText("frontpage-blurb").get_text(
            with_lang_code='en',
            as_html=True
        )
    return render_template('index.html', page_text=page_text)


@main_bp.route('/site/error', methods=['GET'])
def server_error():
    return render_template('server-error.html')
