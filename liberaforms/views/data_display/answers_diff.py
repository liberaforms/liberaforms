"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import Blueprint, jsonify
from flask_babel import gettext as _
import flask_login
from liberaforms.models.user import User
from liberaforms.models.answer import Answer
from liberaforms.utils import auth
from liberaforms.utils import utils

answers_diff_bp = Blueprint('answers_diff_bp', __name__)


@answers_diff_bp.route('/data-display/form/<int:form_id>/answers-diff/<int:answer_id>/editions/<int:edition_id>', methods=['GET'])
@flask_login.login_required
@auth.enabled_editor_required
@auth.instantiate_form(allow_admin=False)
def answers_diff(form_id, answer_id, edition_id, **kwargs):
    """Display an Answer's edition history."""

    form = kwargs["form"]
    form_user = kwargs["form_user"]
    answer = Answer.find(id=answer_id, form_id=form.id)

    if not (answer and answer.editions):
        return jsonify(False)

    def dump_edition(obj):
        if obj.__class__.__name__ == "Answer":
            created = utils.stringify_date_time(obj.updated)
            author = None
        else:
            created = utils.stringify_date_time(obj.created)
            author = _("An anonymous user")
            user = User.find(id=obj.user_id)
            if user:
                avatar_src = user.get_avatar_src()
                author = f"<span class='ds-avatar'><img src='{avatar_src}' alt='' class='rounded-circle' />{user.username}</span>"
            else:
                author = _("Anonymous user")

        return {"id": obj.id,
                "created": created,
                "author": author,
                "data": obj.data}

    editions = [dump_edition(answer)]
    for edition in answer.editions:
        editions.append(dump_edition(edition))

    # The Answer class (editions[0]) does not have a property for saving the author of an edition
    # The author is instead saved to the most recent AnswerEdition in answer.editions (grrr)
    # That means we have to move each author up in the list of editions
    for pos in list(range(len(editions)-1)):
        editions[pos]["author"] = editions[pos+1]["author"]
    editions[len(editions)-1]["author"] = _("Anonymous user")

    field_index = form_user.get_field_index_preference()
    default_field_index = form.get_field_index_for_display()


    return jsonify(
        items=editions,
        meta={
            'name': 'Answer edtition',
            'data_type': 'answer-diff',
            'deleted_fields': [],
            'default_field_index': default_field_index,
            'can_edit': False,
            'selected_edition_id': edition_id,
            'form_structure': form.structure,
        },
        user_prefs={
            'field_index': field_index if field_index else default_field_index,
            'order_by': 'created',
            'ascending': 'ascending'
        }
    ), 200
