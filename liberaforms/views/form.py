"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
import datetime
from urllib.parse import urlparse
import markupsafe
from sqlalchemy.orm.attributes import flag_modified
from flask import current_app, Blueprint
from flask import g, request, render_template, redirect, url_for, jsonify
from flask import flash, send_file
from flask_babel import ngettext, gettext as _
import flask_login

from liberaforms.domain import form as form_domain
from liberaforms.domain.custom_text import CustomText
from liberaforms.models.form import Form
from liberaforms.models.invite import Invite
from liberaforms.models.schemas.invite import InviteSchema
from liberaforms.models.schemas.consent import ConsentSchema
from liberaforms.models.schemas.formuser import FormUserSchema
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from liberaforms.models.formauth import FormAuth
from liberaforms.form_templates import form_templates
from liberaforms.utils import utils
from liberaforms.utils import tokens
from liberaforms.utils import auth
from liberaforms.utils import form_helper
from liberaforms.utils import sanitizers
from liberaforms.utils import validators
from liberaforms.utils import html_parser
from liberaforms.utils import ldap
from liberaforms.utils.dispatcher.dispatcher import Dispatcher
from liberaforms.utils import wtf
from liberaforms.utils import i18n

# from pprint import pprint

form_bp = Blueprint('form_bp', __name__,
                    template_folder='../templates/form')


@form_bp.route('/forms', methods=['GET'])
@flask_login.login_required
@auth.enabled_user_required
def my_forms():
    #user_forms = FormUser.find_all(user_id=flask_login.current_user.id)
    #if flask_login.current_user.is_guest():
    #    if user_forms.count() == 0:
    #        return redirect(url_for('user_bp.user_settings'))
    #    if user_forms.count() == 1:
    #        granted_form = user_forms[0].form
    #        return redirect(url_for('answers_bp.list_answers', form_id=granted_form.id))
    return render_template('my-forms.html')


@form_bp.route('/form/new', methods=['GET', 'POST'])
@form_bp.route('/form/new/template/<int:template_id>', methods=['GET', 'POST'])
@form_bp.route('/form/new/duplicate/<int:duplicate_id>', methods=['GET', 'POST'])
@auth.enabled_editor_required
def create_new_form(template_id=None, duplicate_id=None):
    wtform = wtf.NewForm()
    wtform.is_e2ee.render_kw = {
        'disabled': current_app.config['E2EE_MODE'].mode == 'REQUIRED',
    }
    if template_id:
        template = next((sub for sub in form_templates.templates if sub['id'] == template_id), None)
        if not template:
            flash(_("Cannot find that form template"), 'error')
            return redirect(url_for('form_bp.list_templates'))
    else:
        template = None
    if duplicate_id:
        duplicate_form = Form.find(id=duplicate_id)
        if not duplicate_form:
            flash(_("Cannot find the form to duplicate"), 'error')
            return redirect(url_for('form_bp.my_forms'))
        if not (flask_login.current_user.is_admin() or
                FormUser.find(form_id=duplicate_form.id,
                              user_id=flask_login.current_user.id,
                              is_editor=True)):
            flash(_("You can't duplicate that form"), 'info')
            return redirect(url_for('form_bp.my_forms'))
    else:
        duplicate_form = None
    if wtform.validate_on_submit():
        if not flask_login.current_user.can_create_forms():
            return redirect(url_for('form_bp.create_new_form'))
        is_e2ee = True if current_app.config['E2EE_MODE'].is_required else wtform.is_e2ee.data
        if is_e2ee and not flask_login.current_user.e2ee_public_key:
            return redirect(url_for('form_bp.create_new_form'))
        kwargs = {
            'name': wtform.name.data,
            'slug': wtform.slug.data,
            'is_e2ee': is_e2ee
        }
        if template:
            markdown = i18n.lazytext_to_serializable(template['introduction_md'])
            structure = form_helper.repair_form_structure(template['structure'])
            template_args = {
                "introduction_text": {
                             "markdown": markdown,
                             "html": sanitizers.markdown_to_html(markdown)},
                "structure": i18n.lazytext_to_serializable(structure)
            }
            kwargs = {**kwargs, **template_args}
        try:
            form = Form(flask_login.current_user, **kwargs)
            if duplicate_form:
                excluded_props = [
                    '_sa_instance_state',
                    'id',
                    'created',
                    'author_id',
                    'name',
                    'slug',
                    'enabled',
                    'is_e2ee',
                    'e2ee_public_key',
                    'published_cnt',
                    'edit_mode',
                ]
                for prop in vars(duplicate_form):
                    if prop not in excluded_props:
                        setattr(form, prop, getattr(duplicate_form, prop))
            form.save()
        except Exception as error:
            current_app.logger.error(error)
            flash(_("Failed to create form"), 'warning')
            return redirect(url_for('form_bp.create_new_form'))
        FormUser(form=form, user=flask_login.current_user, is_editor=True).save()
        current_app.logger.info(f'FORM - User "{form.author.username}" created form {form.id}, "{form.slug}"')
        form.add_log(_("Form created"))
        Dispatcher().send_new_form_notification(form)  # notify admins
        if g.site.new_form_msg["enabled"]:
            messages = g.site.new_form_msg["languages"]
            #flash(markupsafe.Markup(i18n.get_best_language_value(messages)), 'info')
            flash(markupsafe.Markup(CustomText("new-form-msg").get_text()), 'info')

        else:
            flash(_("New form created ok"), 'success')
        return redirect(url_for('form_bp.inspect_form', form_id=form.id))
    form_source_msg = None
    if template:
        form_source_msg = _('You are creating a copy of the template "%(name)s"',
                            name=template["name"])
    if duplicate_form:
        form_source_msg = _('You are creating a copy of the form "%(name)s"',
                            name=duplicate_form.name)
    wtform.is_e2ee.data = current_app.config['E2EE_MODE'].default_form_value
    return render_template('new-form.html',
                           wtform=wtform,
                           form_source_msg=form_source_msg)


@form_bp.route('/forms/check-slug-availability', methods=['POST'])
@auth.enabled_editor_required__json
def is_slug_available():
    if 'slug' in request.form and request.form['slug']:
        slug = request.form['slug']
    else:
        return jsonify(slug="",
                       available=False,
                       error_msg=_("A slug is required")), 200
    # we return a sanitized slug as a suggestion to the user.
    slug = sanitizers.sanitize_slug(slug)
    if len(slug) < 5 or len(slug) > 50:
        return jsonify(slug=slug,
                       available=False,
                       error_msg=_("Min. 5 and max. 100 characters")), 200
    if not Form.is_slug_available(slug):
        return jsonify(slug=slug,
                       available=False,
                       error_msg=_("This URL is not available. Try another!")), 200
    return jsonify(slug=slug, available=True), 200


@form_bp.route('/form/<int:form_id>', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False, allow_reader=True)
@form_domain.expire_on_date_condition
def inspect_form(form_id, **kwargs):
    form = kwargs["form"]
    if kwargs["is_form_reader"]:
        return redirect(url_for('answers_bp.list_answers', form_id=form.id))
    kwargs["form_user"].save_ui_preference('landing_page', 'config')
    consents = ConsentSchema(many=True).dump(form.get_consents())
    return render_template('form-configuration.html',
                           form=form,
                           form_user=kwargs["form_user"],
                           goto=request.args.get("goto", None),
                           consents=consents,
                           upload_media_form=wtf.UploadMedia(),
                           human_readable_bytes=utils.human_readable_bytes)


@form_bp.route('/form/<int:form_id>/options', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
@form_domain.expire_on_date_condition
def form_details(form_id, **kwargs):
    form = kwargs["form"]
    kwargs["form_user"].save_ui_preference('landing_page', 'details')
    return render_template('form-options.html',
                           form=form,
                           FormUser=FormUser,
                           form_user=kwargs["form_user"],
                           is_form_editor=kwargs["is_form_editor"],
                           Invite=Invite,
                           human_readable_bytes=utils.human_readable_bytes,
                           status_badges=form_helper.status_badges)


@form_bp.route('/form/<int:form_id>/edit', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def edit_form(form_id, **kwargs):
    form = kwargs['form']
    if form.edit_mode and form.edit_mode['editor_id'] != flask_login.current_user.id:
        flash(_("The form is being edited by %(email)s",
                email=form.edit_mode["editor_email"]), 'warning')
        return redirect(url_for('form_bp.form_details', form_id=form.id))
    editing_preview = bool(request.args.get('preview') and form.preview)
    if editing_preview:
        preview_structure = form.preview["structure"]
        form.preview = {}
        form.save()
        form.structure = preview_structure
    else:
        form.start_edit_mode()
    return render_template('edit-form.html',
                           upload_media_form=wtf.UploadMedia(),
                           human_readable_bytes=utils.human_readable_bytes,
                           iso_lang_code=i18n.get_iso_lang_code(g.language),
                           form=form,
                           is_form_editor=True,
                           cancel_edit_mode_alert=form.id,
                           editing_preview=editing_preview)


@form_bp.route('/form/<int:form_id>/cancel-edit-mode/<int:killed>', methods=['POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def cancel_edition(form_id, killed, **kwargs):
    form = kwargs["form"]
    form.edit_mode = {}
    form.preview = {}
    form.save()
    if killed:  # edition has been cancelled via the alert modal
        flash(_("Edit mode cancelled"), 'success')
        return redirect(url_for('form_bp.form_details', form_id=form.id))
    return redirect(url_for('form_bp.inspect_form', form_id=form.id))


@form_bp.route('/form/<int:form_id>/change-name', methods=['GET', 'POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def change_name(form_id, **kwargs):
    form = kwargs['form']
    wtform = wtf.FormName()
    if wtform.validate_on_submit():
        if wtform.name.data != form.name:
            form.name = wtform.name.data
            form.save()
            form.add_log(_("Changed the name of the form"))
            return redirect(url_for('form_bp.form_details', form_id=form.id))
    if request.method == 'GET':
        wtform.name.data = form.name
    return render_template('form-name.html', form=form, wtform=wtform)


@form_bp.route('/form/<int:form_id>/save', methods=['POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def save_form(form_id, **kwargs):
    form = kwargs["form"]

    def failed_save():
        sorry = _("Sorry, something went wrong.")
        try_again = _("Try starting again.")
        flash(f'{sorry} {try_again}', 'warning')
        form.edit_mode = {}
        form.preview = {}
        form.save()
        return redirect(url_for('form_bp.inspect_form', form_id=form.id))

    if form.edit_mode and form.edit_mode['editor_id'] != flask_login.current_user.id:
        flash(_("The form is being edited by %(email)s",
                email=form.edit_mode["editor_email"]), 'warning')
        return redirect(url_for('form_bp.form_details', form_id=form.id))

    save_preview = bool(form.preview and form.edit_mode)  # the user was previewing changes
    if save_preview:
        structure = form.preview["structure"]
    else:
        if 'structure' not in request.form:
            current_app.logger.warning(f"Structure missing. Form: {form.id}, Posted from: {request.referrer}")
            return failed_save()
        try:
            structure = json.loads(request.form['structure'])
            structure = form_helper.repair_form_structure(structure, form=form)
        except Exception as error:
            current_app.logger.error(error)
            return failed_save()
    if form.edit_mode and form.edit_mode['editor_id'] == flask_login.current_user.id:
        form.auto_enable_confirmation(structure)
        form.structure = structure
        form.update_field_index(Form.create_field_index(structure))
        form.update_expiry_conditions()
        form.edit_mode = {}
        form.preview = {}
        form.save()
        # reset formusers' field_index order preference
        FormUser.find_all(form_id=form.id).update({
            FormUser.field_index: None,
            FormUser.order_by: None
        })
        form.add_log(_("Form edited"))
        flash(_("Saved form OK"), 'success')
        return redirect(url_for('form_bp.inspect_form', form_id=form.id))
    return failed_save()


@form_bp.route('/form/<int:form_id>/delete', methods=['GET', 'POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def delete_form(form_id, **kwargs):
    form = kwargs['form']
    if request.method == 'POST' and 'slug' in request.form:
        if form.slug == request.form['slug']:
            answer_cnt = form.get_answers().count()
            form.delete()
            username = flask_login.current_user.username
            current_app.logger.info(f'FORM - User "{username}" deleted form {form.id} and ({answer_cnt}) answers, "{form.slug}"')
            flash_text = ngettext(
                            "Deleted '%(form_name)s' and one answer",
                            "Deleted '%(form_name)s' and %(number)s answers",
                            answer_cnt,
                            form_name=form.name, number=answer_cnt)
            flash(flash_text, 'success')
            return redirect(url_for('form_bp.my_forms'))
    return render_template('delete-form.html', form=form)


@form_bp.route('/form/<int:form_id>/edition/preview', methods=['POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def preview_edition(form_id, **kwargs):
    form = kwargs['form']
    if 'preview_structure' not in request.form:
        redirect(url_for('form_bp.edit_form', form_id=form.id))
    try:
        structure = json.loads(request.form['preview_structure'])
        structure = form_helper.repair_form_structure(structure, form=form)
    except Exception as error:
        current_app.logger.error(error)
        sorry = _("Sorry, something went wrong.")
        try_again = _("Try starting again.")
        flash(f'{sorry} {try_again}', 'warning')
        form.edit_mode = {}
        form.preview = {}
        form.save()
        return redirect(url_for('form_bp.inspect_form', form_id=form.id))
    form.auto_enable_confirmation(structure)
    form.preview = {"structure": structure}
    form.save()
    max_attach_size = utils.human_readable_bytes(current_app.config['MAX_ATTACHMENT_SIZE'])
    consents = form.get_consents()
    return render_template('preview-form.html',
                           previewing_edition=True,
                           form=form,
                           consents=ConsentSchema(many=True).dump(consents),
                           upload_media_form=wtf.UploadMedia(),
                           human_readable_bytes=utils.human_readable_bytes,
                           is_form_editor=kwargs["is_form_editor"],
                           max_attach_size=max_attach_size)


# ## Previews

@form_bp.route('/form/<int:form_id>/preview', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False, allow_reader=True)
def preview(form_id, **kwargs):
    form = kwargs['form']
    form_user = kwargs['form_user']
    max_attach_size = utils.human_readable_bytes(current_app.config['MAX_ATTACHMENT_SIZE'])
    consents = form.get_consents()
    viewer_role = "editor" if form_user.is_editor else "reader"
    return render_template('preview-form.html',
                           form=form,
                           consents=ConsentSchema(many=True).dump(consents),
                           upload_media_form=wtf.UploadMedia(),
                           human_readable_bytes=utils.human_readable_bytes,
                           viewer_role=viewer_role,
                           go_back_url=form_user.get_landing_page(),
                           max_attach_size=max_attach_size)


@form_bp.route('/form/<int:form_id>/preview/thank-you', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False, allow_reader=True)
def preview_thankyou(form_id, **kwargs):
    form = kwargs['form']
    form_user = kwargs['form_user']
    after_submit_text = form.get_after_submit_text_html()
    edition_link = form.get_edit_answer_link() if form.is_anon_edition_enabled() else None
    viewer_role = "editor" if form_user.is_editor else "reader"
    return render_template('preview-thankyou.html',
                           #is_page=True,
                           form=form,
                           after_submit_text=after_submit_text,
                           edition_link=edition_link,
                           viewer_role=viewer_role,
                           go_back_url=form_user.get_landing_page())


@form_bp.route('/form/<int:form_id>/preview/expired', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False, allow_reader=True)
def preview_expired(form_id, **kwargs):
    form = kwargs['form']
    form_user = kwargs['form_user']
    viewer_role = "editor" if form_user.is_editor else "reader"
    return render_template('preview-expired.html',
                           form=form,
                           viewer_role=viewer_role,
                           go_back_url=form_user.get_landing_page())


@form_bp.route('/form/<int:form_id>/style', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def look_and_feel(form_id, **kwargs):
    form = kwargs['form']
    form_user = kwargs['form_user']
    max_attach_size = utils.human_readable_bytes(current_app.config['MAX_ATTACHMENT_SIZE'])
    consents = form.get_consents()
    return render_template('form/look-and-feel.html',
                           form=form,
                           consents=ConsentSchema(many=True).dump(consents),
                           upload_media_form=wtf.UploadMedia(),
                           human_readable_bytes=utils.human_readable_bytes,
                           go_back_url=form_user.get_landing_page(),
                           max_attach_size=max_attach_size)


# ## Settings UI preferences

@form_bp.route('/form/<int:form_id>/toggle-ui-preference', methods=['POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def toggle_card_visibility(form_id, **kwargs):
    if 'preference_key' in request.form and 'is_visible' in request.form:
        is_visible = utils.str2bool(request.form['is_visible'])
        preference_key = request.form['preference_key']
        if preference_key in FormUser.default_ui_prefs().keys():
            form_user = kwargs['form_user']
            form_user.ui_preferences[preference_key] = is_visible
            form_user.save()
            return jsonify(visible=form_user.ui_preferences[preference_key]), 200
    return jsonify("Not Acceptable"), 406


# ## Data consent

@form_bp.route('/form/<int:form_id>/set-requires-consent', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form(allow_admin=False)
def set_requires_consent(form_id, **kwargs):
    form = kwargs['form']
    if 'requires_consent_bool' in request.form:
        form.requires_consent = utils.str2bool(request.form['requires_consent_bool'])
        form.add_log(_("Personal data set to: %(bool)s", bool=form.requires_consent))
        form.save()
        return jsonify(is_public=form.is_public()), 200
    return jsonify("Not acceptable"), 406


# ## Form texts

@form_bp.route('/form/<int:form_id>/save-introduction-text', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def save_introduction_text(form_id, **kwargs):
    form = kwargs['form']
    if 'markdown' in request.form:
        form.save_introduction_text(request.form['markdown'])
        form.add_log(_("Edited Introduction text"))
        pay_load = {'html': form.introduction_text["html"],
                    'markdown': form.introduction_text["markdown"]}
        return jsonify(pay_load), 200
    return jsonify(html="", markdown=""), 406


@form_bp.route('/form/<int:form_id>/save-after-submit-text', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def save_after_submit_text(form_id, **kwargs):
    form = kwargs['form']
    if 'markdown' in request.form:
        form.save_after_submit_text(request.form['markdown'])
        form.add_log(_("Edited Thankyou text"))
        pay_load = {'html': form.get_after_submit_text_html(),
                    'markdown': form.get_after_submit_text_markdown()}
        return jsonify(pay_load), 200
    user_id = flask_login.current_user.id
    msg = f"Failed to save After submit text. user.id:{user_id} form.id:{form_id}"
    current_app.logger.warning(msg)
    pay_load = {'html': "<h1>%s</h1>" % _("An error occured"), 'markdown': ""}
    return jsonify(pay_load), 200


@form_bp.route('/form/<int:form_id>/save-expired-text', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def save_expired_text(form_id, **kwargs):
    form = kwargs['form']
    if 'markdown' in request.form:
        form.save_expired_text(request.form['markdown'])
        form.add_log(_("Edited expiry text"))
        pay_load = {'html': form.get_expired_text_html(),
                    'markdown': form.get_expired_text_markdown()}
        return jsonify(pay_load), 200
    user_id = flask_login.current_user.id
    msg = f"Failed to save Expired text. user.id:{user_id} form.id:{form_id}"
    current_app.logger.warning(msg)
    pay_load = {'html': "<h1>%s</h1>" % _("An error occured"), 'markdown': ""}
    return jsonify(pay_load), 200


@form_bp.route('/form/<int:form_id>/fediverse-publish', methods=['GET', 'POST'])
@auth.enabled_user_required
@auth.instantiate_form()
def fedi_publish(form_id, **kwargs):
    form = kwargs['form']
    if not flask_login.current_user.fedi_auth:
        flash(_("Fediverse connect is not configured"), 'warning')
        return redirect(url_for('form_bp.inspect_form', form_id=form.id))
    wtform = wtf.FormPublish()
    if wtform.validate_on_submit():
        status = Dispatcher().publish_form(wtform.text.data,
                                           wtform.image_source.data,
                                           fediverse=True)
        if status['published'] is True:
            form.published_cnt += 1
            form.save()
            status_uri = status['msg']
            # i18n: variable is a Fediverse node. Example: Published at barcelona.social
            flash(_("Published at %(node_name)s", node_name=status_uri), 'success')
        else:
            flash(status['msg'], 'warning')
        return redirect(url_for('form_bp.inspect_form', form_id=form_id))
    if request.method == 'GET':
        html = form.introduction_text['html']
        text = html_parser.extract_text(html, with_links=True).strip('\n')
        wtform.text.data = f"{form.url}\n\n{text}"
        wtform.image_source.data = form.thumbnail
    fedi_auth = flask_login.current_user.get_fedi_auth()
    return render_template('fedi-publish.html',
                           connection_title=flask_login.current_user.fedi_connection_title(),
                           node_name=urlparse(fedi_auth['node_url']).netloc,
                           form=form,
                           wtform=wtform)


#
# Adding editors and read-only users are very similar, we share most code
#
@form_bp.route('/form/<int:form_id>/editors', methods=['GET'])
@form_bp.route('/form/<int:form_id>/editors/<string:with_email>', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def form_editors(form_id, with_email=None, **kwargs):
    return list_form_users(with_email, sharing_as_editor=True, **kwargs)


@form_bp.route('/form/<int:form_id>/readers', methods=['GET'])
@form_bp.route('/form/<int:form_id>/readers/<string:with_email>', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def form_readers(form_id, with_email=None, **kwargs):
    return list_form_users(with_email, sharing_as_editor=False, **kwargs)


def list_form_users(with_email, sharing_as_editor, **kwargs):
    form = kwargs['form']
    invites = Invite.find_all(
        granted_form=form.id,
        granted_role='editor' if sharing_as_editor else 'reader'
    )
    form_users = FormUser.find_all(form_id=form.id, is_editor=sharing_as_editor)
    return render_template('form-users.html',
                           form=form,
                           form_users=FormUserSchema(many=True).dump(form_users),
                           FormUser=FormUser,
                           invites=InviteSchema(
                               many=True,
                               only=('id', 'last_sent', 'message', 'email')
                           ).dump(invites),
                           wtform=wtf.GetEmail(),
                           invitation_email=with_email,
                           sharing_as_editor=sharing_as_editor,
                           current_formuser=kwargs['form_user'])


@form_bp.route('/form/<int:form_id>/user-permissions/<string:user_email>', methods=['GET'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def form_user_permissions(form_id, user_email=None, **kwargs):
    form = kwargs['form']
    queried_user = User.find(email=user_email)
    res, res_code = form.get_user_permissions(queried_user, user_email=user_email)
    return jsonify(res), res_code


@form_bp.route('/form/<int:form_id>/add-editor', methods=['POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def add_editor(form_id, **kwargs):
    return form_user_add(form_id, sharing_as_editor=True, **kwargs)


@form_bp.route('/form/<int:form_id>/add-reader', methods=['POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def add_reader(form_id, **kwargs):
    return form_user_add(form_id, sharing_as_editor=False, **kwargs)


def form_user_add(form_id, sharing_as_editor, **kwargs):
    redirect_destination = 'form_bp.form_editors' if sharing_as_editor else 'form_bp.form_readers'
    form = kwargs['form']
    if form.is_e2ee and not kwargs['form_user'].is_e2ee_enabled():
        return redirect(url_for(redirect_destination, form_id=form.id))
    wtform = wtf.GetEmail()
    if wtform.validate_on_submit():
        # TOOD: CHECK AND COMPARE PERMISSIONS
        if Invite.find(granted_form=form.id, email=wtform.email.data):
            flash(_("An invitation has already been sent to %(email)s", email=wtform.email.data), 'warning')
            return redirect(url_for(redirect_destination, form_id=form.id))
        new_user_with_access = User.find(email=wtform.email.data)
        if not new_user_with_access:
            return redirect(url_for(
                    redirect_destination,
                    form_id=form.id,
                    with_email=wtform.email.data,  # display a modal to create an invite
            ))
        if new_user_with_access.blocked:
            flash(_("That user has been disabled by an Admin"), 'warning')
            return redirect(url_for(redirect_destination, form_id=form.id))
        form_user = FormUser.find(form_id=form.id, user_id=new_user_with_access.id)
        if form_user:
            if form_user.is_editor:
                flash(_("%(email)s already has Editor permissions", email=new_user_with_access.email), 'warning')
            else:
                #if sharing_as_editor:
                #    # Upgrade user's permissions
                #    form_user.is_editor = True
                #    form_user.save()
                #    flash(_("Upgraded permissions for %(email)s", email=new_user_with_access.email), 'success')
                #else:
                flash(_("The answers are already shared with %(email)s", email=new_user_with_access.email), 'warning')
            return redirect(url_for(redirect_destination, form_id=form.id))
        e2ee_kwargs = {}
        if form.is_e2ee and new_user_with_access.e2ee_public_key:
            try:
                public_key_data = json.loads(request.form.get('e2ee_ciphered_key_backup', ''))
                e2ee_kwargs = {
                    'e2ee_ciphered_key_backup': public_key_data
                }
                # We want to ensure these fields, so we can later on detect a
                # need for key restore/rotations
                assert set(public_key_data.keys()) == set([
                    "user_key_fingerprint",
                    "form_key_fingerprint",
                    "form_public_key",
                    "ciphered_key_backup",
                ])
            except:
                # i18n: Did not receive a valid, encrypted backup of the key for USER
                flash(_("Did not receive a valid, encrypted key backup for %(email)s", email=new_user_with_access.email), 'warning')
                return redirect(url_for(redirect_destination, form_id=form.id))
        try:
            FormUser(
                form=form,
                user=new_user_with_access,
                is_editor=sharing_as_editor,
                **e2ee_kwargs
            ).save()
            log_message = "Added editor %(email)s" if sharing_as_editor else "Added read only user %(email)s"
            form.add_log(_(log_message, email=new_user_with_access.email))
        except Exception as error:
            current_app.logger.error(error)
            flash(_("Could not add the user"), 'error')
    return redirect(url_for(redirect_destination, form_id=form.id))


@form_bp.route('/form/<int:form_id>/remove-editor/<int:editor_id>', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json()
def remove_editor(form_id, editor_id, **kwargs):
    return form_user_remove(form_id, editor_id, sharing_as_editor=True, **kwargs)

@form_bp.route('/form/<int:form_id>/remove-reader/<int:reader_id>', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json()
def remove_reader(form_id, reader_id, **kwargs):
    return form_user_remove(form_id, reader_id, sharing_as_editor=False, **kwargs)

def form_user_remove(form_id, user_id, sharing_as_editor, **kwargs):
    form = kwargs['form']
    if form.author_id == user_id:
        return jsonify(False), 200
    form_user = FormUser.find(form_id=form.id, user_id=user_id, is_editor=sharing_as_editor)
    if form_user:
        removed_user = form_user.user
        form_user.delete()
        log_message = "Removed editor %(email)s" if sharing_as_editor else "Removed user %(email)s"
        form.add_log(_(log_message, email=removed_user.email))
        return jsonify(str(removed_user.id)), 200
    return jsonify(False), 404

@form_bp.route('/form/<int:form_id>/invite/<string:grant_as>/<string:email>', methods=['GET', 'POST'])
@auth.enabled_user_required
@auth.instantiate_form()
def new_invite(form_id, grant_as, email, **kwargs):
    """Send an invite to create a new user with permission on this form."""

    form = kwargs['form']
    if grant_as not in ['editor', 'reader']:
        flash("Not valid permission", 'warning')
        return redirect(url_for('form_bp.form_inspect', form_id=form.id))

    redirect_destination = 'form_bp.form_editors' if grant_as == 'editor' else 'form_bp.form_readers'
    wtform = wtf.FormUserInvite()
    if request.method == 'POST' and not wtform.message.data:
        wtform.message.data = Invite.default_message(wtform.language.data)
    user_role = "guest" if g.site.invitation_only else "editor"
    if wtform.validate_on_submit():
        if Invite.find(granted_form=form.id, email=wtform.email.data):
            flash(_("An invitation has already been sent to %(email)s", email=wtform.email.data), 'warning')
            return redirect(url_for(redirect_destination, form_id=form.id))

        ldap_uuid = None
        if current_app.config['ENABLE_LDAP']:
            conn, msg = ldap.bind()
            results, msg = ldap.search(conn, wtform.email.data)
            ldap.unbind(conn)
            if msg['status'] == 1:
                ldap_uuid = results[0]['entryUUID']
                user_role = "editor"
        token = tokens.create_token()
        url = Invite.get_invitation_url(token["token"])
        message = Invite.parse_message(wtform.message.data, invite_url=url)
        invite = Invite(
                        email=wtform.email.data,
                        message=message,
                        token=token,
                        role=user_role,
                        granted_form={
                            'id': form.id,
                            'granted_role': grant_as
                        },
                        invited_by_id=flask_login.current_user.id,
                        ldap_uuid=ldap_uuid
                    )
        invite.save()
        status = Dispatcher().send_invitation(invite)
        if status['email_sent'] is True:
            invite.last_sent = datetime.datetime.now(datetime.timezone.utc)
            invite.save()
            form.add_log(_("Sent invitation to %(email)s", email=invite.email))
            flash_text = _("We have sent an invitation to %(email)s", email=invite.email)
            flash(flash_text, 'success')
        else:
            invite.delete()
            flash(status['msg'], 'warning')
        return redirect(url_for(redirect_destination, form_id=form.id))

    default_message = Invite.default_message(lang=g.language)
    if request.method == 'GET':
        wtform.message.data = default_message
        wtform.email.data = email
    return render_template('invite-user.html',
                           wtform=wtform,
                           granted_form=form,
                           default_message=default_message,
                           grant_as=grant_as,
                           preview=Invite.get_preview(wtform.message.data))


@form_bp.route('/form/<int:form_id>/invite/<int:invite_id>/send-again', methods=['POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
@auth.instantiate_invite()
def send_invite_again(form_id, invite_id, **kwargs):
    form = kwargs["form"]
    invite = kwargs["invite"]
    if invite and invite.granted_form and invite.granted_form['id'] == form.id:
        invite.renew_token_life()
        status = Dispatcher().send_invitation(invite)
        if status['email_sent'] is True:
            invite.renew_token_life()
            invite.last_sent = datetime.datetime.now(datetime.timezone.utc)
            invite.save()
            form.add_log(_("Resent invitation to %(email)s", email=invite.email))
            flash_text = _("We have sent the invitation to %(email)s again",
                           email=invite.email)
            flash(flash_text, 'success')
        else:
            flash(status['msg'], 'warning')
        return redirect(url_for(request.form['redirect_destination'], form_id=form.id))
    flash(_("Opps! We cannot find that invitation"), 'error')
    return redirect(url_for('form_bp.form_details', form_id=form.id))


@form_bp.route('/form/<int:form_id>/invite/<int:invite_id>/delete', methods=['POST'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
@auth.instantiate_invite()
def delete_invite(form_id, invite_id, **kwargs):
    form = kwargs["form"]
    invite = kwargs["invite"]
    invite.delete()
    form.add_log(_("Deleted invitation for %(email)s", email=invite.email))
    flash(_("Invitation to %(email)s deleted OK", email=invite.email), 'success')
    return redirect(url_for(request.form['redirect_destination'], form_id=form.id))


@form_bp.route('/form/<int:form_id>/expiration', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def expiration(form_id, **kwargs):
    form_user = kwargs["form_user"]
    form_user.save_ui_preference('landing_page', 'expiration')
    return render_template('expiration.html',
                           form=form_user.form,
                           form_user=form_user,
                           goto=request.args.get("goto", None),
                           upload_media_form=wtf.UploadMedia(),
                           human_readable_bytes=utils.human_readable_bytes)


@form_bp.route('/form/<int:form_id>/expiration/set-date', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def set_expiration_date(form_id, **kwargs):
    form = kwargs['form']
    if 'date' in request.form and 'time' in request.form:
        if request.form['date'] and request.form['time']:
            expireDate = "%s %s:00" % (request.form['date'], request.form['time'])
            if not validators.is_valid_date(expireDate):
                pay_load = {'error': _("Date-time is not valid"),
                            'expired': form.has_expired()}
                return jsonify(pay_load), 200
            else:
                form.save_expiry_date(expireDate)
                form.add_log(_("Expiry date set to: %(date)s", date=expireDate))
        elif not request.form['date'] and not request.form['time']:
            if form.expiry_conditions['expireDate']:
                form.save_expiry_date(False)
                form.add_log(_("Expiry date cancelled"))
        else:
            pay_load = {'error': _("Missing date or time"),
                        'expired': form.has_expired()}
            return jsonify(pay_load), 200
        return jsonify(expired=form.has_expired()), 200
    return jsonify(False), 200


@form_bp.route('/form/<int:form_id>/expiration/set-field-condition', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def set_expiry_field_condition(form_id, **kwargs):
    form = kwargs["form"]
    if form.is_e2ee:
        return jsonify(condition=False), 406
    if 'field_name' in request.form and 'condition' in request.form:
        condition = form.save_expiry_field_condition(request.form['field_name'],
                                                     request.form['condition'])
        field_label = form.get_field_label(request.form['field_name'])
        form.add_log(_("Field '%(label)s' expiry set to: %(condition)s",
                     label=field_label,
                     condition=request.form['condition']))
        return jsonify(condition=condition, expired=form.expired), 200
    return jsonify(condition=False), 406


@form_bp.route('/form/<int:form_id>/expiration/set-total-answers', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def set_expiry_total_answers(form_id, **kwargs):
    form = kwargs['form']
    if 'total_answers' not in request.form:
        return jsonify(expired=False, total_answers=0), 406
    total_answers = request.form['total_answers']
    total_answers = form.save_expiry_total_answers(total_answers)
    form.add_log(_("Expire when total answers set to: %(number)s", number=total_answers))
    return jsonify(expired=form.expired, total_answers=total_answers), 200


@form_bp.route('/form/<int:form_id>/log', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def list_log(form_id, **kwargs):
    return render_template('list-log.html', User=User, form=kwargs['form'])


@form_bp.route('/form/<int:form_id>/qr', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form(allow_admin=False)
def form_qr(form_id, **kwargs):
    form = kwargs['form']
    if 'as_png' in request.args:
        return send_file(form.get_qr(as_png=True),
                         download_name=f"{form.slug}-QR.png",
                         mimetype="image/png",
                         as_attachment=True)
    return render_template('form/qr.html', form=form)


## Form settings

@form_bp.route('/form/toggle-enabled/<int:form_id>', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def toggle_form_enabled(form_id, **kwargs):
    form = kwargs['form']
    enabled = form.toggle_enabled()
    form.add_log(_("Public set to: %(boolean)s", boolean=enabled))
    return jsonify(toggle_public=enabled), 200


@form_bp.route('/form/<int:form_id>/toggle-restricted-access', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def toggle_restricted_access(form_id, **kwargs):
    form = kwargs['form']
    access = not form.restricted_access
    form.restricted_access = access
    form.save()
    form.add_log(_("Restricted access set to: %(boolean)s", boolean=access))
    return jsonify(restricted=access), 200


# ## On submit confirmation email
@form_bp.route('/form/<int:form_id>/toggle-send-confirmation', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json()
def toggle_form_send_confirmation(form_id, **kwargs):
    form = kwargs['form']
    form_user = kwargs['form_user']
    if form.is_e2ee:
        send_email = False
    else:
        send_email = not form.confirmation["send_email"]
    form.confirmation["send_email"] = send_email
    if not send_email:
        form.confirmation["with_edit_link"] = False
        form_user.notifications["anon_answer_edition"] = False  # TODO: Really?
        form_user.save()
    form.save()
    form.add_log(_("Send confirmation email set to: %(boolean)s", boolean=send_email))
    return jsonify(send_confirmation=send_email), 200


@form_bp.route('/form/<int:form_id>/toggle-anon-edition', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json()
def toggle_anon_edition(form_id, **kwargs):
    """Can anonymous users edit their answers."""
    form = kwargs['form']
    if form.is_e2ee:
        form.anon_edition = False
    else:
        form.anon_edition = not form.anon_edition
    form.save()
    if not form.anon_edition:
        kwargs['form_user'].notifications["anon_answer_edition"] = False
        kwargs['form_user'].save()
    return jsonify(anon_edition=form.anon_edition), 200


@form_bp.route('/form/<int:form_id>/toggle-publish-map', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def toggle_publish_map(form_id, **kwargs):
    form = kwargs["form"]
    if not "map" in form.preferences:
        form.preferences["map"] = form.map_preference()
    if form.is_e2ee:
        form.preferences["map"]["is_public"] = False
    else:
        form.preferences["map"]["is_public"] = not form.preferences["map"]["is_public"]
    flag_modified(form, "preferences")
    form.save()
    return jsonify(enable_public_map=form.preferences["map"]["is_public"]), 200

@form_bp.route('/form/<int:form_id>/map-markers', methods=['GET'])
@auth.enabled_user_required
@auth.instantiate_form__json(allow_admin=False)
def map_markers(form_id, **kwargs):
    form = kwargs["form"]
    return render_template('map-markers.html', form=form)


@form_bp.route('/form/<int:form_id>/set-map-marker-popup', methods=['PATCH'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def map_set_markers(form_id, **kwargs):
    form = kwargs["form"]
    data = request.get_json(silent=True)
    if not("map_id" in data and isinstance(data["map_id"], str) and
           "popup_fields" in data and isinstance(data["popup_fields"], list)):
         return jsonify("Not Acceptable"), 406
    map_id = data["map_id"]
    popup_fields = data["popup_fields"]
    if map_id not in form.preferences["map"]:
        form.preferences["map"]["markers"][map_id] = {
                "is_public": True,
                "props": {},  # Future
                "popup_fields": []
        }
    form.preferences["map"]["markers"][map_id]["popup_fields"] = popup_fields
    flag_modified(form, "preferences")
    form.save()
    return jsonify(saved=True), 200


@form_bp.route('/form/<int:form_id>/toggle-api-endpoint', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def toggle_api_endpoint(form_id, **kwargs):
    form = kwargs["form"]
    if form.id != form_id:
        return jsonify(message="Not acceptable"), 406
    if not form.endpoint_auth:
        FormAuth(form).save()
    else:
        form_auth = FormAuth.find(form_id=form.id)
        form_auth.enabled = not form_auth.enabled
        form_auth.save()
    username = flask_login.current_user.username
    if not form.endpoint_auth.enabled:
        current_app.logger.info(f'FORM - API endpoints disabled by "{username}". form: {form.id}, "{form.slug}"')
        form.add_log(_("Disabled endpoints"))
        return jsonify(api_enabled=False), 200
    current_app.logger.info(f'FORM - API endpoints enabled by "{username}". form: {form.id}, "{form.slug}"')
    form.add_log(_("Enabled endpoints"))
    auth_value = form.endpoint_auth.get_authorization_header_value()
    return jsonify(api_enabled=True, authorization_header_value=auth_value), 200


@form_bp.route('/form/<int:form_id>/toggle-notify-answer-edition', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json()
def toggle_notify_on_answer_edition(form_id, **kwargs):
    """Notify the form user when an answer is edited."""
    form_user = kwargs['form_user']
    notify = not form_user.notifications["anon_answer_edition"]
    form_user.notifications["anon_answer_edition"] = notify
    form_user.save()
    return jsonify(notify_answer_edition=notify), 200


@form_bp.route('/form/<int:form_id>/toggle-notification', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def toggle_new_answer_notification(form_id, **kwargs):
    """Toggle current_user's Form preference."""
    form_user = kwargs["form_user"]
    return jsonify(notification=form_user.toggle_new_answer_notification()), 200


@form_bp.route('/form/<int:form_id>/expiration/toggle-notification', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False)
def toggle_form_expiration_notification(form_id, **kwargs):
    form_user = kwargs["form_user"]
    return jsonify(notification=form_user.toggle_expiration_notification()), 200


#
# Form templates
#
@form_bp.route('/forms/templates', methods=['GET'])
@auth.enabled_editor_required
def list_templates():
    return render_template('list-templates.html',
                           templates=form_templates.templates)


@form_bp.route('/forms/templates/<int:template_id>', methods=['GET'])
@auth.enabled_editor_required
def view_template(template_id):
    template = next((sub for sub in form_templates.templates if sub['id'] == template_id), None)
    if not template:
        return redirect(url_for('form_bp.list_templates'))
    markdown = template['introduction_md']
    introduction_text = {"markdown": markdown,
                         "html": sanitizers.markdown_to_html(markdown)}
    form = Form(flask_login.current_user,
                structure=template['structure'],
                introduction_text=introduction_text)
    return render_template('preview-template.html',
                           form=form,
                           template_id=template['id'],
                           human_readable_bytes=utils.human_readable_bytes)


# E2EE

@form_bp.route('/form/<int:form_id>/enable-e2ee', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form(allow_admin=False)
def enable_e2ee(form_id, **kwargs):
    form = kwargs["form"]
    form_user = kwargs["form_user"]

    if form.author_id != flask_login.current_user.id:
        return jsonify(msg="Form authors only"), 401
    if form.e2ee_public_key:
        return jsonify(msg="Form key already on server"), 406
    if not form.is_e2ee:
        return jsonify(msg="Not an E2EE form"), 406
    if form.get_total_answers() > 0:
        return jsonify(msg="Can't change E2EE with answers"), 412

    data = request.get_json(silent=True)
    try:
        assert (
            set(data.keys()) == set([
                "e2ee_public_key",
                "e2ee_user_ciphered_key",
            ])
        )
    except:
        return jsonify(msg="Unexpected data"), 406
    res, res_code = validators.is_valid_e2ee_public_key(data)
    if res_code == 200:
        ciphered_key = data['e2ee_user_ciphered_key']
        res, res_code = validators.is_valid_e2ee_ciphered_key(ciphered_key)
        if res_code == 200:
            form.e2ee_public_key = data['e2ee_public_key']
            form.save()
            form_user.e2ee_ciphered_key_backup = ciphered_key
            form_user.save()
            # i18n: A user enabled encryption on this form
            form.add_log(_("Enabled encryption"))
            return jsonify(saved=True), 200
    return res, res_code


@form_bp.route('/form/<int:form_id>/set-formuser-ciphered-key/<int:user_id>', methods=['PATCH'])
@auth.enabled_user_required__json
@auth.instantiate_form__json()
def set_formuser_ciphered_key(form_id, user_id, **kwargs):
    """Add form's ciphered key pair to this form_user."""
    if not kwargs['form'].is_e2ee_enabled():
        return jsonify(msg="Form is not E2EE enabled"), 406
    form_user = FormUser.find(form_id=form_id, user_id=user_id)
    if not form_user:
        return jsonify(msg="Form user not found"), 404

    data = request.get_json(silent=True)
    res, res_code = validators.is_valid_e2ee_ciphered_key(data)
    if res_code == 200:
        form_user.e2ee_ciphered_key_backup = data
        log_msg = "FORM - Ciphered keys shared with {} by {}: form {}".format(
            form_user.user.username,
            flask_login.current_user.username,
            form_user.form.id
        )
        current_app.logger.info(log_msg)
        form_user.save()
        return jsonify(saved=True), 200
    return res, res_code


@form_bp.route('/form/<int:form_id>/log-key-copy', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json()
def log_key_copy(form_id, **kwargs):
    """The user copied the form's private E2EE key to the clipboard."""
    form = kwargs['form']
    form.add_log(_("Copied E2EE private key to clipboard"))
    return jsonify(logged=True), 200
