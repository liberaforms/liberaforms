"""Rename e2ee_answers column to is_e2ee

Revision ID: 6120e7dd4b14
Revises: 41914166326a
Create Date: 2024-08-09 12:01:31.547494

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6120e7dd4b14"
down_revision = "41914166326a"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("forms", "e2ee_answers", new_column_name="is_e2ee")


def downgrade():
    op.alter_column("forms", "is_e2ee", new_column_name="e2ee_answers")
