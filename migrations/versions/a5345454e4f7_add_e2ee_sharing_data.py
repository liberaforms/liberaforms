"""add e2ee sharing data

Revision ID: a5345454e4f7
Revises: 6120e7dd4b14
Create Date: 2024-08-09 14:06:19.873589

"""

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.orm.session import Session

# revision identifiers, used by Alembic.
revision = "a5345454e4f7"
down_revision = "6120e7dd4b14"
branch_labels = None
depends_on = None


def ensure_empty_e2ee_forms():
    session = Session(bind=op.get_bind())
    res = session.query(
        sa.text(
            "a.id FROM ANSWERS AS a JOIN forms AS f ON a.form_id=f.id WHERE f.is_e2ee"
        )
    )
    if res.count() > 0:
        raise RuntimeError(
            " ".join(
                [
                    "Some E2EE forms have answers, this migration would lose them.",
                    "Delete all answers from E2EE forms before this migration.",
                ]
            )
        )


def upgrade():
    ensure_empty_e2ee_forms()

    for table, column in [
        ("forms", "e2ee_public_key"),
        ("form_users", "e2ee_ciphered_key_backup"),
    ]:
        op.add_column(
            table,
            sa.Column(
                column,
                MutableDict.as_mutable(JSONB),
                nullable=False,
                server_default="{}",
            ),
        )
        op.alter_column(
            table,
            sa.Column(
                column,
                MutableDict.as_mutable(JSONB),
                nullable=False,
                server_default=None,
            ),
        )


def downgrade():
    ensure_empty_e2ee_forms()

    for table, column in [
        ("forms", "e2ee_public_key"),
        ("form_users", "e2ee_ciphered_key_backup"),
    ]:
        op.drop_column(table, column)
