"""Add forms.e2ee_answers column

Revision ID: 6a75a0773356
Revises: bec970e6d5c1
Create Date: 2024-07-29 17:42:53.774673

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6a75a0773356'
down_revision = 'bec970e6d5c1'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('forms', sa.Column(
        'e2ee_answers', sa.Boolean,
        default=False, nullable=False, server_default='False'))
    op.alter_column('forms', sa.Column(
        'e2ee_answers', sa.Boolean,
        default=False, nullable=False, server_default=None))


def downgrade():
    # This is not safe as they would lose answers
    op.drop_column('forms', 'e2ee_answers')
