"""adds form_preferences dict to Site model

Revision ID: f923fc23e27f
Revises: a5345454e4f7
Create Date: 2024-08-12 17:19:37.169820

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'f923fc23e27f'
down_revision = 'a5345454e4f7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('site', sa.Column('form_preferences', postgresql.JSONB(astext_type=sa.Text()), nullable=True))
    op.execute("UPDATE site SET form_preferences = '{}'")
    op.alter_column('site', 'form_preferences', nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('site', 'form_preferences')
    # ### end Alembic commands ###
