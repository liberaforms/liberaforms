"""Ensure email addresses are lower case

Revision ID: 257a4f31751b
Revises: ebabdcf85792
Create Date: 2024-12-24 10:07:56.074179

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm.session import Session
from sqlalchemy.orm import declarative_base

# revision identifiers, used by Alembic.
revision = '257a4f31751b'
down_revision = 'ebabdcf85792'
branch_labels = None
depends_on = None

Base = declarative_base()

class User(Base):
    __tablename__ = "users"
    id = sa.Column(sa.Integer, primary_key=True, index=True)
    email = sa.Column(sa.String, unique=True, nullable=False)


class Invites(Base):
    __tablename__ = "invites"
    id = sa.Column(sa.Integer, primary_key=True, index=True)
    email = sa.Column(sa.String, nullable=False)


def upgrade():
    session = Session(bind=op.get_bind())
    users = session.query(User).all()
    for user in users:
        lowered_email = user.email.lower()
        if lowered_email != user.email:
            print("lowered", lowered_email)
            user.email = lowered_email
    invites = session.query(Invites).all()
    for invite in invites:
        lowered_email = invite.email.lower()
        if lowered_email != invite.email:
            invite.email = lowered_email
    print('OK')
    session.commit()
    session.close()


def downgrade():
    pass
