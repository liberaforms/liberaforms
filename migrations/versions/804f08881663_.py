"""Add users.e2ee_public_key column

Revision ID: 804f08881663
Revises: 6a75a0773356
Create Date: 2024-07-29 19:51:04.714773

"""

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.mutable import MutableDict


# revision identifiers, used by Alembic.
revision = "804f08881663"
down_revision = "6a75a0773356"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "users",
        sa.Column(
            "e2ee_public_key",
            MutableDict.as_mutable(JSONB),
            nullable=False,
            server_default="{}",
        ),
    )
    op.alter_column(
        "users",
        sa.Column(
            "e2ee_public_key",
            MutableDict.as_mutable(JSONB),
            nullable=False,
            server_default=None,
        ),
    )


def downgrade():
    op.drop_column("users", "e2ee_public_key")
