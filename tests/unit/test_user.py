"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from factories import UserFactory
from flask import current_app
from liberaforms.models.site import Site
from liberaforms.models.user import User


class TestUser():

    @classmethod
    def setup_class(cls):
        cls.user = UserFactory()
        cls.user.save()

    def test_new_user(self):
        assert self.user.role == 'editor'
        assert not self.user.validated_email
        assert not self.user.enabled
        assert not self.user.can_create_forms()
        assert self.user.uploads_enabled == Site.find().newuser_enableuploads
        assert self.user.created == self.user.last_login


    def test_disabled_new_forms(self):
        self.user.validated_email = True
        self.user.save()
        assert self.user.can_create_forms()
        self.user.blocked = True
        self.user.save()
        assert not self.user.can_create_forms()
        self.user.blocked = False
        self.user.role = "guest"
        self.user.save()
        assert not self.user.can_create_forms()
