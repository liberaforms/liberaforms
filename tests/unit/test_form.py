"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
import pytest
from factories import FormFactory
#from flask import current_app
#from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from tests import utils


class TestForm():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        structure = utils.create_form_structure(with_email=True, with_map=True)
        form = FormFactory(author=editor,
                           slug=utils.random_slug(),
                           structure=json.loads(structure))
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties["form"] = form

    def test_new_form(self):
        form = self.properties["form"]
        assert form.id
        assert not form.anon_edition
        assert not form.is_e2ee
        # TODO: add more tests here
