"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import flask_login
from liberaforms.models.media import Media
from tests import utils
from tests.utils import random_slug as random_string


def save_valid_media(app, user):
    media_file = utils.create_file_obj("valid_media.png")
    with app.test_request_context():
        flask_login.login_user(user)
        alt_text = random_string()
        Media().save_media(user, media_file, alt_text)
    assert Media.find(user_id=user.id, alt_text=alt_text)
