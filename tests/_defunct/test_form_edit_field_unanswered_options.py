"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from flask import url_for
from liberaforms.models.form import Form
from tests.utils import login, random_slug
from tests import user_creds


class TestEditUnansweredFieldOptions():
    """Given an un-answered form
       Change multi-option field labels
       Test for unique multi-option field values."""

    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, client):
        """Create a form
           Test for unique values based on provided labels."""

        login(client, user_creds["editor"])

        slug = random_slug()
        form_name = "Un-answered field edition test form"
        client.post(
            url_for("form_bp.create_new_form"),
            data={
                "name": form_name,
                "slug": slug,
            },
            follow_redirects=True,
        )
        form = Form.find(name=form_name)
        assert form.slug == slug
        self.properties['form'] = form
        assert len(form.structure) == 0
        structure = [
          {
            "className": "form-control",
            "label": "Select group",
            "name": "select-1692872264164",
            "required": False,
            "type": "select",
            "values": [
              {
                # note _ in label. tests utils.sanitizers.sanitize_label
                "label": "select option_1",
                "value": ""
              },
              {
                "label": "select option 2",
                "value": ""
              },
              {
                "label": "select option 3",
                "value": ""
              }
            ]
          },
          {
             "inline": False,
             "label": "Radio group",
             "name": "radio-group-1692872298619",
             "other": False,
             "required": False,
             "type": "radio-group",
             "values": [
               {
                 "label": "radio option 1",
                 "value": ""
               },
               {
                 "label": "radio option 1",  # this label is not unique
                 "value": ""
               }
             ]
          }
        ]
        form.start_edit_mode()
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )
        assert len(form.structure) == 2
        select_field = form.structure[0]
        assert len(select_field["values"]) == 3
        assert select_field["values"][0]["label"] == "select option-1"  # sanitize_label converts _ to -
        assert select_field["values"][0]["value"] == "select-option-1"
        assert select_field["values"][1]["label"] == "select option 2"
        assert select_field["values"][1]["value"] == "select-option-2"
        assert select_field["values"][2]["label"] == "select option 3"
        assert select_field["values"][2]["value"] == "select-option-3"
        radio_field = form.structure[1]
        assert len(radio_field["values"]) == 2
        assert radio_field["values"][0]["label"] == "radio option 1"
        assert radio_field["values"][0]["value"] == "radio-option-1"
        assert radio_field["values"][1]["label"] == "radio option 1"
        assert radio_field["values"][1]["value"] == "radio-option-1_1"

    def test_edit_field_options(self, client):
        """Test for unique values based on provided labels."""

        form = self.properties['form']
        structure = form.structure.copy()
        edited_select_field = {
          "className": "form-control",
          "label": "Select group",
          "name": "select-1692872264164",
          "required": False,
          "type": "select",
          # changes made:
          # values[0]["label"] is changed
          "values": [
            {
              "label": "select option 1 edited",  # this label is changed
              "value": structure[0]["values"][0]["value"]
            },
            {
              "label": "select option 2",
              "value": structure[0]["values"][1]["value"]
            },
            {
              "label": "select option 3",
              "value": structure[0]["values"][2]["value"]
            }
          ]
        }
        structure[0] = edited_select_field

        login(client, user_creds["editor"])
        form.start_edit_mode()
        assert form.answers.count() == 0
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )

        select_field = form.structure[0]
        assert select_field["values"][0]["label"] == "select option 1 edited"
        assert select_field["values"][0]["value"] == "select-option-1-edited"
        assert select_field["values"][1]["label"] == "select option 2"
        assert select_field["values"][1]["value"] == "select-option-2_1"
        assert select_field["values"][2]["label"] == "select option 3"
        assert select_field["values"][2]["value"] == "select-option-3_1"
        radio_field = form.structure[1]
        assert radio_field["values"][0]["label"] == "radio option 1"
        assert radio_field["values"][0]["value"] == "radio-option-1_2"
        assert radio_field["values"][1]["label"] == "radio option 1"
        assert radio_field["values"][1]["value"] == "radio-option-1_3"

        structure = form.structure.copy()
        edited_select_field = {
          "className": "form-control",
          "label": "Select group",
          "name": "select-1692872264164",
          "required": False,
          "type": "select",
          # changes made:
          # values[2]["label"] is changed and is not unique
          "values": [
            {
              "label": "select option 1 edited",
              "value": structure[0]["values"][0]["value"]
            },
            {
              "label": "select option 2",
              "value": structure[0]["values"][1]["value"]
            },
            {
              "label": "select option 2",
              "value": structure[0]["values"][2]["value"]
            }
          ]
        }
        structure[0] = edited_select_field

        form.start_edit_mode()
        assert form.answers.count() == 0
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )

        select_field = form.structure[0]
        assert select_field["values"][0]["label"] == "select option 1 edited"
        assert select_field["values"][0]["value"] == "select-option-1-edited_1"
        assert select_field["values"][1]["label"] == "select option 2"
        assert select_field["values"][1]["value"] == "select-option-2"
        assert select_field["values"][2]["label"] == "select option 2"
        assert select_field["values"][2]["value"] == "select-option-2_2"
        radio_field = form.structure[1]
        assert radio_field["values"][0]["label"] == "radio option 1"
        assert radio_field["values"][0]["value"] == "radio-option-1"
        assert radio_field["values"][1]["label"] == "radio option 1"
        assert radio_field["values"][1]["value"] == "radio-option-1_1"

        structure = form.structure.copy()
        edited_select_field = {
          "className": "form-control",
          "label": "Select group",
          "name": "select-1692872264164",
          "required": False,
          "type": "select",
          # changes made:
          # values[1]["label"] is changed
          # values[2]["label"] is changed and is not unique
          "values": [
            {
              "label": "select option 1 edited",
              "value": structure[0]["values"][0]["value"]
            },
            {
              "label": "select option 3",
              "value": structure[0]["values"][1]["value"]
            },
            {
              "label": "select option 3",
              "value": structure[0]["values"][2]["value"]
            }
          ]
        }
        structure[0] = edited_select_field

        form.start_edit_mode()
        assert form.answers.count() == 0
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )

        select_field = form.structure[0]
        assert select_field["values"][0]["label"] == "select option 1 edited"
        assert select_field["values"][0]["value"] == "select-option-1-edited"
        assert select_field["values"][1]["label"] == "select option 3"
        assert select_field["values"][1]["value"] == "select-option-3"
        assert select_field["values"][2]["label"] == "select option 3"
        assert select_field["values"][2]["value"] == "select-option-3_1"
        radio_field = form.structure[1]
        assert radio_field["values"][0]["label"] == "radio option 1"
        assert radio_field["values"][0]["value"] == "radio-option-1_2"
        assert radio_field["values"][1]["label"] == "radio option 1"
        assert radio_field["values"][1]["value"] == "radio-option-1_3"

    def test_add_field(self, client):
        """Adds a field to an existing form.structure."""

        form = self.properties['form']
        structure = form.structure.copy()

        new_field = {
          "inline": False,
          "label": "Checkbox group",
          "name": "checkbox-group-1692872324074",
          "other": False,
          "required": False,
          "type": "checkbox-group",
          "values": [
            {
              "label": "checkbox option 1",
              "value": ""
            },
            {
              "label": "checkbox option 2",
              "value": ""
            },
            {
              "label": "",
              "value": ""
            }
          ]
        }
        structure.append(new_field)

        login(client, user_creds["editor"])
        form.start_edit_mode()
        assert form.answers.count() == 0
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )
        assert len(form.structure) == 3
        select_field = form.structure[0]
        assert select_field["values"][0]["label"] == "select option 1 edited"
        assert select_field["values"][0]["value"] == "select-option-1-edited_1"
        assert select_field["values"][1]["label"] == "select option 3"
        assert select_field["values"][1]["value"] == "select-option-3_2"
        assert select_field["values"][2]["label"] == "select option 3"
        assert select_field["values"][2]["value"] == "select-option-3_3"
        radio_field = form.structure[1]
        assert radio_field["values"][0]["label"] == "radio option 1"
        assert radio_field["values"][0]["value"] == "radio-option-1"
        assert radio_field["values"][1]["label"] == "radio option 1"
        assert radio_field["values"][1]["value"] == "radio-option-1_1"
        checkbox_field = form.structure[2]  # the new field
        assert len(checkbox_field["values"]) == 2
        assert checkbox_field["values"][0]["label"] == "checkbox option 1"
        assert checkbox_field["values"][0]["value"] == "checkbox-option-1"
        assert checkbox_field["values"][1]["label"] == "checkbox option 2"
        assert checkbox_field["values"][1]["value"] == "checkbox-option-2"
