"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2025 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import re
import polib
import pytest

GREEN = '\033[0;32;48m'
RED = '\033[1;31;48m'
END = '\033[1;37;0m'

start = re.escape('%(')
stop = re.escape(')')
gettext_var_filter = re.compile('{}[^{}]*?{}'.format(start, start, stop))


class TestGettextFile():
    """Test for valid valiables in all po files."""

    # def test_requirements(self, app):
    #     pass
    #     translations_path = f"{app.root_path}/translations"
    #     assert os.path.isfile(f"{translations_path}/messages.pot")
    #     self.properties['default_pot'] = polib.pofile(f"{translations_path}/messages.pot")

    #     # Create new tmp english po file to test against
    #     from pathlib import Path
    #     tmp_path = f"{app.config['ROOT_DIR']}/tests/tmp/translations"
    #     tmp_dir = Path(tmp_path)
    #     tmp_dir.mkdir(parents=True, exist_ok=True)
    #             import subprocess
    #     subprocess.call([
    #      "pybabel", "init",
    #      "-i", f"{app.root_path}/translations/messages.pot",
    #      "-d", tmp_path,
    #      "-l", "en",
    #     ])

    #     po_path = f"{tmp_path}/en/LC_MESSAGES/messages.po"
    #     assert os.path.isfile(po_path)


    def test_gettext_vars(self, app):

        def throw_error(lang_code, var, entry):
            po_path = f"translations/{lang_code}/LC_MESSAGES/messages.po"
            error = f"lang: {lang_code}, Missing variable: '{var}' in '{entry.msgid}' {po_path}"
            pytest.exit(f"{RED}Failed: {error}{END}")

        for lang_code in app.config['LANGUAGES'].keys():
            if lang_code == 'en':
                continue

            po_path = f"{app.root_path}/translations/{lang_code}/LC_MESSAGES/messages.po"
            assert os.path.isfile(po_path)

            po = polib.pofile(po_path)
            entries = [e for e in po if e.translated() and not e.obsolete and not e.fuzzy]
            for entry in entries:
                msgid_vars = re.findall(gettext_var_filter, entry.msgid)
                msgstr_vars = re.findall(gettext_var_filter, entry.msgstr)

                if entry.msgid_plural:
                    msgid_vars = msgid_vars + re.findall(gettext_var_filter, entry.msgid_plural)
                    msgstr_vars = re.findall(gettext_var_filter, entry.msgstr_plural[0]) + \
                                  re.findall(gettext_var_filter, entry.msgstr_plural[1])
                    #print('entry.msgid',entry.msgid)
                    #print('entry.msgid_plural',entry.msgid_plural)
                    #print('entry.msgstr_plural',entry.msgstr_plural)
                    #print('msgid_vars',msgid_vars)
                    #print('msgstr_vars',msgstr_vars)

                for var in msgid_vars:
                    if var not in msgstr_vars:
                        throw_error(lang_code, var, entry)
                for var in msgstr_vars:
                    if var not in msgid_vars:
                        throw_error(lang_code, var, entry)
