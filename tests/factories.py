"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
import factory
from factory.fuzzy import FuzzyText
from tests import VALID_PASSWORD
from tests.utils import create_form_structure
from liberaforms.models.site import Site
from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.models.consent import Consent
#from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer
from liberaforms.utils import sanitizers

#from liberaforms import create_app
#from flask import has_app_context, g

#factory.random.reseed_random('my awesome project')

# random # IDEA:
# foo = factory.LazyAttribute(lambda a: MyModel.objects.order_by('?').first())


def _create_introduction_text(intro_text: dict):
    markdown = intro_text["markdown"]
    return {'markdown': markdown,
            'html': sanitizers.markdown_to_html(markdown)}


# def _get_consent_text():
#     if has_app_context():
#         return Form.new_data_consent()
#     with create_app().app_context():
#         g.site = Site.find()
#         return Form.new_data_consent()


# def with_request(f):
#     @wraps(f)
#     def wrap(*args, **kwargs):
#         with create_app().test_request_context():
#             return f
#     return wrap


#def fuzzy_text(length=24):
#    return str(uuid.uuid4())
#    return FuzzyText(length=length).fuzz().lower()
#    with create_app().app_context():
#        slug = FuzzyText(length=length).fuzz().lower()
#        while Form.find(slug=slug):
#            slug = FuzzyText(length=length).fuzz().lower()
#        return slug

class UserFactory(factory.Factory):
    class Meta:
        model = User

    username = factory.Faker('user_name')
    email = factory.Faker('email')
    password = VALID_PASSWORD
    validated_email = False


class ConsentFactory(factory.Factory):
    class Meta:
        model = Consent

    site_id = None
    user_id = None
    form_id = None
    name = factory.Faker('word')
    label = {"en": "A label"}
    text = {"en": {"markdown": "# hello", "html": "<h1>hello</h1>"}}
    checkbox_label:dict = {}
    required = True
    wizard:dict = {}


# class FormFactory(factory.alchemy.SQLAlchemyModelFactory):
class FormFactory(factory.Factory):
    class Meta:
        model = Form
        #sqlalchemy_session = db.session

    name = "test form"
    slug = None
    structure = json.loads(create_form_structure())
    fieldIndex = Form.create_field_index(structure)
    introduction_text = _create_introduction_text(Form.default_introduction_text())
    after_submit_text = {'html': "", 'markdown': ""}
    expired_text = {'html': "", 'markdown': ""}
    author = None

    confirmation = {
        "send_email": Form.structure_has_email_field(structure)
    }
    #confirmation = {}
    #form_user = factory.SubFactory(FormUserFactory)
    #@factory.post_generation
    #def users(self, create, extracted, **kwargs):
    #    if not create:
    #        return
    #    if extracted:
    #        for user in extracted:
    #            self.users.add(user)


#class FormUserFactory(factory.Factory):
#    class Meta:
#        model = FormUser
#
#    notifications = {}  #user.new_form_notifications()
#    ui_preferences = {}  #FormUser.default_ui_prefs()


class AnswerFactory(factory.Factory):
    class Meta:
        model = Answer

    form_id = None
    author_id = None
    data:dict = {}
