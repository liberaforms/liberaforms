"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from flask import url_for
from liberaforms.models.formuser import FormUser
from tests.factories import UserFactory, FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestShareForm():
    """Runs through the process of sharing a form with Editor and Reader roles."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.new_editor = UserFactory(validated_email=True)
        cls.new_editor.save()
        cls.new_reader = UserFactory(validated_email=True)
        cls.new_reader.save()

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(user=editor, form=form, is_editor=True).save()
        self.properties['form'] = form

    def test_auth(self, client):
        """Test form_bp.form_editors
                form_bp.add_editor
                ."""

        form = self.properties['form']
        logout(client)
        response = client.get(
                        url_for('form_bp.form_editors', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.add_editor', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for('form_bp.form_readers', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.toggle_restricted_access', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds['admin'])
        response = client.get(
                        url_for('form_bp.form_editors', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.add_editor', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.get(
                        url_for('form_bp.form_readers', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.toggle_restricted_access', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds['editor'])

    def test_share_with_editors(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.form_editors', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- form_users_editor_page -->' in response.data.decode()

    def test_add_nonexistent_user_as_editor(self, client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        nonexistent_user_email = "nonexistent@example.com"
        response = client.post(
                        url_for('form_bp.add_editor', form_id=form.id),
                        data={
                            "email": nonexistent_user_email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- form_users_editor_page -->' in html
        assert 'id="user_not_found_modal"' in html
        #assert "alert-warning" in response.data.decode()
        assert f'"{url_for("form_bp.add_editor", form_id=form.id)}" method="POST"' in response.data.decode()
        assert form.log.count() == initial_log_count

    def test_add_editor(self, client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        assert not FormUser.find(form_id=form.id, user_id=self.new_editor.id, is_editor=True)
        response = client.post(
                        url_for('form_bp.add_editor', form_id=form.id),
                        data={
                            "email":  self.new_editor.email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_users_editor_page -->' in response.data.decode()
        assert self.new_editor.email in response.data.decode()
        assert form.log.count() == initial_log_count + 1
        assert FormUser.find(form_id=form.id, user_id=self.new_editor.id, is_editor=True)

    def test_add_same_editor(self, client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.add_editor', form_id=form.id),
                        data={
                            "email":  self.new_editor.email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_users_editor_page -->' in response.data.decode()
        assert "alert-warning" in response.data.decode()
        assert self.new_editor.email in response.data.decode()
        assert form.log.count() == initial_log_count

    def test_share_with_readers(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.form_readers', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- form_users_reader_page -->' in html
        assert f'"{url_for("form_bp.add_reader", form_id=form.id)}" method="POST"' in html

    def test_add_nonexistent_user_as_reader(self, client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        nonexistent_user_email = "nonexistent@example.com"
        response = client.post(
                        url_for('form_bp.add_reader', form_id=form.id),
                        data={
                            "email": nonexistent_user_email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert f'"{url_for("form_bp.add_reader", form_id=form.id)}" method="POST"' in html
        assert self.properties['form'].log.count() == initial_log_count
        assert 'user_not_found_modal' in html

    def test_add_blocked_user(self, client):
        form = self.properties['form']
        user = UserFactory(validated_email=True)
        user.blocked = True
        user.save()
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.add_reader', form_id=form.id),
                        data={
                            "email": user.email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_users_reader_page -->' in response.data.decode()
        assert form.log.count() == initial_log_count

    def test_add_reader(self, client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.add_reader', form_id=form.id),
                        data={
                            "email": self.new_reader.email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_users_reader_page -->' in response.data.decode()
        assert form.log.count() == initial_log_count + 1
        assert FormUser.find(form_id=form.id,
                             user_id=self.new_reader.id,
                             is_editor=False)

    def test_add_form_editor_as_reader(self, client):
        """An existing form_user.editor cannot be added as a reader."""
        form = self.properties['form']
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.add_reader', form_id=form.id),
                        data={
                            "email": self.properties['form'].author.email,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_users_reader_page -->' in response.data.decode()
        assert form.log.count() == initial_log_count

    def test_toggle_restricted_form_access(self, client):
        form = self.properties['form']
        initial_restriced_access = form.restricted_access
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.toggle_restricted_access', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['restricted'] is True
        assert form.restricted_access != initial_restriced_access
        assert form.log.count() == initial_log_count + 1
