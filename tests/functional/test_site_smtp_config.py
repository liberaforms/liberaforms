"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import url_for
from liberaforms.models.site import Site
from tests import user_creds
from tests.utils import login, logout

class TestSiteSmtpConfig():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_auth(self, editor, client):
        """Test site_bp.smtp_config
                site_bp.test_smtp."""
        logout(client)
        response = client.get(
                        url_for('site_bp.smtp_config'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.test_smtp'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        logout(client)
        login(client, user_creds['editor'])
        response = client.get(
                        url_for('site_bp.smtp_config'),
                        follow_redirects=True,
                    )
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.test_smtp'),
                        follow_redirects=True,
                    )
        assert '<!-- my_forms_page -->' in response.data.decode()

        logout(client)
        login(client, user_creds['admin'])
        response = client.get(
                        url_for('site_bp.smtp_config'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- smtp_config_page -->' in response.data.decode()


    def test_save_smtp_config(self, client):
        """Tests invalid and valid smtp configuration."""

        initial_smtp_config = self.site.smtp_config
        invalid_smtp_conf = {
            'host': 'smtp.example.com',
            'port': "i_am_a_string",
            'encryption': 'SSL',
            'user': 'username',
            'password': 'password',
            'noreplyAddress': 'noreply@example.com'
        }
        response = client.post(
                        url_for('site_bp.smtp_config'),
                        data=invalid_smtp_conf,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert self.site.smtp_config == initial_smtp_config
        assert '<!-- smtp_config_page -->' in response.data.decode()
        valid_smtp_conf = {
            'host': os.environ['TEST_SMTP_HOST'],
            'port': int(os.environ['TEST_SMTP_PORT']),
            'encryption': os.environ['TEST_SMTP_ENCRYPTION'],
            'user': os.environ['TEST_SMTP_USERNAME'],
            'password': os.environ['TEST_SMTP_USER_PASSWORD'],
            'noreplyAddress': os.environ['TEST_SMTP_NO_REPLY']
        }
        response = client.post(
                        url_for('site_bp.smtp_config'),
                        data=valid_smtp_conf,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert self.site.smtp_config['host'] == os.environ['TEST_SMTP_HOST']
        assert self.site.smtp_config['port'] == int(os.environ['TEST_SMTP_PORT'])
        assert self.site.smtp_config['encryption'] == os.environ['TEST_SMTP_ENCRYPTION']
        assert self.site.smtp_config['user'] == os.environ['TEST_SMTP_USERNAME']
        assert self.site.smtp_config['password'] == os.environ['TEST_SMTP_USER_PASSWORD']
        assert self.site.smtp_config['noreplyAddress'] == os.environ['TEST_SMTP_NO_REPLY']

    @pytest.mark.skipif(os.environ['SKIP_EMAILS'] == 'True', reason="SKIP_EMAILS=True in test.ini")
    def test_test_smtp_config(self, client):
        """Send a test email."""
        response = client.post(
                        url_for('site_bp.test_smtp'),
                        data={
                            'email': user_creds['admin']['email'],
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert 'alert alert-success' in response.data.decode()
