"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from copy import copy
from flask import url_for
from liberaforms.models.formuser import FormUser
from liberaforms.models.formauth import FormAuth
from liberaforms.utils import validators
from tests.factories import FormFactory
from tests import user_creds
from tests import utils
from tests.utils import login, logout, random_slug


class TestFormEmailField():
    """ Email confirmation is enabled and disabled
        when an email field is added or removed."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor, client):
        structure = utils.create_form_structure()
        form = FormFactory(author=editor,
                           slug=utils.random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

        assert form.confirmation == {"send_email": False}
        assert not form.has_email_field() # start without an email field

    def test_email_form_field(self, client):

        form = self.properties['form']
        login(client, user_creds['editor'])

        assert not form.has_email_field()
        assert form.confirmation == {"send_email": False}

        structure = copy(form.structure)
        structure.append({
              "type": "text",
              "subtype": "email",
              "required": False,
              "label": "Email",
              "className": "form-control",
              "name": "text-1620232903350"
        })
        # add an email field
        form.start_edit_mode()
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )
        assert form.has_email_field()
        assert form.confirmation == {"send_email": True}

        # remove the field
        structure = utils.create_form_structure()
        form.start_edit_mode()
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": structure}
        )
        assert not form.has_email_field()
        assert form.confirmation == {"send_email": False}

        
