"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from tests.factories import UserFactory
from tests import user_creds, VALID_PASSWORD
from tests.utils import login


class TestChangeRole():
    """Create a guest user then change to editor."""

    @classmethod
    def setup_class(cls):
        """Create user."""
        cls.properties = {}
        cls.user = UserFactory(validated_email=True)
        cls.user.role = "guest"
        cls.user.save()

    def test_requirements(self, editor, client):
        pass

    def test_auth(self, client):
        """Test form_bp.create_new_form"""
        login(client, {'username': self.user.username, 'password': VALID_PASSWORD})

        response = client.get(
                        url_for("form_bp.create_new_form"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

    def test_change_role(self, client):
        login(client, user_creds['admin'])
        response = client.post(
                        url_for('admin_bp.set_role', user_id=self.user.id, role="editor"),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json["role"] == "editor"
        assert self.user.role == "editor"

    def test_create_form(self,client):
        login(client, {'username': self.user.username, 'password': VALID_PASSWORD})
        response = client.get(
                        url_for("form_bp.create_new_form"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- new_form_page -->' in response.data.decode()
