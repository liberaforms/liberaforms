"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
from flask import url_for
import flask_login
from liberaforms.models.answer import Answer, AnswerEdition
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory, AnswerFactory
from tests import user_creds
from tests.utils import login, logout
from tests import utils


class TestAnswerEdition():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor, client):
        structure = utils.create_form_structure()
        form = FormFactory(author=editor,
                           slug=utils.random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.anon_edition = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

        logout(client)
        utils.submit_answer(form, client,
                            answer_data={
                                'plain-answer': {
                                    "text-1620232883208": "Julia"
                                }
                            })
        answer = Answer.find(author_id=editor.id, form_id=form.id)
        assert answer.public_id
        assert answer.data["text-1620232883208"] == "Julia"
        assert form.answers.count() == 1

    def test_auth(self, editor, client):
        """Test data_display_bp.update_answer."""

        form = self.properties['form']
        answer = Answer.find(author_id=editor.id, form_id=form.id)

        logout(client)
        response = client.patch(
                    url_for('data_display_bp.update_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401

        login(client, user_creds['admin'])
        response = client.patch(
                    url_for('data_display_bp.update_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401

    def test_edit_answer_field(self, editor, client):
        """Test data_display field edition."""

        form = self.properties['form']
        answer = Answer.find(author_id=editor.id, form_id=form.id)

        login(client, user_creds['editor'])

        answer_cnt = form.answers.count()
        edited_answer_data = dict(answer.data)
        edited_answer_data["text-1620232883208"] = "Stella"
        response = client.patch(
            url_for('data_display_bp.update_answer', answer_id=answer.id),
            json={
                "item_data": edited_answer_data
            },
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json["saved"] is True
        answer_edition = AnswerEdition.find(answer_id=answer.id)
        assert answer_edition in answer.editions
        assert answer_edition.answer == answer
        assert answer_edition.user_id == editor.id
        assert answer.data["text-1620232883208"] == "Stella"
        assert answer_edition.data["text-1620232883208"] == "Julia"
        assert form.answers.count() == answer_cnt


    def test_anon_edit_answer(self, editor, client):
        """Test anon user edit form."""

        form = self.properties['form']
        answer = Answer.find(author_id=editor.id, form_id=form.id)
        answer_cnt = form.answers.count()

        edited_answer_data = dict(answer.data)
        edited_answer_data["text-1620232883208"] = "Debbie"
        edited_answer_data = {'plain-answer': json.dumps(edited_answer_data)}

        logout(client)
        response = client.post(
            answer.get_anon_edition_url(),
            data=edited_answer_data,
            follow_redirects=True,
        )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        assert answer.data["text-1620232883208"] == "Debbie"
        assert answer.editions.count() == 2
        answer_edition = answer.editions[0]
        assert answer_edition.data["text-1620232883208"] != "Debbie"

        assert form.answers.count() == 1
        assert form.answers.count() == answer_cnt
