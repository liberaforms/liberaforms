"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import g, current_app, url_for
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.utils import validators
from tests.utils import login, logout, random_slug, create_form_structure
from tests import user_creds
from tests.factories import FormFactory
import flask_login



class TestAdminPreviewForm():
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        """Prepare a form in edit_mode."""
        structure = create_form_structure()
        form = FormFactory(author=editor,
                           slug=random_slug(),
                           structure=json.loads(structure))
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

    def test_auth(self, client):
        """Test form_bp.preview
                form_bp.preview_thankyou
                form_bp.preview_expired."""
        form = self.properties['form']
        logout(client)
        response = client.get(
                        url_for('admin_bp.preview_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for('admin_bp.preview_thankyou', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for('admin_bp.preview_expired', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.get(
                        url_for('admin_bp.preview_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.get(
                        url_for('admin_bp.preview_thankyou', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.get(
                        url_for('admin_bp.preview_expired', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        login(client, user_creds['admin'])

    def test_preview(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('admin_bp.preview_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- preview_nav_menu -->' in html
        assert '<!-- preview_form_page -->' in html
        assert '<!-- render_form_partial -->' in html

    def test_preview_thankyou(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('admin_bp.preview_thankyou', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- preview_nav_menu -->' in html
        assert '<!-- preview_thankyou_page -->' in html
        assert form.get_after_submit_text_html() in html

    def test_preview_expired_form(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('admin_bp.preview_expired', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- preview_nav_menu -->' in html
        assert '<!-- preview_expired_form_page -->' in html
        assert form.get_expired_text_html() in html
