"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import g, current_app, url_for
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.utils import validators
from tests.utils import login, logout, random_slug, create_form_structure
from tests import user_creds
from tests.factories import FormFactory
import flask_login



class TestEditForm():
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, app, editor):
        """Prepare a form in edit_mode."""
        structure = create_form_structure()
        form = FormFactory(author=editor,
                           slug=random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        #with app.test_request_context():
        #    flask_login.login_user(editor)
        #    form.start_edit_mode()
        self.properties['form'] = form

    def test_auth(self, client):
        """Test form_bp.edit_form,
                form_bp.preview_edition
                form_bp.save_form."""
        logout(client)
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.edit_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.preview_edition', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.save_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['admin'])
        response = client.get(
                        url_for('form_bp.edit_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.preview_edition', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.save_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        logout(client)
        login(client, user_creds['editor'])

    def test_edit_with_alert(self, editor, client):
        form = self.properties['form']
        assert form.preview == {}
        assert editor.preferences['show_edit_alert'] is True
        response = client.get(
                        url_for('form_bp.edit_form', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- edit_form_page -->' in response.data.decode()
        assert '<!-- edit_mode_alert_modal -->' in response.data.decode()
        assert form.edit_mode["form_id"] == form.id


    def test_toggle_edit_mode_alert(self, editor, client):
        assert editor.preferences['show_edit_alert'] is True
        response = client.post(
                        url_for('user_bp.toggle_edit_mode_reminder'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert editor.preferences['show_edit_alert'] is False

    def test_edit(self, editor, client):
        form = self.properties['form']
        assert form.preview == {}
        response = client.get(
                        url_for('form_bp.edit_form', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- edit_form_page -->' in response.data.decode()
        assert '<!-- edit_mode_alert_modal -->' not in response.data.decode()
        assert form.edit_mode["form_id"] == form.id
        assert form.edit_mode["label"] == form.name
        assert form.edit_mode["editor_id"] == editor.id
        assert form.edit_mode["editor_email"] == editor.email

    def test_preview_edition(self, client):
        """Preview changes after editing."""
        form = self.properties['form']

        structure = list(form.structure)
        structure.append({
            "label": "Thursday 27th",
            "type": "checkbox-group", "name": "checkbox-group-1563698001314",
            "values": [{"label": "10h - 13h. Computer Lab management with Free software", "value": ""},
                       {"label": "18h - 20h. GIT for beginners Session 2", "value": ""}]
        })
        response = client.post(
                        url_for('form_bp.preview_edition', form_id=form.id),
                        follow_redirects=True,
                        data={
                            "preview_structure": json.dumps(structure)
                        }
                    )
        assert response.status_code == 200
        assert '<!-- preview_form_page -->' in response.data.decode()
        assert form.preview is not None
        assert form.edit_mode is not None
        last_field = form.preview["structure"][-1:][0]
        assert last_field["values"][0]["label"] == "10h - 13h. Computer Lab management with Free software"

    def test_save_preview(self, client):
        """Save changes after previewing edition."""
        form = self.properties['form']
        assert form.preview is not None
        assert form.edit_mode is not None
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.save_form', form_id=form.id, preview=True),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_configuration_page -->' in response.data.decode()
        assert form.edit_mode == {}
        assert form.preview == {}
        last_field = form.structure[-1:][0]
        assert last_field["values"][1]["label"] == "18h - 20h. GIT for beginners Session 2"
        assert form.log.count() is initial_log_count + 1

    def test_save(self, client):
        form = self.properties['form']
        login(client, user_creds['editor'])
        form.start_edit_mode()
        structure = list(form.structure)
        structure.append({
            "label": ("Tuesday 25th"),
            "type": "checkbox-group", "name": "checkbox-group-1563572627073",
            "values": [{"label": ("10h - 13h. Neutral networks. A practical presentation of our WIFI installation"), "value": ""},
                       {"label": ("18h - 20h. GIT for beginners Session 1"), "value": ""}]
        })
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.save_form', form_id=form.id),
                        follow_redirects=True,
                        data={
                            "structure": json.dumps(structure)
                        }
                    )
        assert response.status_code == 200
        assert '<!-- form_configuration_page -->' in response.data.decode()
        last_field = form.structure[-1:][0]
        assert last_field["label"] == "Tuesday 25th"
        assert form.log.count() is initial_log_count + 1
