"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from flask import g, current_app, url_for
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.utils import validators
from tests.utils import login, logout, random_slug, create_form_structure
from tests import user_creds
from tests.factories import FormFactory
import flask_login



class TestFormLookAndFeel():
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        """Prepare a form in edit_mode."""
        structure = create_form_structure()
        form = FormFactory(author=editor,
                           slug=random_slug(),
                           structure=json.loads(structure))
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

    def test_auth(self, client):
        """Test form_bp.look_and_feel."""
        form = self.properties['form']
        logout(client)
        response = client.get(
                        url_for('form_bp.look_and_feel', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['admin'])
        response = client.get(
                        url_for('form_bp.look_and_feel', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()  # admin.forms.count = 0

        login(client, user_creds['editor'])

    def test_style(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.look_and_feel', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- form_style_page -->' in html
        assert '<!-- insert_header_image_modal -->' in html
        assert '<!-- insert_background_image_modal -->' in html
        assert '<!-- font_color_selector_modal -->' in html
        assert '<!-- background_color_selector_modal -->' in html
