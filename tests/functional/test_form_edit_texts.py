"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import g, current_app, url_for
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.utils import validators
from tests.utils import login, logout, random_slug, create_form_structure
from tests import user_creds
from tests.factories import FormFactory
import flask_login



class TestEditFormTexts():
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(
            user=editor,
            form=form,
            is_editor=True,
            notifications=editor.new_form_notifications()
        ).save()
        self.properties['editor'] = editor
        self.properties['form'] = form

    def test_introduction_text(self, client):
        """Edit introduction text.
           Then check for """
        logout(client)
        form = self.properties['form']
        response = client.post(
                        url_for("form_bp.save_introduction_text", form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        login(client, user_creds['editor'])
        new_string = "Some new text"
        initial_log_count = form.log.count()
        response = client.post(
                        url_for("form_bp.save_introduction_text", form_id=form.id),
                        data={
                            "markdown": f"{form.introduction_text['markdown']} **{new_string}**"
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert f"<strong>{new_string}</strong>" in response.json['html']
        assert f"<strong>{new_string}</strong>" in form.introduction_text['html']
        assert f"**{new_string}**" in form.introduction_text['markdown']
        assert new_string in form.introduction_text['short_text']
        assert form.log.count() == initial_log_count + 1

    def test_after_submit_text(self, client):
        """Edit after submit text.
           Then check html."""
        logout(client)
        form = self.properties['form']
        response = client.post(
                        url_for("form_bp.save_after_submit_text", form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        login(client, user_creds['editor'])
        new_string = "Some new text"
        initial_log_count = form.log.count()
        response = client.post(
                        url_for("form_bp.save_after_submit_text", form_id=form.id),
                        data={
                            "markdown": f"{form.after_submit_text['markdown']} **{new_string}**"
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert f"<strong>{new_string}</strong>" in response.json['html']
        assert f"<strong>{new_string}</strong>" in form.after_submit_text['html']
        assert f"**{new_string}**" in form.after_submit_text['markdown']
        assert form.log.count() == initial_log_count + 1

    def test_expired_text(self, client):
        """Edit expired text.
           Then check html."""
        logout(client)
        form = self.properties['form']
        initial_log_count = form.log.count()
        response = client.post(
                        url_for("form_bp.save_expired_text", form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        login(client, user_creds['editor'])
        new_string = "Some new text"
        response = client.post(
                        url_for("form_bp.save_expired_text", form_id=form.id),
                        data={
                            "markdown": f"{form.expired_text['markdown']} **{new_string}**"
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert f"<strong>{new_string}</strong>" in response.json['html']
        assert f"<strong>{new_string}</strong>" in form.expired_text['html']
        assert f"**{new_string}**" in form.expired_text['markdown']
        assert form.log.count() == initial_log_count + 1
