"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from bs4 import BeautifulSoup
import flask_login
from flask import current_app, url_for
from liberaforms.models.site import Site
from liberaforms.models.user import User
from factories import UserFactory
from tests import VALID_PASSWORD, INVALID_PASSWORD
from tests import user_creds
from tests import utils
from tests.utils import logout


class TestSiteUserRegistration():
    """Creates the 'editor' user
       Tests site.invitation_only
             RESERVED_USERNAMES
             Reserved ROOT_USER email
             user_bp.create_new_user
             user_bp.validate_email
             """

    def count_errors(self, html):
        count = 0
        soup = BeautifulSoup(html, features="lxml")
        error_containers = soup.find_all("div", class_="error-messages")
        for container in error_containers:
            count += len(container.find_all("span", class_="wtf-error"))
        return count

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()
        cls.site.invitation_only = True
        cls.site.save()
        cls.properties = {}

    def test_requirements(self):
        pass

    def test_invitation_only(self, client):
        response = client.get(
                        url_for('user_bp.create_new_user'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()
        self.site.invitation_only = False
        self.site.save()
        response = client.get(
                        url_for('user_bp.create_new_user'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- new_user_form_page -->' in response.data.decode()

    def test_new_user_form(self, client):
        """ Tests RESERVED_USERNAMES
            Tests invalid email and passwords."""
        assert not self.site.invitation_only
        reserved_username = current_app.config['RESERVED_USERNAMES'][0]
        response = client.post(
                        url_for('user_bp.create_new_user'),
                        data={
                            "username": reserved_username,
                            "email": "invalid_email_string",
                            "password": INVALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_user_form_page -->' in html
        assert self.count_errors(html) == 4

    def test_root_user_new_user_form_without_root_user(self, client):
        """Test new user with reserved root_user email before root_user has registered."""
        assert not self.site.invitation_only
        assert not User.find(email=user_creds['root_user']['email'])
        new_username = utils.username_with_len(UserFactory().username)
        response = client.post(
                        url_for('user_bp.create_new_user'),
                        data={
                            "username": new_username,
                            "email": user_creds['root_user']['email'],
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_user_form_page -->' in html
        assert not User.find(username=new_username)

    def test_root_user_new_user_form_with_root_user(self, client, root_user):
        """Test new user with reserved root_user email after root_user has registered."""
        assert not self.site.invitation_only
        assert User.find(email=user_creds['root_user']['email'])
        new_username = utils.username_with_len(UserFactory().username)
        response = client.post(
                        url_for('user_bp.create_new_user'),
                        data={
                            "username": new_username,
                            "email": user_creds['root_user']['email'],
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_user_form_page -->' in html
        assert not User.find(username=new_username)
        assert self.count_errors(html) == 1


    def test_unique_new_user_form(self, client, editor):
        """Test username and email uniqueness."""
        assert not self.site.invitation_only
        logout(client)
        response = client.post(
                        url_for('user_bp.create_new_user'),
                        data={
                            "username": editor.username,
                            "email": editor.email,
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- new_user_form_page -->' in html
        assert self.count_errors(html) == 2

    def test_create_user_account(self, client):
        """Create a new user."""
        assert not self.site.invitation_only
        fake_username = utils.get_unique_username()
        fake_email = utils.get_unique_email()
        response = client.post(
                        url_for('user_bp.create_new_user'),
                        data={
                            "username": fake_username,
                            "email": fake_email,
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- unvalidated_user_page -->' in response.data.decode()
        assert flask_login.current_user.username == fake_username
        assert flask_login.current_user.role == 'editor'
        assert flask_login.current_user.is_validated is False
        self.properties["new_user"] = User.find(id=flask_login.current_user.id)
        avatar = os.path.join(current_app.config['UPLOADS_DIR'],
                              current_app.config['AVATAR_DIR'],
                              f"{self.properties['new_user'].username}.png")
        assert os.path.isfile(avatar)


    def test_validate_user_email(self, client):
        user = self.properties["new_user"]
        response = client.get(
                        url_for('user_bp.validate_email', token=user.token['token']),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_settings_page -->' in response.data.decode()
        assert user.validated_email is True
