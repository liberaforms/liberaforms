"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
from io import BytesIO
from sqlalchemy.orm.attributes import flag_modified
from flask import url_for
from liberaforms.models.site import Site
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.utils import validators
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug, create_form_structure
from tests import utils


class TestAnswerAttachment():

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.site = Site.find()

    def test_requirements(self, editor, client):
        """ Create a form with file field."""
        assert editor.uploads_enabled
        structure = create_form_structure(with_attachment=True)
        form = FormFactory(author=editor,
                           slug=random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form
        logout(client)

    def test_form_indentifier(self, client):
        """Each individual rendered form includes a unique identifier.
           Test for valid identifier."""
        form = self.properties['form']

        response = client.get(
                        form.url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert not response.is_json
        response = response.data.decode()
        # <!-- public_form_page: e5fb1f1355a04f098a7849a2fc055887 -->
        pos = response.find("<!-- public_form_page: ") + 23
        identifier = response[pos:][:32]
        assert validators.is_valid_UUID(identifier)
        self.properties["identifier"] = identifier

    def test_submit_invalid_mimetype(self, client):
        form = self.properties['form']
        self.site.mimetypes["extensions"].append("txt")
        self.site.mimetypes["mimetypes"].append("text/plain")
        flag_modified(self.site, "mimetypes")
        self.site.save()
        response = client.post(
                        url_for("public_form_bp.upload_file", slug=form.slug),
                        data={
                            "identifier": self.properties['identifier'],
                            "file-1622045746136": utils.create_file_obj("binary_content.txt"),
                        },
                    )
        assert response.status_code == 400

    def test_submit_valid_attachment(self, client, app):
        """Submit attachment.
           Backend saves attachments to tmp dir."""
        form = self.properties['form']
        self.properties['valid_attachment'] = utils.create_file_obj("valid_attachment.pdf")
        response = client.post(
                        url_for("public_form_bp.upload_file", slug=form.slug),
                        data={
                            "identifier": self.properties['identifier'],
                            "file-1622045746136": self.properties['valid_attachment'],
                        },
                    )
        assert response.status_code == 200
        assert response.is_json is True
        tmp_attachment_dir = os.path.join(app.config["TMP_DIR"], self.properties['identifier'])
        os.path.exists(f"{tmp_attachment_dir}/file-1622045746136.txt")
        os.path.exists(f"{tmp_attachment_dir}/file-1622045746136.file")
        self.properties['tmp_attachment_dir'] = tmp_attachment_dir
        with open(f"{tmp_attachment_dir}/file-1622045746136.txt", encoding="utf-8") as f:
            assert f.readline() == "valid_attachment.pdf"

    def test_submit_form(self, client):
        """Submit form without file attachment (file is perviously uploaded).
           Javascript on the client creates a list of previuosly successfully uploaded file fields."""

        form = self.properties['form']
        name = "Stella"
        response = client.post(
            form.url,
            data={
                'plain-answer': json.dumps({'text-1620232883208': name}),
                "identifier": self.properties['identifier'],
                "uploaded-file-fields": '["file-1622045746136"]',
            },
        )
        assert response.status_code == 200
        assert response.is_json
        assert "<!-- thank_you_page -->" in response.data.decode()
        answer = form.get_last_answer()
        self.properties["answer_with_attachment"] = answer
        assert answer.data['text-1620232883208'] == name
        attachment = AnswerAttachment.find(form_id=form.id, answer_id=answer.id)
        assert attachment.get_link() == answer.data['file-1622045746136']
        assert attachment.does_file_exist()
        (saved_attachemt, saved_name) = attachment.get_attachment()
        assert saved_name == "valid_attachment.pdf"
        saved_attachemt.seek(0, os.SEEK_END)
        orignal_uploaded_file = utils.create_file_obj("valid_attachment.pdf")
        orignal_uploaded_file.seek(0, os.SEEK_END)
        assert saved_attachemt.tell() == orignal_uploaded_file.tell()

    def test_download_attachment_auth(self, client):
        form = self.properties['form']
        answer = self.properties["answer_with_attachment"]
        attachment = AnswerAttachment.find(form_id=form.id, answer_id=answer.id)

        response = client.get(
            attachment.get_url(),
            follow_redirects=True,
        )
        assert response.status_code == 200
        assert "<!-- user_login_page -->" in response.data.decode()

        login(client, user_creds['admin'])
        response = client.get(
            attachment.get_url(),
            follow_redirects=True,
        )
        assert "<!-- my_forms_page -->" in response.data.decode()

    def test_download_attachment(self, client):
        form = self.properties['form']
        answer = self.properties["answer_with_attachment"]
        attachment = AnswerAttachment.find(form_id=form.id, answer_id=answer.id)

        login(client, user_creds['editor'])
        response = client.get(
            attachment.get_url(),
        )
        assert response.status_code == 200
        downloaded_file = BytesIO(response.data)
        downloaded_file.seek(0, os.SEEK_END)
        orignal_uploaded_file = utils.create_file_obj("valid_attachment.pdf")
        orignal_uploaded_file.seek(0, os.SEEK_END)
        assert downloaded_file.tell() == orignal_uploaded_file.tell()
        logout(client)

    def test_submit_form_with_attachment(self, client):
        """Submit form with file attachment.
           Backend does not save attachments included in form submission."""

        form = self.properties['form']
        name = "Julia"
        response = client.post(
                        form.url,
                        data={
                            'plain-answer': json.dumps({'text-1620232883208': name}),
                            "file-1622045746136": utils.create_file_obj("valid_attachment.pdf"),
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json
        assert "<!-- thank_you_page -->" in response.data.decode()
        answer = form.get_last_answer()
        assert answer.data['text-1620232883208'] == name
        assert 'file-1622045746136' not in answer.data
        assert not AnswerAttachment.find(form_id=form.id, answer_id=answer.id)

    def test_delete_answer_auth(self, client):
        """Test data_display_bp.delete_answer
                answers_bp.delete_all."""
        logout(client)
        form = self.properties['form']
        answer = form.get_last_answer()
        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds["admin"])
        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        login(client, user_creds["editor"])

    def test_delete_answer_and_attachment(self, client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        initial_answers_count = form.answers.count()
        answer = self.properties["answer_with_attachment"]
        attachment = AnswerAttachment.find(form_id=form.id, answer_id=answer.id)
        assert attachment

        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['deleted'] is True
        assert form.answers.count() == initial_answers_count - 1
        assert form.log.count() == initial_log_count + 1
        assert attachment.does_file_exist() is False
        assert not Answer.find(id=answer.id)
        assert not AnswerAttachment.find(form_id=form.id, answer_id=answer.id)

    def test_delete_all_answers_and_attachments(self, client):
        form = self.properties['form']

        initial_answers_count = form.answers.count()
        initial_log_count = form.log.count()
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert f'<label for="total-answers" class="visually-hidden">{initial_answers_count}</label>'  in html
        assert os.path.isdir(form.get_attachment_dir()) is True
        login(client, user_creds["editor"])
        response = client.post(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        data={
                            "totalAnswers": initial_answers_count,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert 'alert alert-success' in html
        assert '<!-- list_answers_page -->' in html
        assert form.answers.count() == 0
        assert form.log.count() == initial_log_count + 1
        assert os.path.isdir(form.get_attachment_dir()) is False
