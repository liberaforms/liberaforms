"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
from flask import url_for
from liberaforms.models.user import User
from liberaforms.models.media import Media
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment, AnswerEdition
from tests.factories import FormFactory, UserFactory
from tests import utils
from tests import with_context
from tests import user_creds, VALID_PASSWORD
from tests.utils import login, logout

class TestDeleteUser():
    """User account delete.

    Creates an answer with attachment and 1 media upload
    Tests the deletion of the user, media, forms and answer and attachments.
    """

    @classmethod
    def setup_class(cls):
        """Create user and form."""
        cls.properties = {}
        user = UserFactory(validated_email=True)
        user.uploads_enabled = True
        user.save()
        structure = utils.create_form_structure(with_attachment=True)
        form = FormFactory(author=user,
                           slug=utils.random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.anon_edition = True
        form.save()
        FormUser(user=user, form=form, is_editor=True).save()
        cls.properties['user'] = user
        cls.properties['form'] = form

    def test_requirements(self, app, client):
        """Create media
                  form
                  answer with attachment
                  answer edition."""
        user = self.properties["user"]
        form = self.properties["form"]

        with_context.save_valid_media(app, user)
        assert Media.find_all(user_id=user.id).count() == 1
        assert len(os.listdir(user.get_media_dir())) == 2  # both 1. the uploaded media file and 2. the thumbnail

        logout(client)
        # create answer with attachment
        utils.submit_answer(form, client, with_attachment=True)

        assert form.answers.count() == 1
        assert len(os.listdir(form.get_attachment_dir())) == 1
        answer = form.get_last_answer()
        # create an answer edition
        response = client.post(
            answer.get_anon_edition_url(),
            data={
                "text-1620232883208": "Julia",
            },
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert form.get_last_answer().public_id is not None
        assert answer.editions.count() == 1
        answer_edition = AnswerEdition.find(answer_id=answer.id)
        assert answer_edition is not None
        self.properties['answer_edition'] = answer_edition

    def test_auth(self, editor, client):
        """Test user_bp.delete_account."""
        user = self.properties["user"]
        logout(client)
        response = client.get(
                        url_for('user_bp.delete_account', user_id=user.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, {'username': user.username, 'password': VALID_PASSWORD})

    def test_delete_account(self, client):
        """Delete user."""
        user = self.properties["user"]
        form = self.properties["form"]
        response = client.post(
                        url_for('user_bp.delete_account', user_id=user.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- delete_account_page -->' in response.data.decode()

        wrong_username = user_creds['admin']['username']
        response = client.post(
                        url_for('user_bp.delete_account', user_id=user.id),
                        data={
                            'delete_username': wrong_username,
                            'delete_password': VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- delete_account_page -->' in response.data.decode()

        wrong_password = VALID_PASSWORD+'_'
        response = client.post(
                        url_for('user_bp.delete_account', user_id=user.id),
                        data={
                            'delete_username': user.username,
                            'delete_password': wrong_password,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- delete_account_page -->' in response.data.decode()

        answer = Answer.find(author_id=user.id, form_id=form.id)
        answer_edition = AnswerEdition.find(answer_id=answer.id)
        assert answer_edition

        response = client.post(
                        url_for('user_bp.delete_account', user_id=user.id),
                        data={
                            'delete_username': user.username,
                            'delete_password': VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()
        assert not User.find(id=user.id)
        assert not Media.find_all(user_id=user.id).count()
        assert not Form.find_all(author_id=user.id).count()
        assert not FormUser.find_all(user_id=user.id).count()
        assert not Answer.find_all(author_id=user.id).count()
        assert not AnswerAttachment.find_all(form_id=form.id).count()
        assert not AnswerEdition.find_all(id=answer_edition.id).count()
