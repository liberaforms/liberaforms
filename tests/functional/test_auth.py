"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from liberaforms.models.formuser import FormUser
from tests.factories import UserFactory, FormFactory
from tests import user_creds, VALID_PASSWORD
from tests import utils


class TestAuth():
    """Test liberaforms.utils.auth decorators against a list of endpoints."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.form_reader = UserFactory(password=VALID_PASSWORD, validated_email=True)
        cls.form_reader.save()
        cls.some_other_user = UserFactory(password=VALID_PASSWORD, validated_email=True)
        cls.some_other_user.save()

    def test_requirements(self, client, editor):
        form = FormFactory(author=editor, slug=utils.random_slug())
        form.enabled = True
        form.save()
        FormUser(user=editor, form=form, is_editor=True).save()
        FormUser(user=self.form_reader, form=form, is_editor=False).save()
        utils.submit_answer(form, client)
        self.properties['form'] = form

    def form_editor_endpoints(self) -> list:
        """A list of endpoints to check against."""
        form = self.properties['form']
        answer = form.answers[0]
        return [
            # Tests FormUser.is_editor endpoints
            [url_for('form_bp.form_editors', form_id=form.id)],
            [url_for('form_bp.form_readers', form_id=form.id)],
            [url_for('form_bp.form_user_permissions', form_id=form.id, user_email=self.form_reader.email)],
            [url_for('form_bp.expiration', form_id=form.id)],
            [url_for('answers_bp.delete_all', form_id=form.id)],
            [url_for('data_display_bp.delete_answer', answer_id=answer.id), {'method': 'delete'}],
        ]

    def admin_user_endpoints(self) -> list:
        # form = self.properties['form']
        guinea_pig_user = UserFactory(password=VALID_PASSWORD, validated_email=True)
        guinea_pig_user.save()
        return [
            # Tests User.is_admin endpoints
            [url_for('admin_bp.delete_user', user_id=guinea_pig_user.id)],
        ]

    def admin_form_endpoints(self) -> list:
        form = self.properties['form']
        return [

        ]

    def test_anonymous_auth(self, client):
        utils.logout(client)
        assert self.endpoint_test(client,
            [
                *self.form_editor_endpoints(),
                *self.admin_user_endpoints(),
                *self.admin_form_endpoints(),
            ]
        ) == False

    def test_user_auth(self, client):
        utils.login(client, {'username': self.some_other_user.username, 'password': VALID_PASSWORD})
        assert self.endpoint_test(client,
            [
                *self.admin_user_endpoints(),
                *self.admin_form_endpoints(),
            ]
        ) == False

    def test_admin_auth(self, client):
        utils.login(client, user_creds['admin'])
        assert self.endpoint_test(client,
            [
                *self.admin_user_endpoints(),
                *self.admin_form_endpoints(),
            ]
        ) == True
        assert self.endpoint_test(client,
            [
                *self.form_editor_endpoints(),
            ]
        ) == False

    def test_form_editor_auth(self, client):
        utils.login(client, user_creds['editor'])
        assert self.endpoint_test(client,
            [
                *self.form_editor_endpoints(),
            ]
        ) == True
        utils.submit_answer(self.properties['form'], client)


    def test_form_reader_auth(self, client):
        utils.login(client, {'username': self.form_reader.username, 'password': VALID_PASSWORD})
        assert self.endpoint_test(client,
            [
                *self.form_editor_endpoints(),
            ]
        ) == False

    def endpoint_test(self, client, endpoints) -> bool:
        def _test(endpoint, **kwargs):
            method = kwargs['method'] if 'method' in kwargs else 'get'
            data = kwargs['data'] if 'data' in kwargs else {}
            if method == 'delete':
                client_request = client.delete
            else:
                client_request = client.get
            response = client_request(
                endpoint,
                data=data,
                follow_redirects=False,
            )
            return response.status_code == 200

        for endpoint in endpoints:
            kwargs = endpoint[1] if len(endpoint) > 1 else {}
            if _test(endpoint[0], **kwargs) == False:
                print('endpoint test failed', endpoint)
                return False
        return True
