"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import werkzeug
from io import BytesIO
from flask import url_for
from tests import user_creds
from tests.utils import login, logout


class TestSiteServerLogs():

    def test_auth(self, editor, client):
        """Test site_bp.server_logs."""
        logout(client)
        response = client.get(
                            url_for('site_bp.server_logs'),
                            follow_redirects=True
                        )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.get(
                            url_for('site_bp.server_logs'),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        login(client, user_creds['admin'])
        response = client.get(
                        url_for('site_bp.server_logs'),
                        follow_redirects=False
                    )
        assert response.status_code == 200
        assert '<!-- server_logs_page -->' in response.data.decode()
