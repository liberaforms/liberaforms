"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from bs4 import BeautifulSoup
from flask import url_for
import flask_login
from liberaforms.models.site import Site
from tests.factories import UserFactory
from tests import VALID_PASSWORD
from tests.utils import login, logout


class TestGuestUser():

    @classmethod
    def setup_class(cls):
        """Create user."""
        cls.user = UserFactory(validated_email=True, role="guest")
        cls.user.save()
        cls.user_password = "another valid password"

    def test_login(self, client):
        response = client.post(
                        url_for('user_bp.login'),
                        data={
                            "username": self.user.username,
                            "password": VALID_PASSWORD,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert flask_login.current_user.username == self.user.username
        assert flask_login.current_user.role == "guest"
        html = response.data.decode()
        assert '<!-- my_forms_page -->' in html
        assert "<!-- guest_user_modal -->" in html
        soup = BeautifulSoup(html, features="lxml")
        navigation_menu = soup.find("nav", {"id": "ds-main-menu"})
        assert len(navigation_menu.find_all("li")) == 2

    def test_change_password(self, client):
        response = client.post(
                        url_for('user_bp.reset_password'),
                        data={
                            "password": self.user_password,
                            "password2": self.user_password
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        logout(client)
        login(client, {"username": self.user.username, "password": self.user_password})
        assert flask_login.current_user.is_authenticated

    #@pytest.mark.skip(reason="No way of currently testing this")
    #def test_change_email(self, client):
    #    """ Not impletmented """

    def test_change_language(self, client):
        new_language = 'ca'
        assert self.user.preferences['language'] != new_language
        response = client.get(
                        url_for('user_bp.change_language'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_change_language_page -->' in response.data.decode()
        response = client.post(
                        url_for('user_bp.change_language'),
                        data={
                            "language": new_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_settings_page -->' in response.data.decode()
        assert self.user.preferences['language'] == new_language
