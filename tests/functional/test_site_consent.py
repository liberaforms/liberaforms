"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import url_for
import flask_login
from liberaforms.models.site import Site
from liberaforms.models.consent import Consent
from liberaforms.models.formconsent import FormConsent
from tests.factories import ConsentFactory
from tests.factories import FormFactory
from tests import user_creds
from tests import utils
from tests.utils import login, logout


class TestSiteConsent():
    """Test List site consents
            Create a site data consent
            Edit consent
            Toggle site consent options:
                Attach it to the new user form
                Attach it to all forms
                Let users attach it to their forms
            Delete consent."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.site = Site.find()

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=utils.random_slug())
        form.save()
        assert not form.is_public()
        self.properties["form"] = form

        #initial_cnt = Consent.find_all(site_id=self.site.id).count()
        #total_test_consents = 4
        #for _ in list(range(total_test_consents)):
        #    consent = ConsentFactory(site_id=self.site.id)
        #    consent.save()
        #assert Consent.find_all(site_id=self.site.id).count() == initial_cnt + total_test_consents

    def test_auth(self, client):
        """Test consent_bp.list_site_data_consents
                consent_bp.edit_site_data_consent
                consent_bp.toggle_share_consent
                consent_bp.delete_site_data_consent."""

        consent = ConsentFactory(site_id=self.site.id)
        consent.save()

        logout(client)
        response = client.get(
                        url_for('consent_bp.list_site_data_consents'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for('consent_bp.edit_site_data_consent'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('consent_bp.toggle_share_consent', consent_id=consent.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('consent_bp.delete_site_data_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds["editor"])
        response = client.get(
                        url_for('consent_bp.list_site_data_consents'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.get(
                        url_for('consent_bp.edit_site_data_consent'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('consent_bp.toggle_share_consent', consent_id=consent.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('consent_bp.delete_site_data_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

    def test_library(self, editor, client):
        login(client, user_creds["admin"])
        response = client.get(
                        url_for('consent_bp.list_site_data_consents'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- site_consents_page -->' in response.data.decode()

    def test_new_consent(self, editor, client):
        response = client.get(
                        url_for('consent_bp.edit_site_data_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- site_edit_consent_page -->' in response.data.decode()
        initial_cnt = Consent.find_all(site_id=self.site.id).count()
        response = client.post(
                        url_for('consent_bp.edit_site_data_consent'),
                        follow_redirects=False,
                        data={
                            "name": "name",
                            "language_selector": self.site.language,
                            "language": editor.preferences["language"],
                            "label": "A label",
                            "md_text": "# Hello",
                            "required": True,
                            "wizard": ""
                        }
                    )
        assert response.status_code == 200
        assert '<!-- site_edit_consent_page -->' in response.data.decode()
        assert Consent.find_all(site_id=self.site.id).count() == initial_cnt + 1
        consent = Consent.find(site_id=self.site.id)
        self.properties["consent"] = consent

    def test_edit_consent(self, editor, client):
        consent = self.properties["consent"]
        new_name = "new site consent name"
        response = client.post(
                        url_for('consent_bp.edit_site_data_consent',
                                consent_id=consent.id),
                        follow_redirects=False,
                        data={
                            "name": new_name,
                            "language_selector": self.site.language,
                            "language": editor.preferences["language"],
                            "label": "A label",
                            "md_text": "# Hello",
                            "required": True,
                            "wizard": ""
                        }
                    )
        assert response.status_code == 200
        assert '<!-- site_edit_consent_page -->' in response.data.decode()
        assert consent.name == new_name

    def test_toggle_share_consent(self, editor, client):
        consent = self.properties["consent"]
        assert consent not in editor.get_available_consents()
        response = client.post(
                        url_for('consent_bp.toggle_share_consent',
                                consent_id=consent.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert consent in editor.get_available_consents()
        consent.shared = False
        consent.save()

    def test_delete_consent(self, client):
        consent = self.properties["consent"]
        response = client.post(
                        url_for('consent_bp.delete_site_data_consent'),
                        data={
                            "consent_id": consent.id,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['deleted'] is True
        assert not Consent.find(id=consent.id)
