"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
import flask_login
from flask import url_for
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from liberaforms.utils import validators
from tests import user_creds
from tests.utils import login, logout, random_slug
from tests.factories import FormFactory

class TestFormSettings():
    """Test the settings on form details page."""

    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(user=editor, form=form, is_editor=True).save()
        self.properties['form'] = form

    def test_auth(self, client):
        """Test form_bp.form_details
                form_bp.toggle_form_enabled
                form_bp.toggle_new_answer_notification."""

        form = self.properties['form']
        logout(client)
        response = client.get(
                        url_for('form_bp.form_details', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.toggle_form_enabled', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('form_bp.toggle_new_answer_notification', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.get(
                        url_for('form_bp.list_log', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['admin'])
        response = client.get(
                        url_for('form_bp.form_details', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.toggle_form_enabled', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('form_bp.toggle_new_answer_notification', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.get(
                        url_for('form_bp.list_log', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        login(client, user_creds['editor'])

    def test_form_details(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.form_details', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- editor_form_options_page -->' in html
        assert '<!-- form_form_options -->' in html
        assert '<!-- form_answer_options -->' in html
        assert '<!-- form_confirmation_options_partial -->' in html
        assert '<!-- form_form_shares -->' in html
        assert '<!-- form_api_endpoints -->' in html


    def test_toggle_public(self, client):
        """Tests Form.enabled bool.
           Tests for a new FormLog entry
        """
        form = self.properties['form']
        initial_enabled = form.enabled
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.toggle_form_enabled', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert form.enabled != initial_enabled
        assert response.json['toggle_public'] == form.enabled
        assert form.log.count() == initial_log_count + 1

    def test_toggle_new_answer_notification(self, editor, client):
        form = self.properties['form']
        form_user = FormUser.find(form_id=form.id, user_id=editor.id, is_editor=True)
        initial_preference = form_user.notifications['newAnswer']
        response = client.post(
                        url_for('form_bp.toggle_new_answer_notification', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert initial_preference != response.json['notification']
        saved_preference = form_user.notifications['newAnswer']
        assert saved_preference != initial_preference
        assert type(saved_preference) == type(bool())

    def test_view_log(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.list_log', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- log_list_page -->" in response.data.decode()

    #def test_duplicate_form(self, editor_client):
    #    form = self.properties['form']
    #    login(editor_client, user_creds['editor'])
    #    response = editor_client.get(
    #                    url_for('form_bp.duplicate_form', form_id=form.id),
    #                    follow_redirects=True,
    #                )
    #    assert response.status_code == 200
    #    html = response.data.decode()
    #    assert 'alert alert-info' in html
    #    assert form.introduction_text['markdown'] in html
    #    assert '<input id="slug" value=""' in html
