"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import base64
from io import BytesIO
from PIL import Image
from flask import url_for
from factories import UserFactory
import flask_login
from tests import utils
from tests.utils import login, logout
from tests import user_creds


class TestUserAvatar():
    """Upload and delete Media."""

    def setup_class(self):
        self.properties = {}

    def setup_method(self):
        # setup_method called for every method
        pass

    def test_auth(self, client):
        """Test user_bp.set_avatar."""
        logout(client)
        response = client.get(
                        url_for('user_bp.set_avatar'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.get(
                        url_for('user_bp.set_avatar'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- set_avatar_page -->' in response.data.decode()

    def test_upload_avatar(self, app, client, editor):
        user_avatar_path = os.path.join(app.config["UPLOADS_DIR"],
                                        app.config["AVATAR_DIR"],
                                        f"{editor.username}.png")
        assert not os.path.exists(user_avatar_path)
        image = Image.open("./assets/valid_avatar.png")
        with BytesIO() as buffer:
            image.save(buffer, 'png')
            base_64 = base64.b64encode(buffer.getvalue()).decode()
            avatar = f"data:image/png;base64,{base_64}"
        response = client.post(
                        url_for('user_bp.set_avatar'),
                        data={
                            "avatar_base64": avatar,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.get_json()["is_saved"] == True
        assert os.path.exists(user_avatar_path)
