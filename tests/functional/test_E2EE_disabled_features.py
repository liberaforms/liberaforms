"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from copy import copy
from flask import url_for
from liberaforms.models.formuser import FormUser
from liberaforms.models.formauth import FormAuth
from liberaforms.utils import validators
from tests.factories import FormFactory
from tests import user_creds
from tests import utils
from tests.utils import login, logout, random_slug


class TestE2EEDisabledFeatures():
    """ Disabled features are:
        - anonymous edition
        - pubic map
        - form submission confirmation
        - form exipration by field data.
    """

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor, client):
        structure = utils.create_form_structure(with_map=True)
        form = FormFactory(author=editor,
                           slug=utils.random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.is_e2ee = True  # okay, we don't have a public key however, the bool will suffice here
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form
        print(form.structure)

        assert form.confirmation == {"send_email": False}
        assert not form.has_email_field() # start without an email field (tested below)
        assert form.get_map_fields()
        assert not form.is_anon_edition_enabled()

        login(client, user_creds['editor'])

    def set_is_e2ee(self, form, state:bool):
        form.is_e2ee = state
        form.save()

    def test_anon_edition(self, client):
        """Test Editor's toggle endpoint."""

        form = self.properties['form']
        assert not form.anon_edition
        assert form.is_e2ee
        assert not form.is_anon_edition_enabled()

        response = client.post(
            url_for('form_bp.toggle_anon_edition', form_id=form.id),
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert response.is_json
        assert not form.anon_edition
        assert response.json["anon_edition"] is False

    def test_publish_map(self, client):
        """Tests Editor toggle and
                 public answers HTML and JSON endpoints."""

        form = self.properties['form']

        # With E2EE enabled
        self.set_is_e2ee(form, True)
        response = client.post(
            url_for('form_bp.toggle_publish_map', form_id=form.id),
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert response.is_json
        assert response.json["enable_public_map"] is False
        assert not form.is_map_enabled()

        response = client.get(
            url_for('public_answers_bp.as_map', slug=form.slug),
            follow_redirects=False,
        )
        assert response.status_code == 404
        assert '<!-- page_not_found_404 -->' in response.data.decode()

        response = client.get(
            url_for('public_answers_bp.map_json_data', form_id=form.id),
            follow_redirects=False,
        )
        assert response.status_code == 404
        assert response.is_json
        assert response.json == "Not found"

        # Without E2EE enabled
        self.set_is_e2ee(form, False)
        assert not form.is_map_enabled()
        response = client.post(
            url_for('form_bp.toggle_publish_map', form_id=form.id),
            follow_redirects=False,
        )
        assert response.json["enable_public_map"] is True
        assert form.is_map_enabled()

        response = client.get(
            url_for('public_answers_bp.as_map', slug=form.slug),
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert '<!-- answers_map_page -->' in response.data.decode()

        response = client.get(
            url_for('public_answers_bp.map_json_data', form_id=form.id),
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert isinstance(response.json["meta"]["centerPoint"], str)

    def test_email_form_field(self, client):
        """Add and email field to the form.
           E2EE forms must not enable confirmation"""

        form = self.properties['form']
        login(client, user_creds['editor'])

        assert not form.has_email_field()
        assert form.confirmation == {"send_email": False}

        self.set_is_e2ee(form, True)
        assert form.is_e2ee

        structure = copy(form.structure)
        structure.append({
              "type": "text",
              "subtype": "email",
              "required": False,
              "label": "Email",
              "className": "form-control",
              "name": "text-1620232903350"
        })
        form.start_edit_mode()
        client.post(
            url_for('form_bp.save_form', form_id=form.id),
            follow_redirects=True,
            data={"structure": json.dumps(structure)}
        )
        assert form.has_email_field()
        assert form.confirmation == {"send_email": False}

    def test_toggle_confirmation(self, client):
        form = self.properties['form']

        assert form.is_e2ee
        assert form.has_email_field()
        response = client.post(
            url_for('form_bp.toggle_form_send_confirmation', form_id=form.id),
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert response.is_json
        assert response.json["send_confirmation"] == False

    def test_form_exipration_by_field(self, client):
        form = self.properties['form']

        assert form.is_e2ee
        assert form.get_number_fields()

        response = client.post(
            url_for('form_bp.set_expiry_field_condition', form_id=form.id),
            data={
                "field_name": "number-1620224716308",
                "condition": 2
            },
            follow_redirects=False,
        )
        assert response.status_code == 406
        assert response.is_json
