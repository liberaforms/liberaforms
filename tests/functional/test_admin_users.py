"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import current_app, url_for
from tests.factories import UserFactory
from tests import user_creds, VALID_PASSWORD
from tests.utils import login, logout
from liberaforms.utils.utils import string_to_bytes


#@pytest.mark.usefixtures("client")
class TestAdministerUsers():
    """ The admin can set some user properties. Test those."""

    @classmethod
    def setup_class(cls):
        cls.guinea_pig_user = UserFactory(validated_email=True)
        cls.guinea_pig_user.save()

    def test_requirements(self, client):
        pass

    def test_auth(self, client):
        """Test admin_bp.set_role
                admin_bp.toggle_uploads_enabled
                admin_bp.toggle_user_form_creation
                admin_bp.change_user_password
                admin_bp.set_user_upload_limit."""
        logout(client)
        response = client.post(
                        url_for('admin_bp.set_role', user_id=self.guinea_pig_user.id, role="guest"),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True

        response = client.post(
            url_for('admin_bp.toggle_user_form_creation', user_id=self.guinea_pig_user.id),
        )
        assert response.status_code == 401
        assert response.is_json is True

        response = client.post(
            url_for('admin_bp.toggle_uploads_enabled', user_id=self.guinea_pig_user.id),
            follow_redirects=False,
        )
        assert response.status_code == 401
        assert response.is_json is True

        response = client.get(
            url_for('admin_bp.change_user_password', user_id=self.guinea_pig_user.id),
            follow_redirects=True,
        )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        response = client.get(
            url_for('admin_bp.set_user_upload_limit', user_id=self.guinea_pig_user.id),
            follow_redirects=True,
        )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.post(
                        url_for('admin_bp.set_role', user_id=self.guinea_pig_user.id, role="guest"),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
            url_for('admin_bp.toggle_user_form_creation', user_id=self.guinea_pig_user.id),
        )
        assert response.status_code == 401

        response = client.post(
            url_for('admin_bp.toggle_uploads_enabled', user_id=self.guinea_pig_user.id),
            follow_redirects=False,
        )
        assert response.status_code == 401

        response = client.get(
            url_for('admin_bp.change_user_password', user_id=self.guinea_pig_user.id),
            follow_redirects=True,
        )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        response = client.get(
            url_for('admin_bp.set_user_upload_limit', user_id=self.guinea_pig_user.id),
            follow_redirects=True,
        )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        login(client, user_creds['admin'])

    def test_set_role(self, client):
        """ Test set role 'admin' """
        #login(client, user_creds['admin'])
        new_role = 'guest'
        initial_role = self.guinea_pig_user.role
        assert initial_role != new_role
        response = client.post(
            url_for('admin_bp.set_role', user_id=self.guinea_pig_user.id, role=new_role),
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json["role"] == new_role
        assert self.guinea_pig_user.role == new_role

    def test_toggle_uploads(self, client):
        """ Test toggle enable uploads """

        initial_uploads_enabled = self.guinea_pig_user.uploads_enabled
        #login(client, user_creds['admin'])
        response = client.post(
            url_for('admin_bp.toggle_uploads_enabled', user_id=self.guinea_pig_user.id),
            follow_redirects=False,
        )
        assert response.status_code == 200
        assert response.is_json is True
        assert self.guinea_pig_user.role == 'guest'
        assert response.json == {"uploads_enabled": self.guinea_pig_user.uploads_enabled}
        assert initial_uploads_enabled == self.guinea_pig_user.uploads_enabled
        self.guinea_pig_user.role='editor'
        self.guinea_pig_user.save()
        response = client.post(
            url_for('admin_bp.toggle_uploads_enabled', user_id=self.guinea_pig_user.id),
            follow_redirects=False,
        )
        assert initial_uploads_enabled != self.guinea_pig_user.uploads_enabled
        # set user.uploads_enabled to False to continue testing
        #user_creds['tested_user'].uploads_enabled = False
        #user_creds['tested_user'].save()


    def test_change_user_password(self, client):
        response = client.get(
                        url_for('admin_bp.change_user_password', user_id=self.guinea_pig_user.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- admin_change_user_password_page -->' in response.data.decode()

        initial_password_hash = self.guinea_pig_user.password_hash
        response = client.post(
                        url_for('admin_bp.change_user_password', user_id=self.guinea_pig_user.id),
                        data = {
                            "username": f"{self.guinea_pig_user.username}-not"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- admin_change_user_password_page -->' in response.data.decode()
        assert self.guinea_pig_user.password_hash == initial_password_hash
        response = client.post(
                        url_for('admin_bp.change_user_password', user_id=self.guinea_pig_user.id),
                        data = {
                            "username": self.guinea_pig_user.username
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- admin_change_user_password_page -->' in response.data.decode()
        assert self.guinea_pig_user.password_hash != initial_password_hash
        # required by test_disabled_user_form_creation()
        self.guinea_pig_user.password_hash = initial_password_hash
        self.guinea_pig_user.save()


    def test_set_uploads_limit(self, client):
        response = client.get(
                        url_for('admin_bp.set_user_upload_limit', user_id=self.guinea_pig_user.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- user_upload_limit_page -->' in response.data.decode()
        over_max_limit = {'size': '10000000', 'unit': 'GB'}
        response = client.post(
                        url_for('admin_bp.set_user_upload_limit', user_id=self.guinea_pig_user.id),
                        data=over_max_limit,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- inspect_user_page -->' in response.data.decode()
        default_limit = string_to_bytes(current_app.config['TOTAL_UPLOADS_LIMIT'])
        assert self.guinea_pig_user.uploads_limit == default_limit
        initial_limit = self.guinea_pig_user.uploads_limit
        valid_limit = {'size': '100', 'unit': 'KB'}
        response = client.post(
                        url_for('admin_bp.set_user_upload_limit', user_id=self.guinea_pig_user.id),
                        data=valid_limit,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- inspect_user_page -->' in response.data.decode()
        assert initial_limit != self.guinea_pig_user.uploads_limit


    def test_disabled_user_form_creation(self, client):
        assert self.guinea_pig_user.can_create_forms()
        assert self.guinea_pig_user.role == "editor"
        response = client.post(
            url_for('admin_bp.toggle_user_form_creation', user_id=self.guinea_pig_user.id),
        )
        assert response.status_code == 200
        assert response.is_json is True
        assert not self.guinea_pig_user.can_create_forms()

        login(client, {"username": self.guinea_pig_user.username, "password": VALID_PASSWORD})
        response = client.get(
                        url_for("form_bp.create_new_form"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- new_form_page -->' in response.data.decode()
        assert '<!-- disabled_new_forms -->' in response.data.decode()

        response = client.post(
                        url_for("form_bp.create_new_form"),
                        data={
                            "name": "A name",
                            "slug": "a-name",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- new_form_page -->' in response.data.decode()
