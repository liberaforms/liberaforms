"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
from flask import url_for
import flask_login
from liberaforms.models.answer import Answer, AnswerEdition, AnswerAttachment
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory, AnswerFactory
from tests import user_creds
from tests.utils import login, logout
from tests import utils


class TestAnswerAttachmentEdition():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor, client):
        structure = utils.create_form_structure(with_attachment=True)
        form = FormFactory(author=editor,
                           slug=utils.random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.anon_edition = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

        logout(client)
        utils.submit_answer(form, client,
                            answer_data={
                                'plain-answer': {
                                    "text-1620232883208": "Julia"
                                }
                            },
                            with_attachment=True)
        answer = Answer.find(author_id=editor.id, form_id=form.id)
        assert answer.public_id
        assert answer.data["text-1620232883208"] == "Julia"
        assert AnswerAttachment.find(form_id=form.id, answer_id=answer.id)
        assert form.answers.count() == 1
        assert len(os.listdir(form.get_attachment_dir())) == 1

    def test_auth(self, editor, client):
        """Test data_display_bp.update_answer."""

        form = self.properties['form']
        answer = Answer.find(author_id=editor.id, form_id=form.id)

        logout(client)
        response = client.patch(
                    url_for('data_display_bp.update_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401

        login(client, user_creds['admin'])
        response = client.patch(
                    url_for('data_display_bp.update_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401


    def test_anon_edit_answer_change_file(self, editor, client):
        """Test anon user edit form."""

        form = self.properties['form']
        answer = Answer.find(author_id=editor.id, form_id=form.id)
        answer_cnt = form.answers.count()

        assert len(os.listdir(form.get_attachment_dir())) == 1
        logout(client)

        identifier = form.gen_identifier()
        response = client.post(
            url_for("public_form_bp.upload_file", slug=form.slug),
            data={
                "identifier": identifier,
                "file-1622045746136": utils.create_file_obj("valid_attachment.pdf"),
            },
        )
        assert response.status_code == 200

        edited_answer_data = dict(answer.data)
        edited_answer_data["identifier"] = identifier
        edited_answer_data['plain-answer'] = json.dumps({'text-1620232883208': 'Debbie'})
        edited_answer_data["uploaded-file-fields"] = '["file-1622045746136"]'
        edited_answer_data.pop('file-1622045746136')

        response = client.post(
            answer.get_anon_edition_url(),
            data=edited_answer_data,
            follow_redirects=True,
        )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        assert answer.data["text-1620232883208"] == "Debbie"
        assert answer.editions.count() == 1
        answer_edition = answer.editions[0]
        assert answer_edition.data["text-1620232883208"] != "Debbie"

        assert answer_edition.data["file-1622045746136"] != answer.data["file-1622045746136"]
        assert form.answers.count() == 1
        assert len(os.listdir(form.get_attachment_dir())) == 1
        assert form.answers.count() == answer_cnt

    def test_anon_edit_answer_leave_file(self, editor, client):
        """Test remove file from answer."""

        form = self.properties['form']
        answer = Answer.find(author_id=editor.id, form_id=form.id)
        answer_cnt = form.answers.count()

        identifier = form.gen_identifier()
        edited_answer_data = dict(answer.data)
        edited_answer_data["identifier"] = identifier
        edited_answer_data['plain-answer'] = json.dumps({'text-1620232883208': 'Vicky'})
        edited_answer_data["uploaded-file-fields"] = '[]'
        edited_answer_data['dummy-file-1622045746136'] = "file-1622045746136"
        edited_answer_data.pop('file-1622045746136')

        assert len(os.listdir(form.get_attachment_dir())) == 1
        response = client.post(
            answer.get_anon_edition_url(),
            data=edited_answer_data,
            follow_redirects=True,
        )
        assert response.status_code == 200

        assert answer.data["text-1620232883208"] == "Vicky"
        assert answer.editions.count() == 2
        answer_edition = answer.editions[0]
        assert answer_edition.data["file-1622045746136"] == answer.data["file-1622045746136"]

        assert "file-1622045746136" in answer.data.keys()
        assert len(os.listdir(form.get_attachment_dir())) == 1
        assert form.answers.count() == answer_cnt

    def test_anon_edit_answer_remove_file(self, editor, client):
        """Test remove file from answer."""

        form = self.properties['form']
        answer = Answer.find(author_id=editor.id, form_id=form.id)
        answer_cnt = form.answers.count()

        identifier = form.gen_identifier()
        edited_answer_data = dict(answer.data)
        edited_answer_data["identifier"] = identifier
        edited_answer_data['plain-answer'] = json.dumps({'text-1620232883208': 'Jackie'})
        edited_answer_data["uploaded-file-fields"] = '[]'
        edited_answer_data.pop('file-1622045746136')

        assert len(os.listdir(form.get_attachment_dir())) == 1
        response = client.post(
            answer.get_anon_edition_url(),
            data=edited_answer_data,
            follow_redirects=True,
        )
        assert response.status_code == 200

        assert answer.data["text-1620232883208"] == "Jackie"
        assert answer.editions.count() == 3
        assert "file-1622045746136" not in answer.data.keys()
        assert len(os.listdir(form.get_attachment_dir())) == 0
        assert form.answers.count() == answer_cnt
