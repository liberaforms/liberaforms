"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2024 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
from flask import url_for
import flask_login
from liberaforms.models.site import Site
from liberaforms.domain.custom_text import CustomText
from liberaforms.utils import sanitizers
from factories import UserFactory
from tests import user_creds, VALID_PASSWORD
from tests.utils import login, logout


class TestSiteCustomLanguages():

    @classmethod
    def setup_class(cls):
        site = Site.find()
        cls.site = site
        cls.site.language = "en"
        cls.site.custom_languages = ["en"]
        cls.site.change_default_language("en")
        cls.site.resources_menu = {
            "enabled": True,
            "languages": {cls.site.language: [["Liberaforms", "https://www.liberaforms.org"]]
        }}
        cls.site.save()
        cls.guinea_pig_user = UserFactory()
        cls.guinea_pig_user.preferences["language"] = "ca"
        cls.guinea_pig_user.save()

    def compare_short_text(self, blurb):
        blurb = sanitizers.remove_html_tags(blurb).replace("\r\n", " ") \
                                                  .replace("\n", " ") \
                                                  .replace("  ", " ")
        assert self.site.get_short_description().rstrip("...") in blurb

    def test_requirements(self, app, client):
        logout(client)
        assert self.site.language == os.environ['DEFAULT_LANGUAGE'] == "en"
        assert len(self.site.custom_languages) == 1
        assert self.site.language == self.site.custom_languages[0]
        with app.test_request_context("/"):
            app.preprocess_request()
            assert ["Liberaforms", "https://www.liberaforms.org"] in CustomText("resources-menu").get_text()
            assert "Privacy statements" in CustomText("wizard-disclaimer").get_text()
            assert CustomText("other-information").get_text() == ""
            assert CustomText("new-form-msg").get_text() == ""
            blurb = CustomText("frontpage-blurb").get_text(as_html=True)
            assert "Use it, share it, improve it!" in blurb
            self.compare_short_text(blurb)

    def test_change_predefined_default_lang(self, app, client):
        """Test languages that have default markdown texts in ASSETS_DIR."""
        self.site.language = "ca"
        self.site.custom_languages = ["ca", "en"]
        self.site.change_default_language("ca")
        with app.test_request_context("/"):  # sends empty "Accept-Language": ""
            app.preprocess_request()
            blurb = CustomText("frontpage-blurb").get_text(as_html=True)
            assert "Usa'l, comparteix-lo i millora'l!" in blurb
            self.compare_short_text(blurb)
            assert "declaracions de privadesa" in CustomText("wizard-disclaimer").get_text(as_html=True)

        with app.test_request_context("/", headers={"Accept-Language": "ca-ES"}):
            app.preprocess_request()
            assert "Usa'l, comparteix-lo i millora'l!" in CustomText("frontpage-blurb").get_text(as_html=True)
            assert "declaracions de privadesa" in CustomText("wizard-disclaimer").get_text(as_html=True)

        with app.test_request_context("/", headers={"Accept-Language": "en-NZ"}):
            app.preprocess_request()
            assert "Use it, share it, improve it!" in CustomText("frontpage-blurb").get_text(as_html=True)
            assert "Privacy statements" in CustomText("wizard-disclaimer").get_text(as_html=True)

        with app.test_request_context("/", headers={"Accept-Language": "xx-XX"}):
            app.preprocess_request()
            assert "Usa'l, comparteix-lo i millora'l!" in CustomText("frontpage-blurb").get_text(as_html=True)
            assert "declaracions de privadesa" in CustomText("wizard-disclaimer").get_text(as_html=True)

    def test_auth(self, client):
        """Test site_bp.custom_languages
                site_bp.add_custom_language
                site_bp.set_default_custom_language
                site_bp.remove_custom_language"""
        logout(client)
        response = client.get(
                            url_for('site_bp.custom_languages'),
                            follow_redirects=True
                        )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                            url_for('site_bp.add_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401
        response = client.post(
                            url_for('site_bp.set_default_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401
        response = client.post(
                            url_for('site_bp.remove_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401

        login(client, user_creds['editor'])
        response = client.get(
                            url_for('site_bp.custom_languages'),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                            url_for('site_bp.add_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401
        response = client.post(
                            url_for('site_bp.set_default_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401
        response = client.post(
                            url_for('site_bp.remove_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401

        login(client, user_creds['admin'])
        response = client.get(
                            url_for('site_bp.custom_languages'),
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert '<!-- site_custom_languages_page -->' in response.data.decode()

    def test_add_custom_language(self, client):
        """Test a valid lang_code."""
        assert "es" not in self.site.custom_languages
        response = client.post(
                            url_for('site_bp.add_custom_language'),
                            data={
                                "lang_code": "es"
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert response.is_json is True
        assert "es" in self.site.custom_languages

    def test_add_invalid_custom_language(self, client):
        """Test an invalid lang_code."""
        initial_languages = self.site.custom_languages
        response = client.post(
                            url_for('site_bp.add_custom_language'),
                            data={
                                "lang_code": "123"  # not a valid lang_code
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 406
        assert response.is_json is True
        assert self.site.custom_languages == initial_languages

    def test_remove_default_custom_language(self, client):
        assert self.site.language == "ca"
        assert "ca" in self.site.custom_languages
        response = client.post(
                            url_for('site_bp.remove_custom_language'),
                            data={
                                "lang_code": self.site.language
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 406
        assert response.is_json is True
        assert self.site.language == "ca"

    def test_set_default_custom_language(self, client):
        assert self.site.language != "es"
        response = client.post(
                            url_for('site_bp.set_default_custom_language'),
                            data={
                                "lang_code": "es"
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert response.is_json is True
        assert self.site.language == "es"

    def test_remove_custom_language(self, app, client, editor):
        """Remove "en" and test for "es" in CustomText.get_text"""
        assert self.site.language == "es"
        assert "en" in self.site.custom_languages
        response = client.post(
                            url_for('site_bp.remove_custom_language'),
                            data={
                                "lang_code": "en"
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert response.is_json is True
        assert "en" not in self.site.custom_languages
        assert len(json.loads(response.data)['custom_languages']) == len(self.site.custom_languages)

    def test_best_lang_match(self, client):
        assert self.site.language == "es"
        logout(client)
        response = client.get("/")
        assert "¡Úsalo, compártelo y mejóralo!" in response.data.decode()
        assert "ca" in self.site.custom_languages
        login(client, {"username": self.guinea_pig_user.username, "password": VALID_PASSWORD})
        assert flask_login.current_user.preferences["language"] == "ca"
        assert "Usa'l, comparteix-lo i millora'l!" in CustomText("frontpage-blurb").get_text(as_html=True)
        assert "declaracions de privadesa" in CustomText("wizard-disclaimer").get_text(as_html=True)

    def test_missing_custom_text(self, app, client):
        self.site.custom_languages.append("sn")
        #self.site.language = "sn"
        #self.site.change_default_language("sn")
        with app.test_request_context("/", headers={"Accept-Language": "sn-ZW"}):
            app.preprocess_request()
            assert CustomText("frontpage-blurb").get_text(as_html=True)
        #self.site.language = initial_site_lang
        #self.site.change_default_language(initial_site_lang)


class TestSiteBlurb():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()
        cls.guinea_pig_user = UserFactory()
        cls.guinea_pig_user.preferences["language"] = "ca"
        cls.guinea_pig_user.save()

    #def test_requirements(self, app, client, editor):
    #    pass

    def test_auth(self, client):
        "Test site_bp.edit_blurb."
        logout(client)
        response = client.get(
                        url_for("site_bp.edit_blurb"),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.get(
                        url_for("site_bp.edit_blurb"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        logout(client)
        login(client, user_creds['admin'])
        response = client.get(
                        url_for("site_bp.edit_blurb"),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- edit_blurb_page -->' in response.data.decode()

    def test_edit_blurb(self, client, editor):
        """Post markdown and tests resulting HTML and short_text."""
        assert self.site.language == "es"
        response = client.post(
                    url_for("site_bp.edit_blurb"),
                    data={
                        'blurb': "# Tested !!\nline1\nline2",
                        "language": self.site.language
                    },
                    follow_redirects=True,
                )
        assert response.status_code == 200
        front_page = self.site.blurb["front_page"][self.site.language]
        assert '<h1>Tested !!</h1>' in front_page['html']
        assert '# Tested !!' in front_page['markdown']
        assert 'Tested !!' in self.site.blurb['short_text']
        assert "Usa'l, comparteix-lo i millora'l!" in self.site.blurb["front_page"]["ca"]["html"]

    def test_add_language_blurb(self, client):
        assert "sn" in self.site.custom_languages
        login(client, user_creds['admin'])
        response = client.post(
                    url_for("site_bp.edit_blurb"),
                    data={
                        'blurb': "# chiShona !!\nline1\nline2",
                        "language": "sn"
                    },
                    follow_redirects=True,
                )
        assert response.status_code == 200
        front_page = self.site.blurb["front_page"]["sn"]
        assert '<h1>chiShona !!</h1>' in front_page['html']
        assert '# chiShona !!' in front_page['markdown']

    def test_blurb_best_lang(self, app, client):
        logout(client)
        assert "Tested !!" in client.get("/").data.decode()
        login(client, {"username": self.guinea_pig_user.username, "password": VALID_PASSWORD})
        assert self.guinea_pig_user.preferences["language"] == "ca"
        assert "Usa'l, comparteix-lo i millora'l!" in client.get("/").data.decode()
        logout(client)
        with app.test_request_context("/", headers={"Accept-Language": "sn-ZW"}):
            app.preprocess_request()
            assert "sn" in self.site.custom_languages
            assert "chiShona !!" in CustomText("frontpage-blurb").get_text(as_html=True)


class TestWizardDisclaimer():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()
        cls.guinea_pig_user = UserFactory()
        cls.guinea_pig_user.preferences["language"] = "ca"
        cls.guinea_pig_user.validated_email = True
        cls.guinea_pig_user.save()

    #def test_requirements(self, app, client, editor):
    #    pass

    def test_auth(self, client):
        "Test site_bp.edit_wizard_disclaimer."
        logout(client)
        response = client.get(
                        url_for("site_bp.edit_wizard_disclaimer"),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.get(
                        url_for("site_bp.edit_wizard_disclaimer"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        logout(client)
        login(client, user_creds['admin'])
        response = client.get(
                        url_for("site_bp.edit_wizard_disclaimer"),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- edit_disclaimer_page -->' in response.data.decode()

    def test_edit_disclaimer(self, client, editor):
        """Post markdown and tests resulting HTML and short_text."""
        assert self.site.language == "es"
        response = client.post(
                    url_for("site_bp.edit_wizard_disclaimer"),
                    data={
                        'disclaimer': "## Edited Disclaimer !!",
                        "language": self.site.language
                    },
                    follow_redirects=True,
                )
        assert response.status_code == 200
        disclaimer = self.site.data_protection["disclaimer"]["languages"][self.site.language ]
        assert '<h2>Edited Disclaimer !!</h2>' in disclaimer['html']
        assert '## Edited Disclaimer !!' in disclaimer['markdown']

    def test_edited_disclaimer_best_lang(self, client):
        self.guinea_pig_user.preferences["language"] = "es"
        self.guinea_pig_user.save()
        login(client, {"username": self.guinea_pig_user.username, "password": VALID_PASSWORD})
        assert "<h2>Edited Disclaimer !!</h2>" in client.get(url_for("consent_bp.wizard_for_user")).data.decode()
        self.guinea_pig_user.preferences["language"] = "ca"
        self.guinea_pig_user.save()
        login(client, {"username": self.guinea_pig_user.username, "password": VALID_PASSWORD})
        assert self.guinea_pig_user.preferences["language"] == "ca"
        assert "Aquest assistent us ajudarà" in client.get(url_for("consent_bp.wizard_for_user")).data.decode()
        logout(client)
