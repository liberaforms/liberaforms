"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import pytest
import werkzeug
from io import BytesIO
import mimetypes
from flask import url_for
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.models.log import FormLog
from tests import user_creds
from tests.factories import FormFactory, AnswerFactory
from tests import utils
from tests.utils import login, logout, random_slug, create_form_structure


class TestDeleteForm():
    """Delete a form with answers and attachments."""

    @classmethod
    def setup_class(cls):
        cls.properties={}

    def test_requirements(self, editor, client):
        """Create form and answers with attachment."""
        form = FormFactory(author=editor,
                           slug=random_slug(),
                           structure=json.loads(create_form_structure(with_attachment=True, with_email=True)))
        form.enabled = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['editor'] = editor
        self.properties['form'] = form

        #print(form)

        logout(client)
        total_answers = 3
        answer_cnt = 0
        while answer_cnt < total_answers:
            answer_cnt = answer_cnt+1
            utils.submit_answer(form, client, with_attachment=True, with_confirmation=True)

        assert form.answers.count() == total_answers
        assert len(os.listdir(form.get_attachment_dir())) == total_answers

    def test_auth(self, client):
        """Tests form_bp.delete_form."""
        logout(client)
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.delete_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()
        login(client, user_creds['admin'])
        response = client.get(
                        url_for('form_bp.delete_form', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

    def test_delete_form(self, editor, client):
        """Delete the form and check the answers and attachments are deleted."""
        form = self.properties['form']
        initial_answers_count = form.answers.count()
        login(client, user_creds['editor'])
        response = client.get(
                        url_for('form_bp.delete_form', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- delete_form_page -->' in html
        assert f' and its {initial_answers_count} answers' in html
        # test incorrect slug
        response = client.post(
                        url_for('form_bp.delete_form', form_id=form.id),
                        data={
                            "slug": f"{form.slug}-wrong"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- delete_form_page -->' in response.data.decode()
        # test correct slug
        response = client.post(
                        url_for('form_bp.delete_form', form_id=form.id),
                        data={
                            "slug": form.slug
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        assert Form.find(id=form.id) is None
        assert Answer.find_all(form_id=form.id).count() == 0
        assert FormUser.find_all(form_id=form.id).count() == 0
        assert AnswerAttachment.find_all(form_id=form.id).count() == 0
        assert os.path.isdir(form.get_attachment_dir()) is False
        assert FormLog.find_all(form_id=form.id).count() == 0
