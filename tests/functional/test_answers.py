"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from liberaforms.models.answer import Answer
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory, AnswerFactory
from tests import user_creds
from tests.utils import login, logout, random_slug


class TestAnswers():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor, client):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        while form.answers.count() < 10:
            answer = AnswerFactory(form_id=form.id, author_id=editor.id)
            answer.save()
        self.properties['form'] = form

    def test_auth(self, client):
        """Test answers_bp.list_answers
                data_display_bp.toggle_answer_mark
                data_display_bp.delete_answer
                answers_bp.delete_all."""

        form = self.properties['form']
        answer = form.get_last_answer()

        logout(client)
        response = client.get(
                        url_for('answers_bp.list_answers', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_login_page -->' in response.data.decode()
        response = client.post(
                    url_for('data_display_bp.toggle_answer_mark', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.is_json is True
        assert response.status_code == 401
        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['admin'])
        response = client.get(
                        url_for('answers_bp.list_answers', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                    url_for('data_display_bp.toggle_answer_mark', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.is_json is True
        assert response.status_code == 401
        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        login(client, user_creds['editor'])

    def test_list_answers(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('answers_bp.list_answers', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- list_answers_page -->' in response.data.decode()

    def test_toggle_marked_answer(self, client):
        form = self.properties['form']
        answer = form.get_last_answer()
        initial_marked = vars(answer)['marked']
        response = client.post(
                    url_for('data_display_bp.toggle_answer_mark', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['marked'] != initial_marked
        assert answer.marked != initial_marked

    def test_delete_answer(self, client):
        form = self.properties['form']
        initial_answers_count = form.answers.count()
        assert initial_answers_count > 0
        initial_log_count = form.log.count()
        answer = form.get_last_answer()
        assert Answer.find(id=answer.id)
        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['deleted'] is True
        assert Answer.find(id=answer.id) is None
        assert form.answers.count() == initial_answers_count - 1
        assert form.log.count() == initial_log_count + 1

    def test_delete_all_answers(self, client):
        form = self.properties['form']
        initial_answers_count = form.answers.count()
        initial_log_count = form.log.count()
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- delete_all_answers_page -->' in html
        assert f'<label for="total-answers" class="visually-hidden">{initial_answers_count}</label>' in html
        response = client.post(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        data={
                            "totalAnswers": initial_answers_count,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert 'alert alert-success' in html
        assert '<!-- list_answers_page -->' in html
        assert form.answers.count() == 0
        assert form.log.count() == initial_log_count + 1
