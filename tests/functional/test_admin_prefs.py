"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import ast
import pytest
from flask import url_for
import flask_login

from tests import user_creds
from tests.utils import login, logout


class TestAdminPreferences():
    """ Tests admin's admin preferences
    """
    def test_requirements(self, client):
        logout(client)

    def test_toggle_new_user_notification(self, client):
        """ Tests admin permission
            Tests POST only
            Tests toggle bool
        """
        response = client.post(
                        url_for('admin_bp.toggle_newuser_notification'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        login(client, user_creds['admin'])
        response = client.get(
                        url_for('admin_bp.toggle_newuser_notification'),
                        follow_redirects=False,
                    )
        assert response.status_code == 405
        notification = flask_login.current_user.admin["notifyNewUser"]
        response = client.post(
                        url_for('admin_bp.toggle_newuser_notification'),
                        follow_redirects=False
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert flask_login.current_user.admin["notifyNewUser"] != notification
        assert isinstance(flask_login.current_user.admin["notifyNewUser"], bool)

    def test_toggle_new_form_notification(self, client):
        """ Tests admin permission
            Tests POST only
            Tests toggle bool
        """
        logout(client)
        response = client.post(
                        url_for('admin_bp.toggle_newform_notification'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        login(client, user_creds['admin'])
        response = client.get(
                        url_for('admin_bp.toggle_newform_notification'),
                        follow_redirects=False,
                    )
        assert response.status_code == 405
        notification = flask_login.current_user.admin["notifyNewForm"]
        response = client.post(
                        url_for('admin_bp.toggle_newform_notification'),
                        follow_redirects=False)
        assert response.status_code == 200
        assert response.is_json is True
        assert flask_login.current_user.admin["notifyNewForm"] != notification
        assert isinstance(flask_login.current_user.admin["notifyNewUser"], bool)

    def test_toggle_newuser_uploadsenabled(self, client):
        pass
