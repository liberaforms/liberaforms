# Formularios éticos con LiberaForms

[LiberaForms](https://liberaforms.org/gl) é unha ferramenta de software libre pensada e desenvolvida como **infraestructura comunitaria, libre e ética** que permite crear e xestionar formularios que respectan os dereitos dixitais das persoas que fan uso deles.

Con LiberaForms podes consultar, editar e descargar as respostas recibidas; incluír un recadro para solicitar o consentimento respecto da Lei de Protección de Datos; colaborar con outros usuarios compartindo permisos; e moitas cousas máis! Ademais, LiberaForms é cultura libre baixo licenza AGPLv3.

Úsao, compárteo y mellórao!