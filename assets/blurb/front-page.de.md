# Ethische Formulare mit LiberaForms

[LiberaForms](https://liberaforms.org/de) ist eine Freie Software, die als **gemeinsame, freie und ethische Infrastruktur** entwickelt wird. LiberaForms macht es leicht, Online-Formulare zu erstellen und zu verwalten, die die digitalen Rechte der Nutzenden respektiert.

Mit LiberaForms können Sie die Antworten auf Ihre Formulare durchsuchen, bearbeiten und herunterladen. Sie können ganz leicht ein Kontrollkästchen einfügen, um die von Datenschutzgesetzen verlangte Zustimmung einzuholen. Sie können mit Anderen zusammenarbeiten, indem Sie Zugriffsrechte vergeben. Und Vieles mehr! LiberaForms ist Freie Kultur, veröffentlicht unter der AGPLv3-Lizenz.

Verwenden Sie es, teilen Sie es, verbessern Sie es!
