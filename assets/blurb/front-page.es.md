# Formularios éticos con LiberaForms

[LiberaForms](https://liberaforms.org/es) es una herramienta de software libre pensada y desarrollada como **infraestructura comunitaria, libre y ética** que permite crear y gestionar formularios que respetan los derechos digitales de las personas que hacen uso de ellos.

Con LiberaForms puedes consultar, editar y descargar las respuestas recibidas; incluir una casilla para pedir consentimiento sobre la Ley de Protección de Datos; colaborar con otras usuarias compartiendo permisos; y muchas cosas más! Además, LiberaForms es cultura libre bajo licencia AGPLv3.

¡Úsalo, compártelo y mejóralo!
