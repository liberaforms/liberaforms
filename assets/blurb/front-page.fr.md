# Des formulaires éthiques avec LiberaForms

[LiberaForms](https://liberaforms.org/fr) est un logiciel libre développé et conçu comme une **infrastructure communautaire, libre et éthique** permettant la création et la gestion de formulaires respectant les droits numériques des personnes qui les utilisent.

Avec LiberaForms vous pouvez consulter, éditer et télécharger les réponses que vous recevez; inclure une case à cocher pour demander le consentement à la loi de protection des données, collaborer avec d'autres utilisateur·rice·s en partageant les permissions; et bien plus !
LiberaForms fait partie de la culture libre et est publié sous licence AGPLv3.

Utilisez-le, partagez-le, améliorez-le !

