Este asistente axudarache a utilizar os criterios máis axeitados ao escribir as túas declaracións de privacidade.

As declaracións de privacidade xeradas por este asistente están feitas coa mellor das intencións, porén non son legalmente vinculantes.

Ten en conta que:

* O texto fornecido polo asistente é só unha recomendación e a súa validez dependerá da información que lle fornezas.
* Recomendámosche que consultes as nosas recomendacións e que revises os textos legais co departamento xurídico ou cun profesional de confianza, para que poidas adaptalos á túa lexislación local.

Consulta as Condicións de servizo de [LINK] para obter máis información.