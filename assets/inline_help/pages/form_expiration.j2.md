

## {{ _("Notify me when this form expires") }}

{{ _("Receive an email when the form has expired.") }}

## {{ _("Date and time when this form will expire") }}

{{ _("The form will not be availabe after this date.") }}

## {{ _("Maximum answers") }}

{{ _("The form will expire when it has been answered this many of times.") }}

## {{ _("Numeric fields") }}

{% if app_config['E2EE_MODE'].e2ee_enabled %}<span class="badge rounded-pill bg-info">{{ _("Disabled by E2EE") }}</span>{% endif %}

{{ _("LiberaForms will add the value of this field to a total. When the total is reached, the form will expire.") }}

{{ _("Imagine you are preparing an event. The maximum number of attendees is 18 and each person may make a reservation for up to 4 people.") }}

1. {{ _("Create a `Number` field and set the `Max` value to `4`.") }}
2. {{ _("Create an expiry condition for this field by setting the 'The sum of all the answers' to `18`.") }}

{{ _("When for example 15 reservations have been made, your form will only accept up to 3 new reservations (the remaining available reservations).") }}

## {{ _("After expiration") }}

{{ _("This text is displayed to the user instead of the form.") }}
