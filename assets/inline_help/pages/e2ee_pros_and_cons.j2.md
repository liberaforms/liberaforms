
## {{ _("End-to-end encryption (E2EE)") }}

{# i18n: secure : adjective #}
{{ _("Secure data transfer between your public and you.") }}

## {{ _("Pros") }}

{{ _("Encryption enables privacy.") }}
{{ _("No one but you can access the answers to your forms.") }}

{{ _("Remember that without encryption, server administrators can read your data.") }}


## {{ _("Cons") }}

{{ with_link(_("Most importantly, you must take responsibility to safeguard $$your key$$ without which, your data is lost."), "user_e2eeintro.j2.md") }}

{{ _("Because your answers are not available to the server, some features cannot be executed.") }}

<span class="badge rounded-pill bg-info">{{ _("Disabled by E2EE") }}</span>

{{ _("These features are:") }}

* {{ help_page(label=_("Confirmation email"), file_name="form_confirmation.j2.md") }}
{#- i18n: Refers to the feature that enables anonymous users to edit their answers #}
* {{ help_page(label=_("Anonymous answer edition"), file_name="form_answer_editions.j2.md") }}
* {{ help_page(label=_("Numeric expiry condition"), file_name="form_expiration.j2.md") }}
* {{ help_page(label=_("A public URL displaying map answers"), file_name="field_map.j2.md") }}
