
{% set link = i18n_docs_site_url("https://docs.liberaforms.org/user-guide/e2ee/") %}
{{with_link(_("$$Read more$$ about how LiberaForms uses key pairs."), link, external_page=True)}}


## {{ _("End-to-end encryption (E2EE)") }}

{{ _("Secure data transfer between your public and you.") }}

{{ _("No one but you can access the answers to your forms.") }}


## {{ _("Key pairs") }}

{{ _("To enable encryption you must first create your personal key pair.") }}

{{ _("When you create your key pair, two mathematically bound keys are created:") }}

+ {{ _("A public key that converts plain text into encrypted text") }}
+ {{ _("A private key that converts encrypted text into plain text") }}


### {{ _("Public key") }}

{{ _("A public key encrypts data. It cannot decrypt.") }}

{{ _("Your public key is saved on the server and is used to manage form encryption.") }}


### {{ _("Private key") }}

{{ _("The private decrypts the data that was encrypted with the public key.") }}

{{ _("The LiberaForms server does not have a copy of your private key.") }}

+ {{ _("You **must keep a copy** of your private key.") }}
+ {{ _("If you lose your private key you will **permanently lose** the **answers** to all your encrypted forms.") }}
+ {{ _("If you lose your private key, we cannot help you.") }} {{ _("BE WARNED!")}}

{{ _("Your private key is a secret.") }} {{ _("You, and only you, should know and have a copy of the your private key.") }}


## {{ _("Passphrase") }}

{{ _("Like a password, it is required to use (unlock) the private key before decryption.") }}


## {{ _("Key Fingerprint") }}

{{ _("Simply an identification of the key in case you ever need to compare two keys." )}}

---
[{{ _("Configure your encryption keys.") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('user_bp.manage_e2ee_keys')}})
