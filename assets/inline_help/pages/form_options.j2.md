
{% include './_form_context_menu.j2.md' %}

## {{ _("Publish") }}

+ {{ _("Public URL") }}: {{ _("The form's public address") }}
+ {{ _("Public") }}: {{ _("Make the form available on the Internet") }}
+ {{ _("Public URL in a QR") }}: {{ _("Download a QR containing the form's URL") }}
{%- if current_user.can_configure_fediverse() %}
+ {{ _("Social media") }}: {{ _("Publish this form on the Fediverse") }}
{% endif -%}
+ {{ _("Public map") }}: {{ help_page(label=_("A public URL displaying map answers"), file_name="field_map.j2.md") }}



## {{ _("Form") }}

+ {{ help_page(label=_("Look and feel"), file_name="form_style.j2.md") }}: {{ _("Add a personal touch to your form") }}
+ {{ help_page(label=_("Expiration"), file_name="form_expiration.j2.md") }}: {{ _("Forms can expire when conditions are met") }}
+ {{ _("Duplicate form") }}: {{ _("Create a copy of the form") }}
+ {{ _("Delete form") }}: {{ _("Permanently delete the form and it's answers") }}
+ {{ _("Author") }}: {{ _("The person who created the form") }}
+ {{ _("Created") }}: {{ _("The date the form was created") }}
+ {{ help_page(label=_("View log"), file_name="form_listlog.j2.md") }}: {{ _("A history of editor activity") }}


## {{ _("Answers") }}

+ {{ _("Last answer") }}: {{ _("The date and time of the last answer submitted") }}
{% if app_config["ENABLE_UPLOADS"] %}+ {{ _("Attached files") }}: {{ _("The total size of uploaded attachments") }}{% endif %}
+ {{ _("Notify me on each new answer") }}: {{ _("You will be sent an email") }}
+ {{ _("Allow users to edit their answers") }}: {{ _("A link is appended to the Thank you message") }}
+ {{ _("Notify me when they edit their answers") }}: {{ _("You will recieve an email when an anonymous user edits an answer") }}

{{ with_link(_("Note that $$answer editions$$ are saved to the form's logs."), "form_answer_editions.j2.md") }}


## {{ _("Confirmation") }}

{{ _("Send an email to the user after they have answered the form, at the same time they are shown the form's Thank you text") }}.
{{ help_page(label=_("Learn more"), file_name="form_confirmation.j2.md") }}


## {{ _("Group permissions") }}

+ {{ help_page(label=_("Editors"), file_name="form_editors.j2.md") }}: {{ _("Other users who can manage the form") }}
+ {{ help_page(label=_("Answers only"), file_name="form_shareanswers.j2.md") }}: {{ with_link(_("Users who can see and $$bookmark$$ answers"), "glossary.j2.md") }}
+ {{ _("Restricted access") }}: {{ _("Only public to people with an account at %(site_name)s", site_name=site.name) }}


## {{ _("Embedded form code") }}

{{ _("Copy the HTML code to embed the form in another website.") }}


{% include './form_apiendpoints.j2.md' %}
