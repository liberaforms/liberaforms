
{{ _("This wizard will help you write text that can be included at the foot of your forms.") }}

{{ _("An effort is made to generate a functional privacy statement for you.") }}

## {{ _("How it works") }}

{{ _("The wizard asks questions needed to build the text.") }}

{{ _("As you answer each question, the resulting text is displayed to you.") }}

{{ _("When you are ready, `Copy` the text and then, as you wish, edit it.") }}

## {{ _("The questions") }}

{{ _("Each question is accompanied by an explaination to help orientate you.") }}

{{ _("Questions can be answered in any order.") }}

## {{ _("The answers") }}

{{ _("You can change the answers at any time. Just click on the question again.") }}

## {{ _("The result") }}

{{ _("After answering the necessary questions the `Copy` button is enabled.") }}

{{ _("Copy the text to your privacy statement and then edit as needed.") }}
