
## API endpoints

{{ _("Computer to computer.") }}

+ JWT authorization: {{ _("Required to access the endpoints") }}
+ Authorization token: {{ _("Authorization value to include in the request header") }}
+ GET endpoints: {{ _("Available endpoints") }}

{{ _("Curl example:") }}

[sourcecode:bash]
curl -H 'Accept: application/json' -H "Authorization: Bearer ${JWT_TOKEN}" {{app_config['BASE_URL']}}/api/form/myresource
[/sourcecode]


{% if app_config['E2EE_MODE'].e2ee_enabled %}
{{ help_page(label=_("API and E2EE"), file_name="form_e2eeapi.j2.md") }}
{% endif %}
