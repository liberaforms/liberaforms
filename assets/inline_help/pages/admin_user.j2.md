

## {{ _("Details") }}

+ {{ _("Validated email") }}:
+ {{ help_page(label="Role", file_name="roles.j2.md") }}: {{ _("Each user has a role that defines what they can and cannot do") }}
+ {{ _("New forms disabled") }}: {{ _("Use this option to stop the user from creating new forms") }}
+ {{ _("Blocked by admin") }}: {{ _("The user's account is deactivated and their forms are not public") }}
+ {{ _("Delete user") }}: {{ with_link(_("Permanently $$delete this user$$ and their forms from the site."), "admin_user_delete.j2.md") }}

## {{ _("Disk usage") }}

{{ _("A summary of the uploaded media and form attachments.") }}

+ {{ help_page(label=_("Uploads limit"), file_name="admin_user_setuploadlimit.j2.md") }}: {{ _("Set maximum disk usage") }}

## {{ _("Statistics") }}

{{ _("A graph displaying the forms created by this user and their answers.") }}

## {{ _("This user's forms") }}

{{ with_link(_("A list of the forms created by this user. Click on the name of a form to $$see more details$$."), "admin_form.j2.md") }}
