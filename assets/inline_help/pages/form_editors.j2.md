


{{ _("Share form management with other users. This is useful when working as a team.") }}

{{ with_link(_("Learn $$more about roles$$."), "roles.j2.md") }}

## {{ _("Add new editor") }}

{{ _("Enter the email or username of the new form editor. This person must already have an account.") }}

## Permissions

{{ _("All editors have the same permissions. They can:") }}

+ {{ _("Change the form configuration and options") }}
+ {{ _("Bookmark, edit and delete answers") }}
+ {{ _("Share and unshare the form with other people") }}
+ {{ _("Delete the form") }}

{{ with_link(_("Note that the $$author$$ cannot be removed from the list of editors."), "glossary.j2.md") }}

## {{ _('Editor activity') }}

{{ with_link(_("Changes made to the form are $$logged$$."), "form_listlog.j2.md") }}

## {{ _('Share answers') }}

{{ with_link(_("Instead of sharing all form permissions, you can $$share the answers$$."), "form_shareanswers.j2.md") }}

{{ _("These users can also bookmark the answers.") }}
