
## {{ _("The new form name") }}

{{ _("The name of the form is for your convenience only and can be changed whenever you wish.") }} {{ _("It is not displayed publicly.") }}

{{ with_link(_("The form's $$URL$$ cannot be changed."), "glossary.j2.md") }}
