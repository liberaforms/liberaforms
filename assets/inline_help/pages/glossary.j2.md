


## {{ _("Admin") }}

{{ _("Responsible for the configuration of this installation of LiberaForms.") }}

{{ with_link(_("Learn $$more about roles$$."), "roles.j2.md") }}

## {{ _("Anonymous user") }}

{{ _("A person who fills out your form.") }}

## {{ _("Author") }}

{{ _("Each form has an Author, the Editor who first created it.") }}

## {{ _("Bookmark answers") }}

{{ with_link(_("Each individual answer can be highlighted with a $$bookmark$$."), "form_answers.j2.md") }}

## {{ _("Browser") }}

{{ _("Software you use to browse the Internet like Firefox.") }}

## {{ _("E2EE") }}

{{ _("End-to-end encryption") }}. {{ _("Secure data transfer between your public and you.") }}

{{ help_page(label=_("Learn more"), file_name="user_e2eeintro.j2.md") }}

## {{ _("Editor") }}

{{ _("A person with a %(site_name)s user account who can create forms and share them with other users.", site_name=site.name) }}

{{ with_link(_("Learn $$more about roles$$."), "roles.j2.md") }}

## {{ _("Favicon") }}

{{ _("The small icon displayed in your browser's tab.") }}

## {{ _("Guest") }}

{{ _("A person with a %(site_name)s user account with limited permissions.", site_name=site.name) }}

{{ with_link(_("Learn $$more about roles$$."), "roles.j2.md") }}

## HTML

{{ _("HyperText Markup Language or HTML is the standard for documents designed to be displayed in a web browser.") }}

## {{ _("Landing page") }}

{% set link = "<a href='/'>BASE_URL</a>"|replace("BASE_URL", BASE_URL) %}
{{ _("The page people see when they navigate to %(http_link)s", http_link=link|safe) }}

## {{ _("Slug" )}}

{{ _("The slug is the last part of a form's URL. For example:") }}

{{ BASE_URL }}/`{{ _("this-is-my-form") }}`

## {{ _("URL") }}

{{ _("The web address of your form. URL stands for: Uniform Resource Locator") }}
