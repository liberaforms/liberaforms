

{{ _("Assign disk space to this user.") }}

## {{ _("Limit") }}

{{ _("The sum of:") }}

+ {{ _("The user's media files") }}
+ {{ _("All attachments uploaded via the forms they have created") }}

{{ _("When the limit is exceeded the user cannot upload new media files or solicit attachments with their forms.") }}

{{ _("Published forms will continue to accept attachments.") }}
