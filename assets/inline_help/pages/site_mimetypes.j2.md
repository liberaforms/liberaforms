


{{ _("The types of files that can be attached to forms") }}

## {{ _("Enabled file extensions") }}

{{ _("Some file types represent a security risk.") }} {{_("<strong>exe</strong> files are a good example.") }}

<u>{{ _("Dangerous") }}</u>

{{ _("LiberaForms does not allow you to enable these extensions:") }}

{{ mimetype_risks("dangerous") }}

<u>{{ _("High risk") }}</u>

{{ _("Not recommended:") }}

{{ mimetype_risks("high-risk") }}

## {{ _("Enabled mimetypes") }}

{{ _("Indicates the nature and format of a file for your information.")}}

---

[{{ _("Edit mimetypes") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.edit_mimetypes')}})
