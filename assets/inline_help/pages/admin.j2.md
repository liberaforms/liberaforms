
{{ _("Options to configure this LiberaForms website.") }}

## {{ _("Site settings") }}

+ {{ help_page(label=_("Look and feel"), file_name="site_lookandfeel.j2.md") }}: {{ _("Site name, logo and color") }}
+ {{ help_page(label=_("Email server"), file_name="site_email_config.j2.md") }}: {{ _("Configure and test your setup") }}
+ {{ help_page(label=_("Custom translations"), file_name="site_customlanguages.j2.md") }}: {{ _("Define languages you can translate") }}
{%if app_config['ENABLE_LDAP']%}+ {{ help_page(label="LDAP", file_name="site_ldapconfig.j2.md") }}: {{ _("Configure and test your setup") }}{%endif%}

## {{ _("Custom texts") }}

+ {{ help_page(label=_("Front page text"), file_name="site_frontpagetext.j2.md") }}: Explain a little about {{site.name}}
+ {{ help_page(label=_("Resources menu"), file_name="site_resourcesmenu.j2.md") }}: {{ _("Add links to external pages for your users") }}
+ {{ help_page(label=_("User invitation template"), file_name="site_invitationtemplate.j2.md") }}: {{ _("A template of the message sent to invite new users") }}
+ {{ help_page(label=_("New form message"), file_name="site_newformmessage.j2.md") }}: {{ _("Optionally display a message to the user when they create a form") }}
+ {{ help_page(label=_("Other information"), file_name="site_otherinfo.j2.md") }}: {{ _("Important information for your users") }}

## {{ _("Data protection") }}

+ {{ help_page(label=_("Data protection settings"), file_name="site_dataprotection.j2.md") }}: {{ _("Minimum requirements") }}
+ {{ help_page(label=_("Privacy statement library"), file_name="site_dataconsent.j2.md") }}: {{ _("Texts that can be attached to forms on this site") }}
+ All forms must include a privacy statement

## {{ _("New users") }}

+ {{ _('Invitation only') }}: {{ _("When `True`, people need to receive an invitation to create an account") }}
+ {{ _('The new user form') }}: {{ _("Preview and configure the New user form") }}
+ {{ help_page(label=_("Default language"), file_name="site_newuserlanguage.j2.md") }}: {{ _("New users' initial language setting") }}
+ {{ _("Default upload limit") }}: {{ _("Disk space assigned to new users.") }}  {{_("To change it, please contact the person who installed LiberaForms for you") }}
+ {{ _("Default timezone") }}: {{ _("New users' timezone.")}} {{_("To change it, please contact the person who installed LiberaForms for you") }}

{% if app_config["ENABLE_UPLOADS"]%}
## {{ _("File uploads") }}

{{ _("The totals displayed here are for your information.") }}

{{ help_page(label=_("Form attachments"), file_name="site_mimetypes.j2.md") }}: {{ _("The types of files that can be attached to forms") }}
{%endif%}

## {{ _("Installation") }}

{{ _("The date %(site_name)s was installed and the version of this software.", site_name=site.name) }}

---
[{{_("Site configuration")}}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('admin_bp.admin_panel')}})
