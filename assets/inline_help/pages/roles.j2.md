

{{ _("Each person with an account on %(site_name)s has a role.", site_name=site.name) }}
<strong>
{% if is_admin %}{{ _("You are an admin.") }}
{% elif is_editor %}{{ _("You are an editor.") }}
{% else %}{{ _("You are a guest.") }}{% endif -%}
</strong>

{{ _("There are three roles.") }}

## 1. {{ _("Guest") }}

{{ _("A person with limited permissions.") }}

+ {{ _("Guests can read, download and bookmark answers") }}
+ {{ _("They can see the form's preview pages") }}
+ {{ _("They cannot edit forms or answers in any way") }}
+ {{ _("They cannot create forms") }}

{{ _("Guests are created when:") }}

+ {{ with_link(_("Editors $$share form answers$$"), "form_shareanswers.j2.md") }}
+ {{ _("Admins invite a new user with the guest role") }}

{{ _("When requested, an admin can change a guest's role to Editor.") }}

## 2. {{ _("Editor") }}

{{ _("A person with a %(site_name)s user account who can:", site_name=site.name) }}

+ {{ _("Create, edit, configure and publish forms") }}
+ {{ _("Read, edit, delete and download form answers") }}
+ {{ _("Share forms with other users") }}

{{ _("Each form has an Author, the Editor who first created it.") }}

## 3. {{ _("Admin") }}

{{ _("Responsible for the configuration of this installation of LiberaForms.") }}

{{ _("Admins can:") }}

+ {{ _("Change users' roles") }}
+ {{ _("Enable users to upload files, and set their total upload limit") }}
+ {{ _("Disable individual forms so that they cannot be made public") }}
+ {{ _("Share privacy statements with you") }}
+ {{ _("Force all forms on the site to include specific privacy statements") }}
+ {{ _("Block users") }}

{{ _("Admins cannot edit forms in any way or see their answers.") }}
