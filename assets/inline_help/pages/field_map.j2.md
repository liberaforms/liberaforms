


## {{ _("The Map field") }}

{{ _("Collect geographical data.") }}

{{ _("Use the field options to set the initial center point and zoom.") }}

## {{ _("Map markers") }}

{{ _("Define the fields to display in each marker's popup.") }}

## {{ _("Publish answers") }}

{%- if app_config['E2EE_MODE'].e2ee_enabled %}
<span class="badge rounded-pill bg-info">{{ _("Disabled by E2EE") }}</span>
{% endif %}

{{ _("Enable a public URL displaying map answers.") }}

### {{ _("Map JSON") }}

{{ _("JSON data URL required to independently render your map.") }}

{{ _("Example HTML") }}

{% set endpoint = BASE_URL %}

[sourcecode:html]

<!--
Edit here to load leaflet.js and leaflet.css
-->
<script src="leaflet.js"></script>
<link rel="stylesheet" type="text/css" href="leaflet.css">

<div id="map" style="height: 100vh;"></div>

<script>

  // get map data, wait until downloaded, then load map
  (async () => {
    let response = await fetch(
      "{{endpoint}}/resource",  // <-- JSON data URL
      {
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        }
      }
    );
    const map_data = await response.json();
    loadMap(map_data)
  })();

  function loadMap(map_data) {

    // create map
    map = L.map('map').setView(map_data.meta.centerPoint.split(',').map(Number),
                               map_data.meta.zoom);

    // add tiles to map
    L.tileLayer(map_data.meta.tiles, {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    // iterate markers
    for (let marker_data of map_data.markers) {

      // validate coords
      if (!(marker_data.coords.length == 2 &&
            is_valid_coord(marker_data.coords[0]) &&
            is_valid_coord(marker_data.coords[1]))) {
        // coords are not valid
        continue
      }

      // create marker and add to map
      let marker = L.marker(marker_data.coords).addTo(map);

      // populate popup
      var popup = ""
      marker_data.popup.forEach(function(popup_field) {
        popup = popup + `<strong>${popup_field.label}</strong>: ${popup_field.value}`
        popup = popup + "</br>"
      })

      // bind popup to marker
      marker.bindPopup(popup)
    }
  }

  function is_valid_coord(value) {
    function isFloat(n) {
      if (typeof n !== 'number') {
          return false;
      }
      return Number(n) === n && n % 1 !== 0;
    }
    value = value.trim()
    if (value !== "") {
      let n = Number(value)
      if ( Number.isInteger(n) || isFloat(n) ) {
        return true
      }
    }
    return false
  }
</script>

[/sourcecode]
