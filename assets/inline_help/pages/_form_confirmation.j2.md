
## {{ _("Confirmation email") }}

{% if app_config['E2EE_MODE'].e2ee_enabled %}<span class="disabled-by-e2ee-badge badge rounded-pill bg-info">{{ _("Disabled by E2EE") }}</span>{% endif %}

{{ _("To enable this option include a `Short text` field in the form and change it's `type` option to `email`.") }}

{{ _("If your form includes more than one email field, LiberaForms will use the first one.") }}
