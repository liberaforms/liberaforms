
## {{ _("Site name" )}}

{{ _("This value is for:") }}

+ {{ _("Form footers") }}
+ {{ _("Email notifications") }}
+ {{ _("LiberaForms' web interface") }}
+ {{ _("Browser tab label") }}
+ {{ _("etc") }}


## {{ _("Site logo") }}

{{ _("The logo is displayed at the top left of your LiberaForms installation.") }}

{{ with_link(_("It is also used as the site's $$favicon$$."), "glossary.j2.md") }}

{{ _("A small image helps load forms more quickly. It does not need to be larger than 64 x 64 pixels.") }}

{{ _("After saving, you may need to refresh `<F5>` your browser to see the changes.") }}

## {{ _("Header font color") }}

{{ _("Sets the font contrast against the background color.") }}

## {{ _("Primary color") }}

{{ _("The color of the top navigation menu and other elements.") }}

## {{ _("HTML color code") }}

{{ _("Use this option to use your own specific color.") }}

---

[{{_("Configure look and feel")}}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.look_and_feel')}})
