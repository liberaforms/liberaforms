

This is very similar to non-encrypted E2EE forms, except the server will only
pass you a special "e2ee-answer" field, which you'll have to decrypt yourself.

{% set link = i18n_docs_site_url("https://docs.liberaforms.org/user-guide/e2ee/") %}
[Read more]({{link}}) about how LiberaForms uses key pairs.


## Importing the PGP key

First of all, you need to get the form's PGP key from the browser, so we can import it in a GnuPG keyring.

Go to the form's Endpoints options and copy the key to the clipboard.

You can now use this command:
[sourcecode:bash]
base64 -d | gpg --import
[/sourcecode]

Now paste the PGP key on its standard input, and finish with `^D` (`Ctrl+D`)(twice?).

You should see something like:
[sourcecode:text]
gpg: key 0xCC37B99A9FB723ED: public key "1@form.keys.{{app_config['SERVER_NAME']}}" imported
gpg: key 0xCC37B99A9FB723ED: secret key imported
gpg: Nombre total processat: 1
gpg:               importades: 1
gpg:  claus privades llegides: 1
gpg: claus privades importades: 1
[/sourcecode]

## Reading answers

After the key has been imported, we can read answers with this example `e2ee-api.sh` script.

[sourcecode:text]
sudo apt-get install curl jq
[/sourcecode]

[sourcecode:bash]
#!/bin/sh -eu

form_id="$1"

fetch_e2ee_answers() {
	curl -sqH "Authorization: Bearer ${JWT_TOKEN}" "${BASE_URL}/api/form/${form_id}/answers" | jq -r '.answers | .[] | .data."e2ee-answer"'
}

for enc_ans in $(fetch_e2ee_answers); do
  ans="$(echo "${enc_ans}" | base64 -d | gpg --decrypt --quiet)"
  echo "${ans}"
done
[/sourcecode]

Note that the shell script relies on the `JWT_TOKEN` and `BASE_URL`
environmental variables.

The output should look something like this (where 1 is the form id, as seen
in the form settings on API Endpoints/GET Form).

[sourcecode:text]
$ ./e2ee-api.sh 1
{"text-1720986390565":"asdf"}
{"text-1720986390565":"abcdef"}
[/sourcecode]
