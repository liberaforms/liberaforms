

{{ _("As an admin you can see some details of the form but you cannot edit them.") }}

{{ _("You cannot consult the form's answers.") }}

## {{ _("Form") }}

+ {{ _("Author") }}: {{ _("The person who created the form") }}
+ {{ _("Duplicate") }}: {{ _("Create a copy of the form") }}
+ {{ _("Disable") }}: {{ _("The form will not be public") }}

{% if is_root_user %}
## {{ _("Super Admin options") }}

{{ _("Change the author of the form.") }}

{% else %}
## {{ _("Change author") }}

{{ _("If required, the admin %(email)s can change the author of the form.", email=app_config["ROOT_USER"]) }}
{% endif %}
